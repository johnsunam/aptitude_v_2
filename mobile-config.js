App.info({
  id: 'com.example.matt.uber',
  name: 'aptitude',
  description: 'aptitude',
  author: 'John Sunam'
});
// Set up resources such as icons and launch screens.
//  App.icons({
//    'android_ldpi': 'resources/icons/icon-36x36.png',
//    'android_mdpi': 'resources/icons/icon-48x48.png',
//    'android_hdpi': 'resources/icons/icon-72x72.png',
//    'android_xhdpi': 'resources/icons/icon-96x96.png'
//   //  More screen sizes and platforms...
//  });
//  App.launchScreens({
//    'android_ldpi_portrait': 'resources/splash/splash-200x320.png',
//    'android_ldpi_landscape': 'resources/splash/splash-320x200.png',
//    'android_mdpi_portrait': 'resources/splash/splash-320x480.png',
//    'android_mdpi_landscape': 'resources/splash/splash-480x320.png',
//    'android_hdpi_portrait': 'resources/splash/splash-480x800.png',
//    'android_hdpi_landscape': 'resources/splash/splash-800x480.png',
//    'android_xhdpi_portrait': 'resources/splash/splash-720x1280.png',
//    'android_xhdpi_landscape': 'resources/splash/splash-1280x720.png'                                                                                                                                                                                                                                                           
//     //More screen sizes and platforms...
//  });                                                                                                                       
// // // Set PhoneGap/Cordova preferences.                                                                                                                                                                                        
// App.setPreference('BackgroundColor', '0xff0000ff');
// App.setPreference('HideKeyboardFormAccessoryBar', true);
//  App.setPreference('Orientation', 'default');
//  App.setPreference('Orientation', 'all', 'ios');
// Pass preferences for a particular PhoneGap/Cordova plugin.
// App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//   APP_ID: '1234567890',
//   API_KEY: 'supersecretapikey'
// });
App.setPreference('android-minSdkVersion' , 19);
App.setPreference("LoadUrlTimeoutValue", 1000000);

App.setPreference("WebAppStartupTimeout",1000000);
App.accessRule('data:*', {type: 'navigation'});
//// Add custom tags for a particular PhoneGap/Cordova plugin to the end of the
// generated config.xml. 'Universal Links' is shown as an example here.

App.appendToConfig(`
  <universal-links>
    <host name="http://54.169.152.181:80" />
  </universal-links>
`);