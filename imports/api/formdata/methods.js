import {FormDataDb} from './collection/formdata.collection.js';
import {WorkflowDataDb} from './collection/workflowData.collection.js'
import moment from '../../../public/lib/moment-2.13.0'
Meteor.methods({
  'addFormData': function (data) {
    FormDataDb.insert(data)
  },
  'addWorkflowData': function (data) {
    WorkflowDataDb.insert(data)
  },
  'updateFormData': function (data) {
    FormDataDb.update({
      _id: data.id
    }, {
      $set: {
        formdata: data.formdata
      }
    })
  },
  updateWorkflowDataApprove: function (data) {
    WorkflowDataDb.update({
      _id: data.id
    }, {
      $set: {
        approveRoles: data.approveRoles
      }
    });
  },
  updateWorkflowAction: function (data) {
    WorkflowDataDb.update({
      _id: data.id
    }, {
      $set: {
        action: data.action
      }
    });
  },
  updateLastmsg: function (data) {
    WorkflowDataDb.update({
      _id: data.id
    }, {
      $set: {
        'action.lastmsg': data.lastmsg
      }
    });
  },
  addRemark: function (data) {
    WorkflowDataDb.update({
      _id: data.id
    }, {
      $set: {
        remark: data.remark
      }
    });
  },
  updateWorkflowData: function (data) {

    var old = WorkflowDataDb.findOne({
      _id: data.id
    }, {formdata: 1});

    _.extend(old.formdata, data.formdata);

    WorkflowDataDb.update({
      _id: data.id
    }, {
      $set: {
        formdata: old.formdata,
        level: data.level
      }
    });
  },
  'getdataperiod': function (type) {
    if (type == 'daily') {
      let date = moment()
        .startOf('day')
        .toDate()
      let datas = WorkflowDataDb
        .find({
        createdAt: {
          $gt: date
        }
      })
        .fetch();
      return datas;
    }
    if (type == 'weekly') {
      let from = moment()
        .startOf('week')
        .toDate()
      let to = moment().toDate();
      let datas = WorkflowDataDb
        .find({
        createdAt: {
          $gt: from,
          $lt: to
        }
      })
        .fetch();
      return datas;
    }
    if (type == 'monthly') {
      let from = moment()
        .startOf('month')
        .toDate()
      let to = moment().toDate();
      let datas = WorkflowDataDb
        .find({
        createdAt: {
          $gt: from,
          $lt: to
        }
      })
        .fetch();
      return datas;
    }
  },

  "getdataRange": function (range) {
    let from = moment(range.from).toDate();
    let to = moment(range.to).toDate();
    let datas = WorkflowDataDb
      .find({
      workflow: range.workflow,
      createdAt: {
        $gte: from,
        $lte: to
      }
    })
      .fetch();
    return datas;
  },

  "closeIncident": function (id) {
      WorkflowDataDb.update({_id:id},{$set:{status:'close'}});
  }
})