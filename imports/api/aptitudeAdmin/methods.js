import {AptitudeAdminDb} from "./collection/aptitude.admin.collection.js"
import {AccountDb} from '../account/collection/collection.js'
import {UserAccountDb} from '../account/collection/userAccount.js'
import {Accounts} from 'meteor/accounts-base'
import { Email } from 'meteor/email'
import { Sparkpost } from 'meteor/agoldman:sparkpost-mail'
// var SparkPost = require('sparkpost');
Meteor.methods({
  'createAptitudeAdmin':function(record){
    let user=Accounts.createUser({email:record.data.email,password:"aptitude123"})
    Roles.addUsersToRoles(user,['aptitude-admin'])
    let rec=record.data
    rec.user=user;
    rec.companies=[];
    AptitudeAdminDb.insert(rec);
  },
  'deleteUser':function(id){
    let ad=AptitudeAdminDb.findOne({_id:id});
    Meteor.users.remove({_id:ad.user})
    AptitudeAdminDb.remove({_id:id});
  },
  'editUser':function(record){
    return AptitudeAdminDb.update({_id:record.id},{$set:record.data})
  },
  sendEmail: function(mail) {
        
        //npm install sparkpost -- save
        //i create a key temporary later sign up account and verify domain 100k/month limit sparkpost.com
       // var sp = new SparkPost("7d83a70ce6aebb04cac288d363d3e3ca8087b6d2"); //Meteor.settings.private.sparkAPI
    Sparkpost.config('7d83a70ce6aebb04cac288d363d3e3ca8087b6d2');
        let ufrom = "no-reply@aptitude.io";
        let name = "Aptitude";
        let replyto = "no-reply@aptitude.io";
        
   /*  if(mail.CC !=""){
             sp.transmissions.send({
            content: {
                headers:{
                    CC:mail.CC
                },
            	//can use when domain is verified
                from: { 
                    'name': name,
                    'email': "from@sparkpostbox.com"
                },
                subject: mail.subject,
                // reply_to: replyto,
                html: mail.body //content html format
            },
            recipients:mail.recipients
        }).then(data => {
            console.log("sent");
            console.log(data);
        }).catch(err => {
            console.log(err);
        });
        }
        else if (mail.BCC== true) {
             sp.transmissions.send({
            content: {
            	//can use when domain is verified
                from: { 
                    'name': name,
                    'email': "from@sparkpostbox.com"
                },
                subject: mail.subject,
                // reply_to: replyto,
                html: mail.body //content html format
            },
            recipients:mail.recipients
        }).then(data => {
            console.log("sent");
            console.log(data);
        }).catch(err => {
            console.log(err);
        });
        } else {
             sp.transmissions.send({
            content: {
            	//can use when domain is verified
                from: { 
                    'name': name,
                    'email': "from@sparkpostbox.com"
                },
                subject: mail.subject,
                // reply_to: replyto,
                html: mail.body //content html format
            },
            recipients:mail.recipients
        }).then(data => {
            console.log("sent");
            console.log(data);
        }).catch(err => {
            console.log(err);
        });
        }*/
     //   console.log(mail)
      Sparkpost.send({
  	    from: 'from@sparkpostbox.com',
  	    to: mail.to,
        cc:mail.cc,
        bcc:mail.bcc,
  	    subject: mail.subject,
  	    text: mail.body
    });
       
       
    }

})
