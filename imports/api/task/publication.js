import {TaskDb} from './collection/task.collection.js'

Meteor.publish('getTask', function () {
  if (this.userId) {
    return TaskDb.find();
  } else {
    throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
  }

})