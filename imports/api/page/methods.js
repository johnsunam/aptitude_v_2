import {PageDb} from "./collection/page.collection.js"

Meteor.methods({
  'addPage': function (record) {
    var exists = PageDb.findOne({name: record.name})
    if (exists) {
      return false;
    } else {
      let result = PageDb.insert(record)
      return result;
    }

  },
  'updateConfigurationField': function (id, data) {
    PageDb.update({
      _id: id
    }, {
      $set: {
        form: data
      }
    });
  },

  'deletePage': function (id) {
    PageDb.remove({_id: id});
  },
  'editPage': function (record) {
    PageDb.update({
      _id: record.id
    }, {$set: record.data})
  },
  'pageForm': function (record) {
    PageDb.update({
      _id: record.id
    }, {
      $set: {
        form: record.data
      }
    });
  }
})