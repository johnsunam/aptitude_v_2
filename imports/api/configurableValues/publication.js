import {ConfigurableValueDb} from './collection/configurableValues.collection.js';

Meteor.publish('getConfigurableValue', function () {
    if (this.userId) {
        return ConfigurableValueDb.find();
    } else {
        throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
    }

})