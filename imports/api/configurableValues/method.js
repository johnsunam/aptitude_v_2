import {ConfigurableValueDb} from "./collection/configurableValues.collection.js";

Meteor.methods({
    'addConfigurableValue': data => {
        ConfigurableValueDb.insert(data);
    },
    'addValues': data => {
        ConfigurableValueDb.update({
            workflow: data.workflow,
            page: data.page,
            field_name: data.field_name
        }, data);
    }
})