import {FormDb} from './collection/form.collection.js'

Meteor.publish('getForm', function () {
  if (this.userId) {
    return FormDb.find();
  } else {
    throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
  }
  return FormDb.find();
})