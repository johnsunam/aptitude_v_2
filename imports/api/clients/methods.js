import {ClientDb} from "./collection/client.collection.js"
import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base'
import {Email} from "meteor/email"
import {Random} from "meteor/random"
import {GetContactEmail} from '../email-template';
import {AccountDb} from '../account/collection/collection.js'
import {UserAccountDb} from '../account/collection/userAccount.js'
import {ClientUserMap} from '../account/collection/clientUserMap.js'
import {AptitudeAdminDb} from '../aptitudeAdmin/collection/aptitude.admin.collection.js'
Meteor.methods({
  'addClient': function (record) {
    let clientExits = ClientDb.findOne({companyName: record.data.companyName});
    if (clientExits) {
      return false;
    } else {
      let user = Accounts.createUser({email: record.data.email, password: "aptitude123"});
      Roles.addUsersToRoles(user, ['client-admin']);
      let rec = record.data;
      rec.user = user;
      rec.client_users = [];
      rec.companies = [user];
      rec.type = "super_admin";
      let client = ClientDb.insert(rec);
      AptitudeAdminDb.update({
        user: record.user
      }, {
        $push: {
          companies: user
        }
      });
    }
  },
  'deleteClient': function (id) {
    let client = ClientDb.findOne({_id: id});
    Meteor
      .users
      .remove({_id: client.user});
    ClientDb.remove({_id: id});
  },
  'editClient': function (record) {
    ClientDb.update({
      _id: record.id
    }, {$set: record.data})
  },
  'saveRoles': function (data) {
    ClientDb.update({
      _id: data.client
    }, {
      $set: {
        roles: data.data
      }
    })
  },

  'removeRoles': function (data) {
    ClientDb.update({
      _id: data.client
    }, {
      $set: {
        roles: data.roles
      }
    })
  }

});