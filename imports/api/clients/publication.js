import {ClientDb} from './collection/client.collection.js'

Meteor.publish('getClient', function () {
  if (this.userId) {
    return ClientDb.find();
  } else {
    throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
  }

})