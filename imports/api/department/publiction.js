import {DepartmentDb} from './collection/department.collection.js'

Meteor.publish('getDepartment', function () {
  if (this.userId) {
    return DepartmentDb.find();
  } else {
    throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
  }

})