import {WorkflowDb} from "./collection/workflow.collection.js"
import {ClientDb} from "../clients/collection/client.collection";
Meteor.methods({
  'addWorkFlow': function (record) {
    return WorkflowDb.insert(record);
  },
  "saveFlowchart": function (data) {
    WorkflowDb.update({
      _id: data.workflow
    }, {
      $set: {
        charts: data.charts,
        flowchart: data.flowchartData,
        action: data.action,
        emails: data.emails
      }
    })
  },
  'deleteWorkFlow': function (id) {
    WorkflowDb.remove({_id: id});
  },
  'editWorkFlow': function (record) {
    return WorkflowDb.update({
      _id: record.id
    }, {$set: record.data})
  },
  updateWorkflowChart: function (data) {
    return WorkflowDb.update({
      _id: data.id
    }, {
      $set: {
        charts: data.charts
      }
    })
  },
  'updateWorkflowAxis': function (data) {
    return WorkflowDb.update({
      _id: data.id
    }, {
      $set: {
        axis: {
          xAxis: data.xAxis,
          yAxis: data.yAxis
        }
      }
    })
  },
  'addWorkflowClient': function (data) {
    let roles = [];
    client = {};
    _.each(data.clients, (single) => {
      client = ClientDb.findOne({user: single});
      roles = _.union(roles, client.roles);
    });

    return WorkflowDb.update({
      _id: data.id
    }, {
      $set: {
        client: data.clients,
        roles: roles
      }
    })
  },
  'updateWorkflowPageRoles': function (data) {
    WorkflowDb.update({
      _id: data.id
    }, data.workflow)
  }
})