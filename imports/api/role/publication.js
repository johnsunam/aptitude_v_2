import {RoleDb} from './collection/role.collection.js'

Meteor.publish('getRole', function () {
  if (this.userId) {
    return RoleDb.find();
  } else {
    throw new Meteor.Error('list.unauthorized', 'This list doesn\'t belong to you.');
  }

})