import {NotificationDb} from './collection/notification.collection';

Meteor.methods({
    'addNotification': function(data) {
        data.created_at = new Date();
        data.status = true;
        NotificationDb.insert(data);
    }
})