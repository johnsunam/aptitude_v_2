import {ClientUserDb} from './collection/clientUser.collection.js'
import {ClientDb} from '../clients/collection/client.collection.js'
import {Random} from 'meteor/random'
import {AccountDb} from '../account/collection/collection.js'
import {UserAccountDb} from '../account/collection/userAccount.js'
import {ClientUserMap} from '../account/collection/clientUserMap.js'

Meteor.methods({
  'addClientUser': function (record) {
    let userId = Accounts.createUser({email: record.data.email, password: "aptitude123"})
    let roles = record.data.userTypes;
    let accountId = AccountDb.insert({account_name: userId})
    let data = {
      userId: userId,
      accountId: accountId
    }
    let rec = record.data
    rec.user = userId
    UserAccountDb.insert(data)

    if (userId) {
      ClientUserDb.insert(rec)
      roles.push("client-user");
      Roles.addUsersToRoles(userId, roles);
    }

    return userId;

  },
  'editClientUser': function (record) {
    ClientUserDb.update({
      _id: record.id
    }, {$set: record.data})
  },
  'deleteClientUser': function (id) {
    let client = ClientUserDb.findOne({_id: id});
    Meteor
      .users
      .remove({_id: client.user})
    ClientUserDb.remove({_id: id});
  }
})