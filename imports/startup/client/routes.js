import React from 'react';
import {FlowRouter} from 'meteor/kadira:flow-router'
import {ReactLayout} from 'meteor/kadira:react-layout'
import MainLayout from '../../ui/container/mainLayout.js';
import AddForm from '../../ui/components/aptitude/form/addForm.jsx'
import ManageForm from '../../ui/container/manageForm.js'
import AddClient from '../../ui/components/aptitude/client/addClient.jsx'
import EditClient from '../../ui/container/editClient.js';
import ManageClient from '../../ui/container/manageClient.js'
import AddPage from '../../ui/container/addPage.js'
import ManagePage from '../../ui/container/managePage.js'
import AddWorkFlow from '../../ui/components/aptitude/workflow/addWorkFlow.jsx'
import EditWorkflow from '../../ui/container/editWorkflow';
import ChooseClient from '../../ui/container/chooseClient'
import DefineWorkFlow from '../../ui/components/aptitude/workflow/defineWorkFlow.jsx'
import AddUser from '../../ui/components/aptitude/user/addUser.jsx'
import ManageUser from '../../ui/container/manageUser.js'
import ClientAdminLayout from '../../ui/container/clientAdminLayout.js'
import AdminLogin from '../../ui/container/adminLogin.js'
import ClientLogin from '../../ui/components/accounts/login/clientlogin.jsx'
import ClientAdminPages from '../../ui/container/clientDashboard.js'
import EditClientUser from '../../ui/container/editClientUser.js'
import AddClientUser from '../../ui/container/addClientUser.js'
import ManageClientUser from '../../ui/container/manageClientUser.js'
import AssignRoles from '../../ui/container/assignRoles.js'
import ClientUserLogin from '../../ui/components/app/login/login.jsx'
import AppDashboard from '../../ui/container/appDashboard.js'
import AptitudeAccountsLayout from '../../ui/container/aptitudeAccountsLayout.js'
import ClientAccountsLayout from '../../ui/container/clientAccountLayout.js'
import AppAccountsLayout from '../../ui/container/appAccountLayout.js'
import AppLayout from '../../ui/container/appLayout.js'
import EditForm from '../../ui/container/editForm.js'
import ChooseAccount from '../../ui/container/chooseAccounts.js'
import Layout from '../../ui/container/layout.js'
import Reports from '../../ui/container/reports.js'
import FillForm from '../../ui/container/fillForm.js'
import ViewReport from '../../ui/container/viewReport.js'
import NotFoundPage from '../../ui/components/common/notFoundPage.jsx'
import PageFormEdit from '../../ui/container/pageFormEdit.js'
import EditPage from '../../ui/container/editPage.js'
import CofigureField from '../../../imports/ui/container/configureField'
import WorkflowPages from '../../ui/container/workflowPages';
import HomePage from '../../ui/components/client/homePages/homePage';
import PublicPage from '../../ui/container/publicPage';

FlowRouter.route('/', {
    name: 'aptitudeLogin',
    action: function () {
        window
            .localStorage
            .setItem('appType', null);
        window
            .localStorage
            .setItem('user', null);
        window
            .localStorage
            .setItem('loginType', null);
        window
            .localStorage
            .setItem('subType', null);
        ReactLayout.render(AptitudeAccountsLayout, {
            content: < AdminLogin / >
        })
    }
});

//Routes for client side

FlowRouter.route('/client/manage-role', {
    name: 'manageClientRole',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < ManageClientRole / >
        })
    }
});

FlowRouter.route('/client/add-role', {
    name: 'addClientRole',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < AddClientRole / >
        })
    }
});
FlowRouter.route('/client/add-user', {
    name: 'addClientUser',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < AddClientUser / >,
            type: 'Users'
        })
    }
});
FlowRouter.route('/client/manage-user', {
    name: 'manageClientUser',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < ManageClientUser / >,
            type: "Users"
        })
    }
});

FlowRouter.route('/client/configurable/:id', {
    name: 'worflowPages',
    action: (params) => {
        ReactLayout.render(ClientAdminLayout, {
            content: < WorkflowPages id = {
                params.id
            } />
        })
    }
})

//Route for aptitude admin /route for add form
FlowRouter.route('/aptitude/assign-roles', {
    name: 'assignRoles',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AssignRoles / >
        })
    }
});

FlowRouter.route('/aptitude/add-form', {
    name: 'addForm',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddForm / >
        })
    }
});
FlowRouter.route('/aptitude/add-form/:id', {
    name: 'addForm',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < AddForm adminuser = {
                params.id
            } />
        })
    }
});
//route to choose the admin or client account
FlowRouter.route('/chooseAccount', {
    name: 'manageForm',
    action: function () {
        ReactLayout.render(Layout, {
            content: < ChooseAccount / >
        })
    }
});
//route for manage form

FlowRouter.route('/aptitude/manage-form', {
    name: 'manageForm',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ManageForm / >
        })
    }
});

//route for add client
FlowRouter.route('/aptitude/add-client', {
    name: 'addClient',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddClient / >
        })
    }
});

FlowRouter.route('/aptitude/edit-client/:id', {
    name: 'editClient',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < EditClient id = {
                params.id
            } />
        })
    }
});

//route for manage client
FlowRouter.route('/aptitude/manage-client', {
    name: 'manageClient',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ManageClient / >
        })
    }
});

//route for add page
FlowRouter.route('/aptitude/add-page', {
    name: 'addPage',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddPage / >
        })
    }
});
FlowRouter.route('/aptitude/edit-page/:id', {
    name: 'editPage',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < EditPage id = {
                params.id
            } />
        })
    }
});
//route for manage page
FlowRouter.route('/aptitude/manage-page', {
    name: 'managePage',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ManagePage / >
        })
    }
});

//route for add user
FlowRouter.route('/aptitude/add-user', {
    name: 'addUser',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddUser / >
        })
    }
});

//route for manage user
FlowRouter.route('/aptitude/manage-user', {
    name: 'manageUser',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ManageUser / >
        })
    }
});
FlowRouter.route('/aptitude/edit-form/:id', {
    name: 'editForm',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < EditForm id = {
                params.id
            } />
        })
    }
});
//route for add role
FlowRouter.route('/aptitude/add-role', {
    name: 'addRole',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddRole / >
        })
    }
});

//route for manage role
FlowRouter.route('/aptitude/manage-role', {
    name: 'manageRole',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ManageRole / >
        })
    }
});

//route for add workflow
FlowRouter.route('/aptitude/add-workflow', {
    name: 'addWorkFlow',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < AddWorkFlow / >
        })
    }
});

FlowRouter.route('/aptitude/edit-workflow/:id', {
    name: 'editWorkFlow',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < EditWorkflow id = {
                params.id
            } />
        })
    }
});

//route for manage workflow
FlowRouter.route('/aptitude/manage-workflow', {
    name: 'manageWorkFlow',
    action: function () {
        ReactLayout.render(MainLayout, {
            content: < ChooseClient / >
        })
    }
});
//route for define workflow
FlowRouter.route('/aptitude/define-workflow', {
    name: 'defineWorkFlow',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < DefineWorkFlow id = {
                params.id
            } />
        })
    }
});
FlowRouter.route('/aptitude/page-form/:id', {
    name: 'pageForm',
    action: function (params) {
        ReactLayout.render(MainLayout, {
            content: < PageFormEdit id = {
                params.id
            } />
        })
    }
});

//route for client dashboard
FlowRouter.route('/client/dashboard', {
    name: 'client',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < HomePage / >,
            type: "dashboard"
        })
    }
});
FlowRouter.route('/client/workflows', {
    name: 'client',
    action: function () {
        ReactLayout.render(ClientAdminLayout, {
            content: < ClientAdminPages / >,
            type: "workflows"
        })
    }
});
//route for client user edit
FlowRouter.route('/client/edit-user/:id', {
    name: 'editClientUser',
    action: function (params) {
        ReactLayout.render(ClientAdminLayout, {
            content: < EditClientUser id = {
                params.id
            } />,
            type: 'Users'
        })
    }
});
//route for client reports
FlowRouter.route('/reports/:id', {
    name: 'client',
    action: function (params) {
        ReactLayout.render(ClientAdminLayout, {
            content: < Reports workflow = {
                params.id
            } />,
            type: "reports"
        })
    }
});

FlowRouter.route('/client/login', {
    name: 'clientLogin',
    action: function () {
        ReactLayout.render(ClientAccountsLayout, {
            content: < ClientLogin / >
        })
    }
});
FlowRouter.route('/aptitude/login', {
    name: 'aptitudeLogin',
    action: function () {
        ReactLayout.render(AptitudeAccountsLayout, {
            content: < AdminLogin / >
        })
    }
});
FlowRouter.route('/app/login', {
    name: 'login',
    action: function () {
        ReactLayout.render(AppAccountsLayout, {
            content: < ClientUserLogin / >
        })
    }
});
FlowRouter.route('/app/dashboard', {
    name: 'dashboard',
    action: function () {
        ReactLayout.render(AppLayout, {
            content: < AppDashboard / >
        })
    }
});

FlowRouter.route('/app/configure/:id', {
    name: 'configureField',
    action: params => {
        ReactLayout.render(AppLayout, {
            content: < CofigureField id = {
                params.id
            } />
        })
    }
});

FlowRouter.route('/fillform/:id', {
    name: 'fillForm',
    action: function (params) {
        ReactLayout.render(AppLayout, {
            content: < FillForm id = {
                params.id
            } />
        })
    }
});

FlowRouter.route('/view/:id', {
    name: 'fillForm',
    action: function (params) {
        ReactLayout.render(AppLayout, {
            content: < ViewReport id = {
                params.id
            } />
        })
    }
});
FlowRouter.route('/aptitude/public/:id', {
    name: 'publicPage',
    action: function (params) {
        ReactLayout.render(PublicPage, {id: params.id})
    }
});

FlowRouter.notFound = {
    // Subscriptions registered here don't have Fast Render support.
    action: function () {
        ReactLayout.render(AptitudeAccountsLayout, {
            content: < NotFoundPage / >
        })
    }
};