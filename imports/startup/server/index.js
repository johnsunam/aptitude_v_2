import '../../api/task/methods.js'
import '../../api/task/publication.js'
import '../../api/form/methods.js'
import '../../api/clients/methods.js'
import '../../api/clients/publication.js'
import '../../api/page/methods.js'
import '../../api/page/publication.js'
import '../../api/form/publication.js'
import '../../api/workflow/methods.js'
import '../../api/workflow/publication.js'
import '../../api/role/methods.js';
import '../../api/role/publication.js'
import '../../api/aptitudeAdmin/methods.js'
import '../../api/aptitudeAdmin/publication.js'
import '../../api/department/methods.js'
import '../../api/department/publiction.js'
import '../../api/clientUser/methods.js'
import '../../api/clientUser/publiction.js'
import '../../api/formdata/publication.js'
import '../../api/formdata/methods.js'
import '../../api/account/publication.js'
import '../../api/account/methods.js'
import '../../api/configurableValues/method';
import '../../api/configurableValues/publication';
import '../../api/notification/collection/notification.collection';
import '../../api/notification/methods';
import '../../api/notification/publication';
import './mail.js'