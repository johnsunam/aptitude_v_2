import React ,{Component} from 'react'
import AdminLogin from '../components/accounts/login/adminlogin.jsx'
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';

export default class AptitudeLogin extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.localStorage.setItem('choosed',null);
        window.localStorage.setItem('users',null);
        window.localStorage.setItem('user',null);
        window.localStorage.setItem('type',null);
        window.localStorage.setItem('userAccess',null);
        window.sessionStorage.setItem('user',null);
        Meteor.logout()
    }
    render(){
        return this.props.content?this.props.content:
                    <div>
                        <AdminLogin/>
                        <Alert stack={{limit: 3}}/>
                    </div>
                
            

    }
}