import React,{Component} from 'react'
import Sidebar from '../components/common/clientAdminSidebar.jsx';
import Header from '../components/common/clientAdminHeader.jsx';
import ClientBreadCrum from '../components/common/clientBreadCrum';

export default ClientAdminLayout = (props) => {
    return( <div id="page-wrapper">
                <div id="page-container" className="header-fixed-top">
                    <div id="main-container">
                        <Header />
                        <div id="my-page-content" style={{overflow: 'hidden', minHeight: '100vh'}}>
                            <ClientBreadCrum type={props.type}/>
                            <div className="row">
                                {props.content}
                            </div>
                        </div>
                    </div>
                </div>
            </div>)
}

