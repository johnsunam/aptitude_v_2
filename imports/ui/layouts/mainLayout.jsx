import React,{Component} from 'react'
import Header from '../container/header.js';
import Footer from '../components/common/footer.jsx'
import Sidebar from '../components/common/siderbar.jsx';
import AdminLogin from '../components/accounts/login/adminlogin.jsx'
import commonImports from '../components/common/commonImports.jsx'
import './inlineCss.css';
export default class MainLayout extends Component {
  constructor(props) {
    super(props);
    
  }
  componentDidMount() {
  }

  render(){
      return(
          <div id="sb-site" style={{minHeight: 800}}>
            <div id="page-wrapper">
              <Header/>
              <Sidebar/>
              <div id="page-content-wrapper">
                <div id="page-content" style={{minHeight: 800}}>
                  <div className="container">
                      {this.props.content}
                  </div>
                </div>
              </div>
            </div>
          </div>
      )
  }   
}
