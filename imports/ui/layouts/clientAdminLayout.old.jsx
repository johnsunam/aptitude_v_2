import React,{Component} from 'react'
import Sidebar from '../components/common/clientAdminSidebar.jsx';
import Header from '../components/common/clientAdminHeader.jsx';

export default class ClientAdminLayout extends Component {
    constructor(props) {
        super(props)
    }

    choose_reports(id) {
    }

    componentDidMount() {
    }
    render(){
        return(
            <div id="sb-site" style={{minHeight: 1728}}>
                <div id="page-wrapper">
                    <Header/>
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div id="page-content" style={{minHeight: 1650}}>
                            <div className="container">
                                {this.props.content}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
