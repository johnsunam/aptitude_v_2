import React,{Component} from 'react';
import Header from '../container/clientUserHeader.js'
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';
import ClientUserSidebar from '../container/clientUserSidebar.js';
import InnerHeader from '../container/innerHeader.js';

export default class AppLayout extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    }
    
    render(){
        return(<div id="sb-site" style={{minHeight:744}}>
                    <div id="loading" style={{display: "none"}} >
                        <div className="spinner">
                            <div className="bounce1"></div>
                            <div className="bounce2"></div>
                            <div className="bounce3"></div>
                        </div>
                    </div>
                    <div id="page-wrapper">
                        <Header/>
                        <ClientUserSidebar/>
                        <div id="page-content-wrapper">
                            <div id="page-content" style={{"minHeight": 591}}>
                                {this.props.content}
                            </div>
                        </div>
                    </div>
                </div>)
            }   
}
