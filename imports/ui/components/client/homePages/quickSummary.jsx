import React ,{Component} from 'react'
import {PieChart, Pie,Cell,Sector,Tooltip } from 'recharts';
import QuickSummaryPieChart from './quickSummaryPieChart';
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#9ACD32', "#FFFF00", '#F5F5F5', '#40E0D0'];

export default  QuickSummary =  (props)=> {
        var count = 0;
        return (
            <div className="block">
                <div className="block-title">
                    <div className="block-options pull-right">
                    </div>
                    <h2>Quick Summary Notifications</h2>
                </div>
                <div className="row text-center">
                    <div className="col-md-4">
                    <QuickSummaryPieChart data = {props.todayIncident} count = {props.allDailyIncident} title="Total todays Incident"/>
                    <label>Total todays Incident</label>
                    </div>

                    <div className="col-md-4 col-md-offset-2">
                    <QuickSummaryPieChart data = {props.weeklyincidents} count ={props.allWeeklyIncident} title = "Weekly Incidnets"/>
                    <label>Weekly Incidents</label>
                    </div>
                </div>
                <hr/>
                
                <div id="workflow_list" className="col-md-offset-3">
                <h3>Workflow List</h3>
                {props.todayIncident.map((obj,index)=>{
                    return( <div>
                        <p><i className="fa fa-circle" style={{color: `${COLORS[index % COLORS.length]}`}}></i>  {obj.name}</p></div>)
                })}
                </div>
            </div>
        )
    
}