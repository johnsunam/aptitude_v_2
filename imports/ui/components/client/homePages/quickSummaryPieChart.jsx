import React, {Component} from 'react';
import {PieChart, Pie,Cell,Sector,Tooltip } from 'recharts';
import './piechart.css';
import './workflow.css'

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#9ACD32', "#FFFF00", '#F5F5F5', '#40E0D0'];
const RADIAN = Math.PI / 180;                    
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
 	const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x  = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy  + radius * Math.sin(-midAngle * RADIAN);
  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} 	dominantBaseline="central">
    	{`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export default class QuickSummaryPieChart extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(<div className="quick-charts">
        {this.props.count ==0 ? <div id="circle"><label>No incident created.</label></div>:<PieChart width={400} height={200} onMouseEnter={this.onPieEnter}>
        <Pie
          data={this.props.data} 
          cx={300} 
          cy={200} 
          labelLine={false}
          label={renderCustomizedLabel}
          outerRadius={80} 
          fill="#8884d8"
        >
        	{
          	this.props.data.map((entry, index) =>{
              return  <Cell fill={COLORS[index % COLORS.length]}/>})
          }
        </Pie>
      </PieChart>}
    </div>)
    }
}