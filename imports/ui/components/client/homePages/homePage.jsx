import React  from 'react';
import DailyWorkflow from '../../../container/dailyWorkflow.js';
import PersonCard from '../../../container/personCard.js';
import QuickSummary from '../../../container/quickSummary.js';
import TodaysUserActivity from './todaysUserActivity';

export default HomePage = () => {
    return (
        <div>
            <div className="col-sm-6 col-lg-4">
                <QuickSummary/>
                <PersonCard/>
            </div>
            <div className="col-md-6 col-sm-12 col-lg-8">
                <DailyWorkflow/>
            </div>
        </div>
    )
}