import React  from 'react';

export default PersonCard = (props) => {
    return (
        <div>
            <a href="javascript:void(0)" className="widget">
                <div className="widget-content text-center">
                    <img src={props.client? props.client.logo:""} alt="avatar" className="img-circle img-thumbnail img-thumbnail-avatar-2x"/>
                        <h2 className="widget-heading h3 text-muted">{props.client? props.client.companyName.toUpperCase():""}</h2>
                </div>
                <div className="widget-content themed-background-muted text-dark text-center">
                    <strong>Client Admin</strong>
                </div>
                <div className="widget-content">
                </div>
            </a>
        </div>
    )
}