import React, {Component} from 'react';

export default function WorkflowTable(props){
    let url = '/reports/'+props.row.id;
            return(<tr role="row" className="odd">
                        <td className="text-center sorting_1">{props.index}</td>
                        <td>{props.row.name}</td>
                        <td>{props.row.description}</td>
                        <td>{props.row.type}</td>
                        <td>{props.row.total_inc}</td>
                        <td><a href={url}><i className="fa fa-link" aria-hidden="true"></i></a></td>
                    </tr>)
       
    
}