import React, {Component}  from 'react';
import WorkflowTable from './worflowTable';

export default class  DailyWorkflow extends Component {
        constructor(props) {
            super(props);
            this.state = {
                workflowDetails:[]
            }
        }

        componentDidMount() {
            this.setState({workflowDetails: this.props.workflowDetails});
        }
    render() {
        let i = 0;
        return (
        <div className="block full">
            <div className="block-title">
                <h2>Workflows Overview</h2>
            </div>
            <div className="table-responsive">
                <div id="example-datatable_wrapper" className="dataTables_wrapper form-inline no-footer">
                    <div className="row">
                        <div className="col-sm-6 col-xs-5">
                            <div className="dataTables_length" id="example-datatable_length">
                                
                            </div>
                        </div>
                        <div className="col-sm-6 col-xs-7">
                            <div id="example-datatable_filter" className="dataTables_filter">
                                <label>
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Search" onKeyUp={e=>{
                                            if(e.target.value == ""){
                                               this.setState({workflowDetails: this.props.workflowDetails});
                                            } else{
                                                 let searchResult = [];
                                                _.each(this.props.workflowDetails,(obj)=>{
                                                    console.log(obj.name.includes(e.target.value));
                                                    obj.name.includes(e.target.value) ? searchResult.push(obj) :"";
                                                });
                                                console.log(searchResult);
                                                this.setState({workflowDetails:searchResult});
                                            }
                                        }} aria-controls="example-datatable"/>
                                        <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <table id="example-datatable" className="table table-striped table-bordered table-vcenter dataTable no-footer" role="grid" aria-describedby="example-datatable_info">
                    <thead>
                    <tr role="row">
                        <th className="text-center sorting_asc" style={{width : 49}} tabIndex="0"
                            aria-controls="example-datatable" rowSpan="1" colSpan="1" aria-sort="ascending"
                            aria-label="ID: activate to sort column descending">Sign no</th>
                        <th className="sorting" tabIndex="0" aria-controls="example-datatable"
                            rowSpan="1" colSpan="1" aria-label="Task Name: activate to sort column ascending"
                            style={{width : 90}}>Workflow</th>
                        <th className="sorting" tabIndex="0" aria-controls="example-datatable"
                            rowSpan="1" colSpan="1" aria-label="Due date: activate to sort column ascending"
                            style={{width : 77}}>Description</th>
                        <th style={{width : 119}} className="sorting" tabIndex="0" aria-controls="example-datatable"
                            rowSpan="1" colSpan="1" aria-label="Status: activate to sort column ascending">Type</th>
                        <th style={{width : 119}} className="sorting" tabIndex="0" aria-controls="example-datatable"
                            rowSpan="1" colSpan="1" aria-label="Status: activate to sort column ascending">Total Incident</th>
                        <th className="text-center sorting_disabled" style={{width : 74}} rowSpan="1" colSpan="1"
                            aria-label="">Detail View </th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.workflowDetails.map((row)=>{
                        i = i + 1;
                        return (<WorkflowTable row={row} index={i}/>)
                    })}
                        
                   </tbody>
                </table><div className="row"><div className="col-sm-5 hidden-xs"><div className="dataTables_info" id="example-datatable_info" role="status" aria-live="polite"><strong>1</strong>-<strong>1</strong> of <strong>1</strong></div></div><div className="col-sm-7 col-xs-12 clearfix"><div className="dataTables_paginate paging_bootstrap" id="example-datatable_paginate"><ul className="pagination pagination-sm remove-margin"><li className="prev disabled"><a href="javascript:void(0)"><i className="fa fa-chevron-left"></i> </a></li><li className="active"><a href="javascript:void(0)">1</a></li><li className="next disabled"><a href="javascript:void(0)"> <i className="fa fa-chevron-right"></i></a></li></ul></div></div></div></div>
            </div>
        </div>
    )
    }
    
}