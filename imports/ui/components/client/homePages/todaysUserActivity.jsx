import React  from 'react';

export default TodaysUserActivity = () => {
    return (
        <div className="block full">
            <div className="block-title">
                <h2>Today's User Activity</h2>
            </div>
            <div className="table-responsive">
                <div id="example-datatable1_wrapper" className="dataTables_wrapper form-inline no-footer">
                    <div className="row">
                        <div className="col-sm-6 col-xs-5">
                            <div className="dataTables_length" id="example-datatable1_length">
                                <label>
                                    <select name="example-datatable1_length" aria-controls="example-datatable1"
                                            className="form-control">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div className="col-sm-6 col-xs-7">
                            <div id="example-datatable1_filter" className="dataTables_filter">
                                <label>
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Search"
                                               aria-controls="example-datatable1"/>
                                        <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <table id="example-datatable1"
                           className="table table-striped table-bordered table-vcenter dataTable no-footer"
                           role="grid" aria-describedby="example-datatable1_info">
                    <thead>
                    <tr role="row">
                        <th className="text-center sorting_asc"
                            style={{width : 45}} tabIndex="0" aria-controls="example-datatable1"
                            rowSpan="1" colSpan="1" aria-sort="ascending"
                            aria-label="ID: activate to sort column descending">ID</th>
                        <th className="sorting" tabIndex="0" aria-controls="example-datatable1"
                            rowSpan="1" colSpan="1" aria-label="Task Name: activate to sort column ascending"
                            style={{width : 47}}>Task Name</th>
                        <th className="sorting" tabIndex="0" aria-controls="example-datatable1"
                            rowSpan="1" colSpan="1" aria-label="To User: activate to sort column ascending"
                            style={{width : 43}}>To User</th>
                        <th className="sorting" tabIndex="0" aria-controls="example-datatable1" rowSpan="1" colSpan="1"
                            aria-label="Due date: activate to sort column ascending" style={{width : 55}}>Due date</th
                        ><th style={{width : 112}} className="sorting_disabled" rowSpan="1" colSpan="1"
                             aria-label="Status">Status</th>
                        <th className="text-center sorting" style={{width : 69}} tabIndex="0"
                            aria-controls="example-datatable1" rowSpan="1" colSpan="1"
                            aria-label=": activate to sort column ascending"><i className="fa fa-flash"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" className="odd">
                        <td className="text-center sorting_1">1</td>
                        <td><strong>Task1</strong></td>
                        <td><strong>AppUser1</strong></td>
                        <td>25/07/2017</td>
                        <td><span className="label label-info">On hold..</span></td>
                        <td className="text-center">
                            <a href="javascript:void(0)" data-toggle="tooltip" title=""
                               className="btn btn-effect-ripple btn-xs btn-success"
                               style={{overflow: 'hidden', position: 'relative'}}
                               data-original-title="Edit User">
                                <i className="fa fa-pencil"></i>
                            </a>
                            <a href="javascript:void(0)" data-toggle="tooltip" title=""
                               className="btn btn-effect-ripple btn-xs btn-danger"
                               style={{overflow: 'hidden', position: 'relative'}}
                               data-original-title="Delete User">
                                <i className="fa fa-times"></i>
                            </a>
                        </td>
                    </tr></tbody>
                </table>
                    <div className="row">
                        <div className="col-sm-5 hidden-xs">
                            <div className="dataTables_info" id="example-datatable1_info"
                                 role="status" aria-live="polite"><strong>1</strong>-<strong>1</strong> of <strong>1</strong>
                            </div>
                        </div>
                        <div className="col-sm-7 col-xs-12 clearfix">
                            <div className="dataTables_paginate paging_bootstrap"
                                 id="example-datatable1_paginate">
                                <ul className="pagination pagination-sm remove-margin">
                                    <li className="prev disabled">
                                        <a href="javascript:void(0)">
                                            <i className="fa fa-chevron-left"></i>
                                        </a>
                                    </li>
                                    <li className="active">
                                        <a href="javascript:void(0)">1</a>
                                    </li>
                                    <li className="next disabled">
                                        <a href="javascript:void(0)">
                                            <i className="fa fa-chevron-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}