import React ,{Component} from 'react'
import AddClientUser from '../../../container/addClientUser.js'
import crudClass from '../../common/crudClass.js'
//import { Table,SearchColumns, search,sort} from 'reactabular';
import orderBy from 'lodash/orderBy';
import Paginate from '../../common/paginator.jsx'
import ReactTable,{Table,Tr,td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'


export default class ManageClientUser extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className=" col-md-offset-2 col-md-9 col-lg-9">
                <div className="card">
                    <div className="card-header">
                        <h4 className="title">Manage Client Users</h4>
                    </div>
                    <hr/>
                    <div className="card-content table-responsive">
                        <table className="table">
                            <thead>
                            <tr><th>Name</th>
                                <th>Contact No</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Actions</th>
                            </tr></thead>
                            <tbody>
                            {this.props.data.users.map(function(row) {
                                return (
                                    <tr key={row.key}>
                                        <td column="Name">{row.name}</td>
                                        <td column="Phone">{row.contactNo}</td>
                                        <td column="Email">{row.email}</td>
                                        <td column="Address">{row.address}</td>

                                        <td column="Action">
                                            <div >
                                                <ReactTooltip id='edit' type='info'>
                                                    <span>Edit</span>
                                                </ReactTooltip>
                                                <a href={"/client/edit-user/" + row._id} data-tip data-for="edit" data-target={`#${row._id}`} className="round-primary edit" >
                                                    <i className="fa fa-pencil-square-o"></i>
                                                </a>&nbsp;&nbsp;&nbsp;
                                                <div className="modal fade" id={`${row._id}`} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div className="modal-dialog" role="document">
                                                        <div className="modal-content">
                                                            <div className="modal-header">
                                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={e => {
                                                                    $('.modal').fadeOut();
                                                                }}>
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div className="modal-body">
                                                                <AddClientUser edit="true" clientUser={row} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ReactTooltip id='delete'  type='info'>
                                                    <span>Delete</span>
                                                </ReactTooltip>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="#" className="round-danger" data-tip data-for="delete" id={row._id} onClick={(e)=>{
                                                    let obj=new crudClass();
                                                    obj.delete('deleteClientUser',e.target.id)
                                                }}>
                                                    <i className="fa fa-trash-o" id={row._id}></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        )
    }
}
