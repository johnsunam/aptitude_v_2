import React ,{Component} from 'react'
import crudClass from '../../common/crudClass.js'
const message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
import MyInput from '../../common/validator.js'
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
const Confirm = require('react-confirm-bootstrap');
import {FlowRouter} from 'meteor/kadira:flow-router';

export default class AddClientUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            saveResult: false,
            edit: this.props.edit,
            clientUser: this.props.clientUser,
            isShowMessage: false,
            userCode: '',
            dob: '',
            address: '',
            mobile: '',
            email: '',
            secQuestion: '',
            secAnswer: '',
            userTypes: '',
            nationality:'',
            gender:'',
            job_type:'',
            age:'',
            employeed_period:'',
            education:'',
            language:'',
            roles: [],
            companies: [],
            type: '',
            showMessage: '',
            message: '',
            gender:''
            
        }
    }

    componentDidMount(){
        this.state.edit ? this.setState(
            {
                name:this.props.clientUser.name,
                dob:this.props.clientUser.dob,
                address:this.props.clientUser.address,
                contact:this.props.clientUser.contact,
                email:this.props.clientUser.email,
                secQuestion:this.props.clientUser.secQuestion,
                secAnswer:this.props.clientUser.secAnswer,
                roles:this.props.clientUser.roles,
                userTypes:this.props.clientUser.userTypes,
                companies:this.props.clientUser.companies,
                type:this.props.clientUser.type,
                nationality:this.props.clientUser.nationality,
                gender:this.props.clientUser.gender,
                job_type: this.props.clientUser.job_type,
                age: this.props.clientUser.age,
                employeed_period: this.props.clientUser.employeed_period,
                education: this.props.clientUser.education,
                language: this.props.clientUser.language,
            }
        ) : '';
    }

    editClientUser(){

    }
    // saving clientUser to clientUserDb
    confirmTrigger(e) {
        const self = this;
        let name=e.name,
            dob=e.dob,
            address=e.address,
            contact=e.contact,
            email=e.email,
            secQuestion=e.secquest,
            secAnswer=e.secAnswer,
            roles=this.state.roles,
            companies=this.state.companies,
            userTypes = this.state.userTypes,
            type=this.state.type,
            nationality = e.nationality,
            gender = this.state.gender,
            job_type = e.job_type,
            age =  e.age,
            employeed_period =  e.employeed_period,
            education =  e.education,
            language =  e.language;

        let userCode=Random.hexString(7);
        let client=window.sessionStorage.getItem('user');
        let createdBy=window.localStorage.getItem('user');
        let user= client !== 'null' ? client : Meteor.userId();
        let company=window.localStorage.getItem('company');
        console.log(this.props.clientUser);
        let record=this.state.edit?{id:this.state.clientUser._id,data:{companies:companies,nationality: nationality,gender: gender,job_type:job_type,
            age: age,
            employeed_period: employeed_period,
            education: education,
            language: language, name:name,dob:dob,status:status,address:address,contact:contact,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roles:roles, userTypes:userTypes}}:
            {user:user,data:{code:userCode,createdBy:createdBy,name:name,dob:dob,status:status,address:address,nationality: nationality,gender: gender,job_type:job_type,
            age: age,
            employeed_period: employeed_period,
            education: education,
            language: language,contact:contact,email:email,secQuestion:secQuestion, secAnswer:secAnswer,userTypes:userTypes,roles:roles,companies:[company]}}


        if(roles.length !== 0){
            $('#submitform').trigger('click');
            this.setState({record:record})

        }
        else {
            Alert.warning('Add atleast one role', {
                position: 'top-right',
                effect: 'bouncyflip',
                timeout: 1000
            })
        }



    }
    enableButton() {
        this.setState({ canSubmit: true });
    }
    disableButton() {
        this.setState({ canSubmit: false });
    }

    shouldComponentUpdate(nextProps, nextState){
        let self=this;
        Tracker.autorun(function(){
            if(Session.equals('confirm',true)){
                if(Session.get('res')==true){
                    self.setState({showMessage:true,message:"User Registered Sucessfully"})
                    $('.message').addClass('su')
                }else{
                    self.setState({showMessage:true,message:"Email already exits"})
                    $('.message').addClass('er')
                }
                $('select').prop('selectedIndex',0);
                $('#name').val('');
                $('#contactNo').val('');
                $('#secAnswer').val('');
                $('#secquest').val('');
                $('#dob').val('');
                $('#address').val('');

                Session.set('confirm',false)
            }
        });

        return true;
    }
    submit(){
        let obj= new crudClass();
        console.log('submit btn clicked');
        this.state.edit ? obj.create('editClientUser',this.state.record) : obj.create('addClientUser',this.state.record);
        FlowRouter.go('/client/manage-user');
    }

    render(){
        let roles=this.props.data.roles?this.props.data.roles:[];
        let submitButton=<button type="submit" className="btn btn-primary" disabled={!this.state.canSubmit} ><span>Save</span></button>;
        let self = this;
        return (
            <div className="panel">
                <div className="panel-body">
                    <h3>Add User</h3>
                    <div className="example-box-wrapper">
                        <Formsy.Form ref="form" onValidSubmit={this.confirmTrigger.bind(this)} id="addClient" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                        <div className="row">
                            <div className="col-md-6">
                                <MyInput title="User Name" type="text" label="Name"  className=" form-control " ref="name" help="Enter User Name" value={this.state.name} id="name" name="name" placeholder="Name"  required/><br/>
                                <MyInput title="Date of Birth" type="date" name="dob" className=" form-control" help="Enter Birthday" value={this.state.dob} id="dob" placeholder="DOB" ref="dob" required/><br/>
                                <MyInput title="Address" type="text" name="address" help="Enter Address" className=" form-control " value={this.state.address} id="address" placeholder="Address" required ref="address"/><br/>
                                <MyInput title="Contact Number"  name="contactNo" type="number" help="Enter Contact" className=" form-control " id="contactNo" value={this.state.contactNo} placeholder="Contact Number" ref="contactNo"/><br/>
                                <MyInput title="Email Address" required name="email" type="email" className=" form-control " help="Enter Email" id="email" value={this.state.email} placeholder="E-Mail" ref="email"/>
                                <MyInput title= "Nationality"  name="nationality" className= "form-control" help="Enter Nationality" id="nationality" value={this.state.nationality} placeholder="Nationality" ref="nationality"/>
                                <label htmlFor="gender">Gender</label>
                                <select name="gender" onChange={e => self.setState({gender: e.target.value})} id="" className="form-control">
                                    <option value="">choose gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                                <div className="form-group tile-stats" style={{padding: 9, fontSize: 13}}>
                                    <label htmlFor="userType">User Type</label>
                                    <CheckboxGroup name="types" onChange={userTypes => this.setState({userTypes: userTypes})} value={this.state.userTypes}>
                                        <div className="checkbox">
                                            <label>
                                                <Checkbox value="App-User"/>
                                                App-User
                                            </label>
                                        </div>
                                        <div className="checkbox">
                                            <label>
                                                <Checkbox value="client"/>
                                                Admin
                                            </label>
                                        </div>
                                    </CheckboxGroup>
                                </div> 
                            </div>
                            <div className="col-md-6">
                                <MyInput title="Security Question" required name="secquest" type="text" help="Enter Security Question" className=" form-control " value={this.state.secQuestion} id="secquest" placeholder="Security Question" ref="secquest"/><br/>
                                <MyInput title="Security Answer"  required name="secAnswer" type="text" help="Enter Security answer" className=" form-control " value={this.state.secAnswer} id="secAnswer" placeholder="Security Answer" ref="secAnswer"/><br/>
                                <MyInput 
                                    title = "Job Type"
                                    name = "job_type"
                                    type = "text"
                                    help = "Enter your job type or post"
                                    className = "form-control"
                                    value = {this.state.job_type}
                                    placeholder = "job type"
                                    ref = "job_type"
                                /><br/>
                                <MyInput 
                                    title = "Age"
                                    name = "age"
                                    type = "number"
                                    help = "Enter your Age"
                                    className = "form-control"
                                    value = {this.state.age}
                                    placeholder = "Enter your age"
                                    ref = "age"
                                /><br/>
                    
                                <MyInput 
                                    title = "Employeed year"
                                    name = "employeed_period"
                                    type = "number"
                                    help = "Enter your employeed period"
                                    className = "form-control"
                                    value = {this.state.employeed_period}
                                    placeholder = "Enter your employeed period"
                                    ref = "employeed_period"
                                /><br/>

                                <MyInput 
                                    title = "Education level"
                                    name = "education"
                                    type = "text"
                                    help = "Enter your Education level"
                                    className = "form-control"
                                    value = {this.state.education}
                                    placeholder = "Enter your Eductation level"
                                    ref = "education"
                                /><br/>

                                <MyInput 
                                    title = "Language"
                                    name = "language"
                                    type = "text"
                                    help = "Enter your Language"
                                    className = "form-control"
                                    value = {this.state.language}
                                    placeholder = "Enter your language"
                                    ref = "language"
                                /><br/>
                                <div className="form-group" style={{padding: 9, fontSize: 13}}>
                                    <label>Assign Roles</label>
                                    <CheckboxGroup name="roles" value={this.state.roles} onChange={newroles => this.setState({roles: newroles})}>
                                        {roles.map((role)=>{
                                            return(
                                                <div className="checkbox">
                                                    <label>
                                                        <Checkbox value={role}/>
                                                        <span className="checkbox-material"><span className="check"></span></span>
                                                        {role}
                                                    </label>
                                                </div>
                                            )
                                        })}
                                    </CheckboxGroup>
                                </div>
                                <div className="form-group" style={{padding: 9, fontSize: 13}}>
                                    <h5>Added Roles</h5>
                                    <ul className="list-group"  style={{"listStyleType": "none"}}>
                                        {this.state.roles.map((role)=>{
                                            return(
                                                <li className="list-group-item" style={{width: '50%', padding: 5}}>{role}
                                                    <a href="#" id={role} onClick={(e)=>{
                                                        e.preventDefault();
                                                        let roles=this.state.roles;
                                                        let addedRoles=_.without(roles,e.target.id);
                                                        this.setState({roles:addedRoles});
                                                    }}>
                                                        <span style={{float: 'right'}} className="badge">
                                                            <i className="fa fa-times" id={role}></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{float: 'right'}}>
                            {submitButton} &nbsp;&nbsp;
                            <Confirm onConfirm={this.submit.bind(this)}
                                     body="Are you sure you want to submit?"
                                     confirmText="Confirm"
                                     title="Submit Form">
                                <button type="button" id="submitform" className="hidden">Delete Stuff</button>
                            </Confirm>
                            <a className="btn btn-warning"
                               onClick={()=>{
                                   this.props.edit?this.refs.form.reset(this.props.clientUser):this.refs.form.reset();
                                   $('select').prop('selectedIndex',0);
                                   $('#name').val('');
                                   $('#contactNo').val('');
                                   $('#secAnswer').val('');
                                   $('#secquest').val('');
                               }}>Reset</a>
                        </div>
                    </Formsy.Form>
                    </div>
                </div>
            </div>
            )


    }
}
