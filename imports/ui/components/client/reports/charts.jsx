import React,{Component} from 'react';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {LineChart, Line} from 'recharts';
import PieChartData from '../../client/reports/pieChartData';

export default class Charts extends Component{
    constructor(props){
        super(props);
        this.state={
            dateData: '',
            multi: [],
            color: ["#8884d8","#82ca9d","#B8860B","#008B8B","#FF8C00","#8B0000","#9932CC","#1E90FF","#B22222"],
            pieData: [],
            xAxis: this.props.workflow.axis ? this.props.workflow.axis.xAxis: '',
            yAxis: this.props.workflow.axis ? this.props.workflow.axis.yAxis: '',
            yData: '',
            y: [],
            x: []
        };
    }

    componentDidMount(){
        $("#barChart").hide();
        $("#pieChart").hide();
        $("#lineChart").hide();

        _.each(this.props.workflow.charts, (value, key) => {
            let chartToBeShown = $(`#${key}`);
            window.localStorage.getItem('appType')!=='aptitude' ? (value ? chartToBeShown.show(): ''): chartToBeShown.show();
        });
        this.chartData(this.props);
    }

    chartData(props){
        let xAxis=props.workflow.axis ? props.workflow.axis.xAxis: '',
            yAxis=props.workflow.axis ? props.workflow.axis.yAxis: '';
        let chart=props.workflow.charts;
        let datas=props.datas;
        let a=_.pluck(props.datas,xAxis);
        let xData= _.uniq(a);
        let b=_.pluck(props.datas,yAxis);
        let yData=_.uniq(b);
        let pieData=[];
        this.setState({yData: yData});

        let barData=[];
        _.each(xData, singleX => {
            let newd={name: singleX};
            let xd=[];
            _.each(datas, data => {
                if(data[xAxis]===singleX){
                    xd.push(data);
                }
            });

            _.each(yData,(singleY)=>{
                let yd=[];
                _.each(xd, data => {
                    if(data[yAxis]===singleY){
                        yd.push(data);
                    }
                });

                newd[singleY]=yd.length;
            });

            barData.push(newd);

        });

        this.setState({dateData: barData});

        _.each(barData, data => {
            let eachData = [];
            let eachName;
            console.log();
            
            _.map(data, (value, name) => {
                if (name === 'name') eachName = value;
                else {
                    let item = {name: name, value: value};
                    eachData.push(item);
                }
            });
            pieData[eachName] = eachData;
        });

        this.setState({pieData: pieData});

        Object.keys(pieData).forEach(function(key) {
        }, pieData);
    }
    componentWillReceiveProps(nextprops){
        this.chartData(nextprops);
    }

    updateData(e){
         console.log(e.target.value);
        let charts=this.props.workflow.charts;
        charts[e.target.value] =  charts[e.target.value] ? false : true;
        let data={id: this.props.workflow._id,charts: charts};
        Meteor.call('updateWorkflowChart',data);
    }

    render(){
        let self = this;
        let lc=0;
        // const COLORS = ["#8884d8","#82ca9d","#B8860B","#008B8B","#FF8C00","#8B0000","#9932CC","#1E90FF","#B22222"];
        // const RADIAN = Math.PI / 180;
        // const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        //     const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        //     const x  = cx + radius * Math.cos(-midAngle * RADIAN);
        //     const y = cy  + radius * Math.sin(-midAngle * RADIAN);

        //     return (
        //         <text x={x} y={y} fill="white" textAnchor={x > cx  ?  'start' : 'end'} 	dominantBaseline="central">
        //             {`${(percent * 100).toFixed(0)}%`}
        //         </text>
        //     );
        // };

        let count=0;
        let chart=this.props.workflow.charts;
        let chrt=_.keys(chart);
        return(<div className="holder-white">
                <div className="row">
                    <br/>
                    {window.localStorage.getItem('appType') === 'aptitude' ?
                    <div className="row">
                        <div className="col-md-4">
                            <button className="btn btn-primary btn-sm" data-target="#axis" data-toggle="modal" onClick={() => {
                                $('.modal-backdrop').css('z-index','-1');
                            }}>Add x and y axis element</button>
                        </div>
                    </div>: <span></span>}
                    
                

                <div className="col-sm-6" id="barChart">
                    <div className="block full">
                        <div className="block-title">
                            <h2>Barchart</h2>
                        </div>
                        <h3>X-Axis = {this.props.workflow.axis ? this.props.workflow.axis.xAxis:"" } </h3>
                        <h3>Y-Axis = {this.props.workflow.axis ? this.props.workflow.axis.yAxis:"" } </h3>
                        <div id="chart-bars">
                            <BarChart width={600} height={320} data={this.state.dateData}
                                  margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                                <XAxis dataKey="name"/>
                                <YAxis/>
                                <CartesianGrid strokeDasharray="3 3"/>
                                <Tooltip/>
                                <Legend />
                                {this.state.yData.length>0 ? this.state.yData.map((single)=>{
                                    count=count+1;
                                    return(<Bar dataKey={single} fill={this.state.color[count]} />)
                                }): <span>helo</span>}


                            </BarChart>
                        </div>
                    </div>
                </div>

                <div className="col-sm-6 " id="lineChart">
                 <div className="block full">
                        <div className="block-title">
                            <h2>LineChart</h2>
                        </div>
                        <h3>X-Axis = {this.props.workflow.axis ? this.props.workflow.axis.xAxis:"" } </h3>
                        <h3>Y-Axis = {this.props.workflow.axis ? this.props.workflow.axis.yAxis:"" } </h3>
                        <div id="chart-bars">
                            <LineChart width={600} height={320} data={this.state.dateData}
                                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                                <XAxis dataKey="name"/>
                                <YAxis/>
                                <CartesianGrid strokeDasharray="3 3"/>
                                <Tooltip/>
                                <Legend />
                                {this.state.yData.length>0 ? this.state.yData.map((single)=>{
                                    lc=lc+1;

                                    return(<Line type="monotone" dataKey={single} strokeWidth={2}  stroke={this.state.color[lc]} />)

                                }): <span>No datas for chart</span>}
                            </LineChart>
                        </div>
                    </div>
                </div>

                <div className="col-sm-6" id="pieChart">
                    <div className="block full">
                        <div className="block-title">
                            <h2>Piechart</h2>
                        </div>
                        <div id="chart-bars">
                        <PieChartData data={this.state.dateData} xAxis={this.props.workflow.axis ? this.props.workflow.axis.xAxis:""} yAxis={this.props.workflow.axis ? this.props.workflow.axis.yAxis:""}/>                                
                        </div>
                    </div>
                </div>
            </div>

                {/*modal for axis */}
                <div className="modal fade" id="axis" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">

                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Choose axis elements</h4>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <h4>Choose elements for x-axis</h4>
                                    <ul>
                                        {this.props.chartFields.map((field)=>{
                                            return(<li><lable><input type="radio" id="axi" value={field.label} name="x"/>{field.label}</lable></li>)
                                        })}
                                    </ul>
                                </div>
                                <div>
                                    <h4>Choose elements for y-axis</h4>
                                    <ul>
                                        {this.props.chartFields.map((field)=>{
                                            return(<li><lable><input type="radio" id="axi"  value={field.label} name="y"/>{field.label}</lable></li>)
                                        })}
                                    </ul>
                                </div>
                                <button type="button" className="btn btn-primary btn-md" onClick={()=>{
                                    let checkedX = $('input[name="x"]:checked');
                                    let checkedY = $('input[name="y"]:checked');
                                    let xAxis=_.map(checkedX, single => { return single.value; });
                                    let yAxis=_.map(checkedY, single => { return single.value; });
                                    checkedX.attr('checked',false);
                                    checkedY.attr('checked',false);
                                    Meteor.call('updateWorkflowAxis',{id: this.props.workflow._id,xAxis: xAxis[0],yAxis: yAxis[0]});
                                }}>Add element</button>
                                <div>
                                    <div>
                                <span>Check the appropriate chart to show </span>
                                 </div>
                                {chrt.map((value,key)=>{
                                    console.log(chart[value], value);
                                    let checked = chart[value]?<input type="checkbox" name="chart" value={value} onChange={this.updateData.bind(this)} checked />:<input type="checkbox" name="chart" value={value} onChange={this.updateData.bind(this)} />;
                                    return(<label>{checked}{value}&nbsp;&nbsp;&nbsp;</label>)
                                })}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}