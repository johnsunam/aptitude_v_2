import React,{Component} from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import moment from '../../../../../public/lib/moment-2.13.0'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
 import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {PieChart, Pie } from 'recharts';
import Charts from './charts';
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
export default class ClientAdminPages extends Component {
  constructor(props) {
    super(props)
    this.state={table_fields:[],approve:[],defaultFields:[],fields:[],current:[],rowdata:[],type:null,assignClients:[],
      workflowdatas:props.data.workflowdatas,t:'',rules:[],image:'',count:'',dataId:'',images:[],datas:[],
    choosedSearch:[],approveLevel:'',searchFields:[],defaultsData:[],newfields:[],otherfields:[],defield:'',dateData:[],roomData:[],data1:[],data2:[],data3:[],data4:[],currentData:{}}
  }


//search query
  searchForm(arr){
    var newfields=[]
    
    if(arr[0].value!=="" && arr[1].value!==""){
       _.map(this.state.datas,function(single){
      if(arr[0].value!=="" && arr[1].value==""){
        if(moment(single.createdAt).isSameOrAfter(arr[0].value)){
           newfields.push(single)
          }
     }
      else if(arr[0].value==="" && arr[1].value!=="" ){
        if(moment(single.createdAt).isSameOrBefore(arr[1].value))
        newfields.push(single)
      }    
      else{
        if(moment(single.createdAt).isSameOrAfter(arr[0].value) && moment(single.createdAt).isSameOrBefore(arr[1].value))
        newfields.push(single)
      }
   })
  }
else{
  newfields=this.state.datas;
}
     _.each(arr,function(obj){
       console.log(obj)
       var tem=[]
      if(obj.value!=="" && obj.name!=='from' && obj.name!=='to'){
            
            
          _.each(newfields,function(single){
             var arra = obj.value.split(',');
             console.log(arra)
             var n=single[obj.name];
             var a=n.split(',')
             console.log(a)
             _.each(a,function(k){
                _.each(arra,function(sin){
               k==sin?tem.push(single):'';
             })
             })
          })
          newfields=tem;
      }
    })
  // console.log(newfields)
   this.setState({newfields:newfields});
  }


 
  formEvents(props){

	   let self=this;
     let flowchart=props.data.workflow.flowchart;
    
  let steps=flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']
  let pages=steps[0].detail;
  console.log(pages)
  let rules=pages.page.form.rules
  let dataRules=pages.page.form.dataRule;
	self.setState({count:0,rules:rules,dataRule:dataRules})
	let form=JSON.parse(pages.page.form.form);
  }


 //defaults field
 defaultsData(){
var data=_.map(this.state.workflowdatas,function(single){
    var d=single.formdata;
    d.incidentId=single._id;
      return d;
    })
     var forms=this.props.data.workflow.flowchart;
    var table_fields=[]
    var defaults=[];
    var fields=[];
    if(forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']){
      let steps=forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']

      _.each(steps,function(single){
        console.log(single)
        fields=fields.concat(single.detail.page.form.fields)
        defaults=defaults.concat(single.detail.page.form.defaultFields)
       /*  _.map(single.detail.page.form.defaultFields,function(field){
          console.log(field)
           d=_.findWhere(single.detail.page.form.fields,{name:field})
           d!=undefined?defaults.push(_.findWhere(single.detail.page.form.fields,{name:field})):''
          
        })*/
      })


       //fields=fields.concat(pg.form.fields);
       
    }
    console.log(defaults)
    var d=_.map(defaults,function(single){
      console.log(single)
      return single.label;
    })
   // console.log(d)
      d.push("incidentId");
      d.push("createdAt")
      this.setState({defield:d});
     // console.log(data)
     var datas= _.map(data,function(obj){
        var d=obj;
        return _.omit(d,'images')
        /*  _.map(obj,function(value,key){
            if(key!="images"){
              let field=_.findWhere(defaults,{name:key});
              console.log(field)
              field==undefined?d.incidentId=value:d[field.label]=value;
            }
          })*/
      })
        console.log(datas)

        var df=datas[0]?_.keys(datas[0]):[];
        this.setState({newfields:datas});

      this.setState({defaultsData:datas}); 
      this.setState({perfields:datas});
      
}

//get datas function
getDatas(){
  var self=this;
  var data=_.map(this.state.workflowdatas,function(single){

     var d=single.formdata;
    d.createdAt=moment(single.createdAt).format('YYYY-MM-DD')
    d.incidentId=single._id;
   
      return d;
    })    
     var forms=this.props.data.workflow.flowchart;
     //console.log(forms)
    var table_fields=[]
    var fields=[];
    var searchFields=[]
    if(forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']){
      var p=forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
        _.each(p,function(single){
           fields=fields.concat(single.detail.page.form.fields)
           //for search keys
            _.map(single.detail.page.form.searchKeys,function(search){
     //    console.log(search)
          var field=_.findWhere(single.detail.page.form.fields,{name:search.name})
          if(field!==undefined){
            searchFields.push(field);
          }      
       })

       // for default fields
        _.map(single.detail.page.form.defaultFields,function(field){
          var label=_.findWhere(single.detail.page.form.fields,{name:field.name})
          table_fields.push(label);
        })
        })     
        self.setState({searchFields:searchFields});

       
    }

  
     var datas= _.map(data,function(obj){
        return _.omit(obj,'images')
       
      })
     console.log(datas)
      this.setState({datas:datas});  
}

addFields(fields){
  console.log(fields)
   var newf=[];     
   var allDatas=this.state.datas;
            _.map(allDatas,function(data){
              let st={};
               _.map(fields,function(field){  
                 st[field]=data[field];
          })
          newf.push(st);
        })

        console.log(newf)
    this.setState({newfields:newf});
}
//end of getDatas function
componentWillMount(){
this.getDatas();
this.defaultsData();

}


otherDatas(){
var all=_.keys(this.state.datas[0]);
var defaults=_.keys(this.state.defaultsData[0]);
var others=_.difference(all,defaults);
this.setState({otherfields:others})
}


  componentDidMount(){
    this.addFields(this.state.defield);
    var data=_.map(this.props.data.workflowdatas,function(single){
      return single.formdata
    })
    var js=this.props.data.workflow.flowchart
            var page=js['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'][0]['detail']['page']
      $("#showform").formRender({
      dataType: 'json',
      formData:JSON.parse(page.form.form)
    })
    this.formEvents(this.props);
    this.otherDatas()
  }


  componentWillReceiveProps(nextProps) {
 this.formEvents(nextProps)
}

  render(){
    var self=this;
    var action;
           if(this.props.data.workflow.action){
             console.log(this.props.data.user,this.props.data.workflow.action.roles)
            var common=_.intersection(this.props.data.user.roles,this.props.data.workflow.action.roles)
            if(common.length>0){
              action=true;
            }
            else{
              action=false;
            }
           }
           else{
             action=false
           }
           
  	return(<div className="row">
        <div class="col-md-12">
        <h1>
          {self.props.data.workflow.name}
        </h1>
        <div className="box">
    <div className="box-header with-border" style={{"margin":30}} >
            <h3 className="box-title">Monthly Recap Report</h3>
          </div>
          <div className="box-body">
         <div className="row">
             <div className="col-lg-6">
               <ul className="nav  ">
                 <form id="search" role="form" style={{border:0}}>
                   <div className="col-md-6">
                     <ul className="nav nav-stacked">
                       <li><strong>From Date</strong></li>
                       <li>
                        <input className="form-control" name="from" type="date" onChange={(e)=>{
                        this.setState({from:e.target.value})
                      }}/>
                       </li>
                     </ul>
                   </div>

                   <div className="col-md-6" style={{marginButtom:40}}>
                     <ul className="nav nav-stacked">
                       <li><strong>To Date</strong></li>
                       <li>
                          <input className="form-control" name="to" type="date" onChange={(e)=>{
                      this.setState({to:e.target.value});
                    }}/>
                       </li>
                     </ul>
                   </div>
      <a href="#" data-toggle="collapse" data-target="#searchForm">Advance search</a>
   
    <div className="collapse" id="searchForm" style={{marginButtom:10,fontSize:15}}>
    
     
    
     {this.state.searchFields.map((search)=>{
          
        if(search.name.search('select')>-1 && $(`#${search.name} option`).length!=0){
          var options=_.map($(`#${search.name} option`),function(option){
           return(<option value={option.value}>{option.value}</option>)
          })
          
          return (<div><label>{search.label}</label> 
     {/* <span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={search.label}/>Show in charts?</span>*/}
          <select className="form-control" name={search.label}><option value="">choose options</option>{options}</select>
          </div>)
        }

        if(search.name.search('radio')>-1){

          var radios=_.map(document.getElementsByName(search.name),function(radio){
            console.log(radio)
            return(<div><label><input type="radio" name={search.label} value={radio.value}/>{radio.value}</label></div>)
          })
          return(<div><label>{search.label}</label> 
      {/*<span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={`radio-${search.label}`}/>Show in charts?</span>*/}
          {radios}
      </div>)    
        }


        if(search.name.search('textarea')>-1){
          return(<div><label>{search.label}</label>{/*<span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={search.label}/>Show in charts?</span>*/}
          <div><input type="text" className="form-control" name={search.label}/></div></div>)
        }


  
    })}
    </div>
                   <div className="col-md-3">
                     <ul className="nav nav-stacked">
                       <li>&nbsp;</li>
                       <li>
                         <button className="btn ra-100 btn-info"  onClick={()=>{
                                var self=this;
                                let arr=$("#search").serializeArray();
                                this.searchForm(arr);
                     
                      }}><i className="fa fa-search" aria-hidden="true"></i></button>
                        
                       </li>
                     </ul>
                   </div>
                 </form>
               </ul>
             </div>
            
              <div className="col-lg-4">
                <ul className="nav nav-pills pull-right on-off">

                  <li className="mt8 gtext"><strong>Graphical View</strong></li>
                  <li className="active">
                    <a href="#tab_default_1" data-toggle="tab">
                      <b>OFF</b>  </a>
                  </li>
                  <li>
                    <a href="#tab_default_2" data-toggle="tab">
                      <b>ON</b> </a>
                  </li>
                </ul>
              </div>
            </div>
        </div>
         <div className="row">
            <div className="tab-content">
              <div className="tab-pane active" id="tab_default_1">

                  <div className="col-md-12">
                    <div className="holder-white">

                      <div className="table-responsive" id="myTable">
                      <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found."> 
                                        {this.state.newfields.map((single)=>{
                                         
                          return(<Tr className="myTableRow" data={single}>
                          <Td column="Action" className="actionTd" onChange={(e)=>{
                              let id=single.incidentId;
                              this.setState({dataId:id});
                              var self=this;
                                let data=_.findWhere(this.props.data.workflowdatas,{_id:id});
                                this.setState({currentData:data});
                              if(e.target.value==="assign"){
                               
                                $('#assignbutton').trigger('click');
                                $('.modal-backdrop').css('z-index','-1');
                              }
                              
                               if(e.target.value==="approve"){
                                 var user=self.props.data.user.roles; 
                                 console.log(data.approveRoles);
                                 var approvelvl=_.find(data.approveRoles,function(single,index){
                                   
                                   if(single.status==='open'){
                                     self.setState({approveLevel:index});
                                     return single;
                                   }
                                   
                                 });

                                 console.log(approvelvl);
                                 var common=_.intersection(approvelvl.roles,user);
                                 
                                 this.setState({approve:common})
                                $('#apprButton').trigger('click');
                                $('.modal-backdrop').css('z-index','-1');
                              }
                              if(e.target.value==="update"){
                                $('#updateButton').trigger('click');
                                $('.modal-backdrop').css('z-index','-1');
                              }

                          }}>
                          <select className="form-control" id="sel1">
                          <option value="">Actions</option>
                            <option value="assign">Assign</option>
                            <option value="approve">Approve</option>
                            <option value="update">Update</option>
                          </select>
                         </Td>
                          </Tr>)
                        })}  
                      </Table>
                      </div>
                      </div>
                      </div>
                      </div>
                       <div className="tab-pane" id="tab_default_2">
                <div className="col-md-12 mt42">
                    <Charts datas={this.state.newfields} chartFields={this.state.searchFields} workflow={this.props.data.workflow}/>
                  </div>
              </div>
                      </div>
                      </div>
          {/*assign model */}
      <button id="assignbutton" className="hidden" data-toggle="modal" data-target="#assign"></button>
    <div className="modal fade" id="assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel">Assign to User</h4>
      </div>
      <div className="modal-body">  

          {this.state.currentData.action?<div>
            {this.state.currentData.action.status==="in-progress"?<div><h3>In Progress</h3><span>Started At:&nbsp;&nbsp;{this.state.currentData.action.started}</span></div>:""}
            {this.state.currentData.action.status==="assigned"?<h3>Open</h3>:""}
            {this.state.currentData.action.status==="completed"?<div><h1>Completed</h1><span>Complete At:&nbsp;&nbsp;{this.state.currentData.action.finished}</span></div>:<div>
            <div className="form well">
            <label>Task description</label>
            <input type="text" id="taskDescription" className="form-control"/>
            <label>EAT</label>
            <Datetime  id="ETA"/>
             <label>Choose time interval for notification message</label>
            <select className="form-control" id="msgInterval">
            <option>Choose options</option>
           <option value="3">every 3hrs</option>
           <option value="4">every 4hrs</option>
           <option value="4">every 4hrs</option>
           <option value="5">every 5hrs</option>
            </select>
            </div>
            {this.props.data.workflow.roles.map((single)=>{
              return( <label><input type="radio" name="role" onClick={(e)=>{
                console.log(e.target.value)
                var clients=_.filter(this.props.data.clientUsers,function(single){
                  console.log(single);
                  var common=_.intersection([e.target.value],single.roles);
                  return common.length>0;
                })
                this.setState({assignClients:clients})
              }} value={single}/>{single}</label>)
             
            })}{this.state.assignClients.map((single)=>{
              console.log(single)
            return(<div><input type="radio" name="assign" id={single.email} value={single.user}/>{single.name}
            </div>)

          })}<button className="btn btn-primary" onClick={(e)=>{
            console.log(this.state.currentData)
              var formdata=this.state.currentData.formdata;
              if(this.state.currentData.action){
                console.log(this.state.currentData.action)
              }
              formdata=_.omit(formdata,'incidentId','createdAt')
              var user=$('input[name="assign"]:checked').val();
              var role=$('input[name="role"]:checked').val();
              var description=$('#taskDescription').val();
               var email=$('input[name="assign"]:checked').attr('id');
              var ETA=$('.rdt').children()[0];
              var msgInterval=$('#msgInterval').val();
              var assgnedAt=new moment().format('YYYY-MM-DD HH-MM');
             var data={action:{user:user,role:role,name:'assign',status:'assigned',description:description,ETA:ETA.value,msgInterval:msgInterval,lastmsg:new Date(),assgnedAt:assgnedAt},id:this.state.currentData._id};
             var mail={to:email,cc:[],bcc:[],subject:"Finish task at the given time",body:ETA };
           Meteor.call('updateWorkflowAction',data,function(err){
               if(!err){
                 Meteor.call('sendEmail',mail,function(err){
                   if(!err){
                     console.log("message send successful");
                   }
                 })
               }
             })
              
            console.log(formdata)
          }}>Assign</button></div>}
         
            </div>:<div>
             <div className="form well">
            <label>Task description</label>
            <input type="text" id="taskDescription" className="form-control"/>
            <label>EAT</label>
            <Datetime id="ETA"/>
             <label>Choose time interval for notification message</label>
            <select className="form-control" id="msgInterval">
            <option>Choose options</option>
           <option value="3">every 3hrs</option>
           <option value="4">every 4hrs</option>
           <option value="4">every 4hrs</option>
           <option value="5">every 5hrs</option>
            </select>
            </div>{this.props.data.workflow.roles.map((single)=>{
              return( <label><input type="radio" name="role" onClick={(e)=>{
               console.log(e.target.value)
                var clients=_.filter(this.props.data.clientUsers,function(single){
                  console.log(single);
                  var common=_.intersection([e.target.value],single.roles);
                  return common.length>0;
                })
                this.setState({assignClients:clients})
              }} value={single}/>{single}</label>)
             
            })}{this.state.assignClients.map((single)=>{
              console.log(single)
            return(<div><input type="radio" name="assign" id={single.email} value={single.user}/>{single.name}
            </div>)

          })}<button className="btn btn-primary" onClick={(e)=>{
              var formdata=this.state.currentData.formdata;
              formdata=_.omit(formdata,'incidentId','createdAt')
              var user=$('input[name="assign"]:checked').val();
              var role=$('input[name="role"]:checked').val();
              var description=$('#taskDescription').val();
              var ETA=$('.rdt').children()[0];
              var msgInterval=$("#msgInterval").val();
              console.log(ETA,ETA.value);
              var email=$('input[name="assign"]:checked').attr('id');
              var assgnedAt=new moment().format('YYYY-MM-DD HH-MM');
              console.log(moment(ETA.value));
             var data={action:{user:user,name:'assign',role:role,status:'assigned',description:description,ETA:ETA.value,msgInterval:ETA.value,lastmsg:new Date(),assgnedAt:assgnedAt},id:this.state.currentData._id};
             var mail={to:email,cc:[],bcc:[],subject:"Finish task at the given time",body:ETA.value };
           Meteor.call('updateWorkflowAction',data,function(err){
                 if(!err){
                   Meteor.call('sendEmail',mail,function(err){
                     if(!err){
                       console.log("message send successful");
                     }
                   })
                   $('input[name="assign"]:checked').val("")
                   $('input[name="role"]:checked').val("");
                   $('#taskDescription').val("")
                   $('.rdt').children()[0].val("");
                 }
               });

          }}>Assign</button></div>}
                  
         
      </div>
      <div className="modal-footer">
             </div>
    </div>
    </div></div>
      
      {/*FormData*/}
      <button id="updateButton" className="" data-toggle="modal" data-target="#update"></button>
       <div className="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel"></h4>
      </div>
      <div className="modal-body">
      <div className="col-md-12">
          <form id="showform" className="hidden">
          </form> 
      <div>
      {this.state.currentData.formdata?_.map(this.state.currentData.formdata,function(value,key){
          if(key!=="images"){
             return(<div><label>{key}&nbsp;&nbsp;:</label>&nbsp;&nbsp;&nbsp;{value}</div>)
          }
           
      }):""}
      </div>
       <div className="images">
       <h2>Uploaded Images</h2>
           {this.state.currentData.formdata?this.state.currentData.formdata.images.map((image)=>{
          return(<div className="col-xs-6 col-md-3">
    <a href="#" className="thumbnail">
      <img src={image} alt="..."/>
    </a>
  </div>)  }):""}
          </div>
           </div>
           <hr/>
          <div>
          <br/>
          <label>Remark</label>
          <textarea type="textarea" className="form-control" value={this.state.currentData.remark}  id="remark"></textarea>
          <a className="btn btn-primary" id="addRemark" onClick={(e)=>{
            var remark=$('#remark').val();
            Meteor.call('addRemark',{id:this.state.currentData._id,remark:remark},function(err){
              if(!err){
                console.log('updated');
                $('#remark').val("")
              }
            })
          }}>Submit</a>
          </div>
      </div>
      <div className="modal-footer">
             </div>
    </div>
    </div></div>

    {/*approve Modal*/}
    <button id="apprButton" className="" data-toggle="modal" data-target="#approve"></button>
      <div className="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel">Approve Data</h4>
      </div>
      <div className="modal-body">
      {this.state.approve.length>0?<button id="as" onClick={()=>{
        console.log(this.state.currentData,this.state.approveLevel);
        this.state.currentData.approveRoles[this.state.approveLevel]['status']='close';
        console.log(this.state.currentData);
        //var newRoles=_.difference(this.state.currentData.approve,this.state.approve);  
        Meteor.call('updateWorkflowDataApprove',{id:this.state.currentData._id,approveRoles:this.state.currentData.approveRoles},function(err){
            if(!err)
            {
              $('#as').text('Approved');
              $('#as').attr('disabled',true);
            }
       })     
      }} className="btn btn-success btn-large">Approve</button>:<h4>Do not have approve authorization</h4>}

      
          <div>
          <br/>
       
         
          </div>
          
          
          
      </div>
      <div className="modal-footer">
             </div>
    </div>
    </div></div>

      {/*detail view*/}
      <button id="modalclick" data-toggle="modal" data-target="#mymodals" className="hidden"></button>
         <div className="modal fade" id="mymodals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel">Choose other fields</h4>
      </div>
      <div className="modal-body">
        <CheckboxGroup name="fruits" value='' onChange={(fields)=>{
         
          let d=this.state.defield;
          let newf=d.concat(fields);
          console.log(newf)
           this.addFields(newf);    
        }}>
        <ul style={{listStyleType:"none"}}>
        {this.state.otherfields.map((single)=>{
          return(<li><label><Checkbox value={single} Checked/>{single}</label></li>)
        })}
       
        </ul>
        </CheckboxGroup>
      </div>
      <div className="modal-footer">
             </div>
    </div>
  </div>
</div>
      </div>
    </div>
    </div>  
  	)
  }
}