import React,{Component} from 'react';
import { PieChart, Pie, Sector, Cell, Tooltip, Legend} from 'recharts';

const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
                  {name: 'Group C', value: 300}, {name: 'Group D', value: 200}];
const COLORS = ["#8884d8","#82ca9d","#B8860B","#008B8B","#FF8C00","#8B0000","#9932CC","#1E90FF","#B22222"];
const RADIAN = Math.PI / 180;  
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    console.log(cx);
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      const x  = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy  + radius * Math.sin(-midAngle * RADIAN);
      return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
            </text>
            );
        };


export default class PieChartData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pieDatas:[],
            data:[]
        }
    }

    componentDidMount() {
        this.pieData(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this.pieData(nextProps.data);
    }

    pieData(data){
        let pieDatas = [];
        _.each(data,(single)=>{
             let datas = [];
          _.each(single,(value, key)=>{
                if(key != 'name')
                datas.push({name:key, value:value});
                else 
                pieDatas.push({datas:datas, axis: value})
            })
        })
        this.setState({pieDatas:pieDatas});
    }

    render() {
        return (<div>
            <PieChart width={1200} height={355} onMouseEnter={this.onPieEnter}>
                <Pie
                data={this.state.data} 
                cx={300} 
                cy={200} 
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={80} 
                fill="#8884d8"
                >
                    {
                    this.state.data.map((entry, index) => {
                        return(<Cell fill={COLORS[index % COLORS.length]}/>)
                        })
                }
                </Pie>
            </PieChart>

            <h3>{this.props.yAxis}</h3>
            {this.state.data.map((obj,index)=>{
                console.log(obj);
                        return( <div>
                            <p><i className="fa fa-circle" style={{color: `${COLORS[index % COLORS.length]}`}}></i>  {obj.name}</p></div>)
                    })} 

            <h3>{this.props.xAxis}</h3>

            {this.state.pieDatas.map((obj, index)=>{
                return(<div><label>
                            <input name="axis" type = "radio"
                                onClick={()=>{
                                    this.setState({data:obj.datas});
                                }}
                            />{obj.axis}</label></div>)
            })}
        </div>)
    }
}