import React,{Component} from 'react';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {PieChart, Pie,Cell,Sector } from 'recharts';
import {LineChart, Line} from 'recharts'
export default class Charts extends Component{
    constructor(props){
        super(props);
        this.state={dateData:'',
            multi:[],
            color:["","#8884d8","#82ca9d","#B8860B","#008B8B","	#FF8C00","#8B0000","#9932CC","#1E90FF","#B22222"],
            pieData:[],
            xAxis:this.props.workflow.axis?this.props.workflow.axis.xAxis:'',
            yAxis:this.props.workflow.axis?this.props.workflow.axis.yAxis:'',
            yData:'',
            y:[],
            x:[]
        };
    }

    componentDidMount(){
        $("#barChart").hide();
        $("#pieChart").hide();
        $("#lineChart").hide();
        _.each(this.props.workflow.charts, (value, key) => {
            let chartToBeShown = $(`#${key}`);
            window.localStorage.getItem('appType')!=='aptitude'?(value?chartToBeShown.show():''):chartToBeShown.show();
        });
        this.chartData(this.props);

        // let x;
        // let y;
        // if(window.localStorage.getItem('appType')=='aptitude'){
        // x=this.props.chartFields.map((field)=>{
        //    return field.label;
        //  });

        //  y=this.props.chartFields.map((field)=>{
        //    return field.label;
        //  });
        // }else{

        //   x=this.props.workflow.axis?this.props.workflow.axis.xAxis:this.props.chartFields.map((field)=>{
        //    return field.label;
        //  });
        //  y=this.props.workflow.axis?this.props.workflow.axis.yAxis:this.props.chartFields.map((field)=>{
        //    return field.label;
        //  });
        // }
        // x.sort();
        // y.sort();
        // this.setState({x:x,y:y});

    }
    chartData(props){
        let xAxis=props.workflow.axis?props.workflow.axis.xAxis:'',
            yAxis=props.workflow.axis?props.workflow.axis.yAxis:'';
        let chart=props.workflow.charts;
        let datas=props.datas;
        let a=_.pluck(props.datas,xAxis)
        let xData= _.uniq(a);
        let b=_.pluck(props.datas,yAxis)
        let yData=_.uniq(b);
        let pieData=[];
        this.setState({yData:yData});
        let barData=[];
        _.each(xData,function(singleX){
            let newd={name:singleX};
            let key='"'+xAxis+'"';

            let val='"'+singleX+'"';

            let xd=[];
            _.each(datas,function(data){
                if(data[xAxis]===singleX){
                    xd.push(data);
                }
            })
            // let xd=_.where(datas,{key:val});
            _.each(yData,(singleY)=>{
                let yd=[];
                _.each(xd,function(data){
                    if(data[yAxis]===singleY){
                        yd.push(data);
                    }
                })
                // let yd=_.where(xd,{yAxis:singleY});
                newd[singleY]=yd.length;

                if(xData.length==1){
                    console.log(xData.length)
                    if(chart['pieChart']==true){

                        pieData.push({'name':singleY,'value':yd.length});

                    }
                }


            })
            barData.push(newd);
            //          console.log(newd);
        })
        this.setState({pieData:pieData});
        this.setState({dateData:barData})

    }
    componentWillReceiveProps(nextprops){
        this.chartData(nextprops);
    }

    updateData(e){
        // console.log(this)
        let charts=this.props.workflow.charts;
        $.each($("input[name='chart']:not(:checked)"),function(){
            charts[$(this).val()]=true
        })
        $.each($("input[name='chart']:checked"),function(){
            charts[$(this).val()]=false
        })
        let data={id:this.props.workflow._id,charts:charts}
        Meteor.call('updateWorkflowChart',data);
    }

    render(){
        let count=0;
        let lc=0;
        const COLORS = ["#8884d8","#82ca9d","#B8860B","#008B8B","#FF8C00","#8B0000","#9932CC","#1E90FF","#B22222"];
        const RADIAN = Math.PI / 180;
        const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
            console.log(cx, cy, midAngle, innerRadius, outerRadius, percent, index)
            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
            const x  = cx + radius * Math.cos(-midAngle * RADIAN);
            const y = cy  + radius * Math.sin(-midAngle * RADIAN);

            return (
                <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} 	dominantBaseline="central">
                    {`${(percent * 100).toFixed(0)}%`}
                </text>
            );
        };

        let self=this;
        let count=0;
        let chart=this.props.workflow.charts;
        let chrt=_.keys(chart);

        return(<div className="holder-white">
                <div className="row">
                    {window.localStorage.getItem('appType')=='aptitude'?<div>
                        <div className="col-md-4">
                            <h4>Choose data for X-axis</h4>
                            <select className="form-control" onChange={(e)=>{
                                //  console.log(e.target.value)
                                this.setState({xAxis:e.target.value})
                            }}  id="first">
                                <option value="">Choose field</option>
                                {this.state.x.map((field)=>{
                                    return(<option value={field}>{field}</option>)
                                })}
                            </select>
                        </div>

                        <div className="col-md-4">
                            <h4>Choose data for Y-axis</h4>
                            <select className="form-control" onChange={(e)=>{
                                this.setState({yAxis:e.target.value})
                            }} id="second">
                                <option value="">Choose field</option>

                                {this.state.y.map((field)=>{
                                    return(<option value={field}>{field}</option>)
                                })}
                            </select>
                        </div>
                    </div>:<span></span>}


                    <div className="col-md-2">
                        <a className="btn btn-primary" onClick={()=>{
                            //   let {xAxis,yAxis}=this.state;
                            //    let datas=this.props.datas;
                            //    let a=_.pluck(this.props.datas,xAxis)
                            //     let xData= _.uniq(a);
                            //     let b=_.pluck(this.props.datas,yAxis)
                            //     let yData=_.uniq(b);
                            //     let pieData=[];
                            //     this.setState({yData:yData});
                            //     let barData=[];
                            //      _.each(xData,function(singleX){
                            //        let newd={name:singleX};
                            //        let key='"'+xAxis+'"';

                            //        let val='"'+singleX+'"';

                            //        let xd=[];
                            //        _.each(datas,function(data){
                            //          if(data[xAxis]===singleX){
                            //            xd.push(data);
                            //          }
                            //        })
                            //    // let xd=_.where(datas,{key:val});
                            //     _.each(yData,(singleY)=>{
                            //        let yd=[];
                            //        _.each(xd,function(data){
                            //          if(data[yAxis]===singleY){
                            //            yd.push(data);
                            //          }
                            //        })
                            //      // let yd=_.where(xd,{yAxis:singleY});
                            //       newd[singleY]=yd.length;

                            //   if(xData.length==1){
                            //     console.log(xData.length)
                            //     if(chart['pieChart']==true){

                            //      pieData.push({'name':singleY,'value':yd.length});

                            //   }
                            //   }


                            //     })
                            //     barData.push(newd);
                            //    //          console.log(newd);
                            //  })
                            //  console.log(pieData)
                            //  this.setState({pieData:pieData});
                            //   this.setState({dateData:barData})

                        }}>Show chart</a>
                    </div>

                    {window.localStorage.getItem('appType')=='aptitude'?<div>
                        <button className="btn btn-primary btn-sm" data-target="#axis" data-toggle="modal">Add x and y axis element</button>
                        <div><span>Check the propriate chart to show </span></div>
                        {chrt.map((value,key)=>{
                            return(<label><input type="checkbox" name="chart" value={value} onChange={this.updateData.bind(this)}/><key>{value}</key>&nbsp;&nbsp;&nbsp;</label>)
                        })}
                    </div>:<span></span>}

                </div>

                <div className="col-lg-6" id="barChart">
                    <h2>Barchart </h2>
                    <BarChart width={600} height={320} data={this.state.dateData}
                              margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        {this.state.yData.length>0?this.state.yData.map((single)=>{
                            count=count+1;
                            return(<Bar dataKey={single} fill={this.state.color[count]} />)
                        }):<span>helo</span>}


                    </BarChart>
                </div>
                <div className="col-lg-6 " id="lineChart">
                    <h2>LineChart</h2>
                    <LineChart width={600} height={320} data={this.state.dateData}
                               margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        {this.state.yData.length>0?this.state.yData.map((single)=>{
                            lc=lc+1;

                            return(<Line type="monotone" dataKey={single} strokeWidth={2}  stroke={this.state.color[lc]} />)

                        }):<span>No datas for chart</span>}
                    </LineChart>
                </div>
                <div className="col-lg-6" id="pieChart">
                    <h2>PieChart</h2>
                    <PieChart width={800} height={400}>
                        <Pie
                            data={this.state.pieData}
                            cx={300}
                            cy={200}
                            labelLine={false}
                            label={renderCustomizedLabel}
                            outerRadius={80}
                            fill="#8884d8"
                        >
                            {
                                this.state.pieData.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                            }
                        </Pie>
                        <Tooltip/>
                    </PieChart>
                </div>

                {/*modal for axis */}
                <div className="modal fade" id="axis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">

                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Choose axis elements</h4>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <h4>Choose elements for x-axis</h4>
                                    <ul>
                                        {this.props.chartFields.map((field)=>{
                                            return(<li><lable><input type="radio" id="axi" value={field.label} name="x"/>{field.label}</lable></li>)
                                        })}
                                    </ul>
                                </div>
                                <div>
                                    <h4>Choose elements for y-axis</h4>
                                    <ul>
                                        {this.props.chartFields.map((field)=>{
                                            return(<li><lable><input type="radio" id="axi"  value={field.label} name="y"/>{field.label}</lable></li>)
                                        })}
                                    </ul>
                                </div>
                                <button type="button" className="btn btn-primary btn-md" onClick={()=>{
                                    let xAxis=_.map($('input[name="x"]:checked'),function(single){
                                        return single.value
                                    })
                                    let yAxis=_.map($('input[name="y"]:checked'),function(single){
                                        return single.value;
                                    })
                                    console.log(xAxis[0],yAxis[0]);
                                    $('input[name="x"]:checked').attr('checked',false);
                                    $('input[name="y"]:checked').attr('checked',false);
                                    Meteor.call('updateWorkflowAxis',{id:this.props.workflow._id,xAxis:xAxis[0],yAxis:yAxis[0]});
                                }}>Add element</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}