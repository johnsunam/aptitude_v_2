import React,{Component} from 'react';
import {Table,Tr,Td,Th} from 'reactable';


export default class ReportTable extends Component {
    constructor(props) {
        super(props);
        this.state ={
            currentData:{}
        }
    }

    render() {
        return(<div><Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found.">
            {this.props.rows.map((single, index)=> {
                return (<Tr className="myTableRow" data={single}>
                        <Td column="Action" className="actionTd" onChange={ e=> {
                            let incident = this.props.datas[index]['_id'];
                            this.props.actionTrigger(e.target.value, incident)
                            } }>
                        <select className="form-control" id="sel1">
                            <option value="">Actions</option>
                            {this.props.workflow.action != null ?<option value="assign">Assign</option>:''}
                            {this.props.workflow.approve != null ?<option value="approve">Approve</option>:''}
                            <option value="update">Update</option>
                        </select>
                        </Td>
                        <Td column="Incident Detail" className="incident_detail">
                            <a href="#" data-toggle="modal" onClick={e=>{
                                let data = this.props.datas[index]['formdata'];
                                this.setState({currentData:data});
                            }} data-target="#show_details">Incident Detail</a>
                        </Td>
                </Tr>)
            })}

        </Table>
         {/* show details */}
         <div className="modal fade" id="show_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 className="modal-title" id="myModalLabel">Filled Data</h4>
                    </div>
                <div className="modal-body">
                    <div>
                        {_.map(this.state.currentData, (value,key) => {
                            if(key!=="images" && key!=='createdAt' && key!=='incidentId' && key!=='user' && key!=='roles'){
                            return(<div><div className=" question"><i className="fa fa-chevron-right"></i><label>{key}</label></div><div className="answer">&nbsp;&nbsp;{value}</div></div>)
                            }
                        })}
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>)
    }
}