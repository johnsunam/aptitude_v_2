import React,{Component} from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import moment from '../../../../../public/lib/moment-2.13.0'
import {Table,Tr,Td,Th} from 'reactable';
import Charts from './charts';
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import ReportTable from '../../../container/reportTable';

export default class ClientAdminPages extends Component {

    constructor(props) {
        console.log('fdfdfdf');        
        super(props);
        this.state = {
            table_fields:[],
            approve:[],
            defaultFields:[],
            fields:[],
            current:[],
            rowdata:[],
            type:null,
            assignClients:[],
            workflowdatas:props.data.workflowdatas,
            t:'',
            rules:[],
            image:'',
            count:'',
            dataId:'',
            images:[],
            datas:[],
            choosedSearch:[],
            approveLevel:'',
            searchFields:[],
            defaultsData:[],
            newfields:[],
            otherfields:[],
            defield:'',
            dateData:[],
            roomData:[],
            data1:[],
            data2:[],
            data3:[],
            data4:[],
            currentData:{}
        }
    }

    searchForm(arr){
        let newfields=[];
        if(arr[0].value!=="" && arr[1].value!==""){
            _.map(this.state.datas, single => {
                if(arr[0].value!=="" && arr[1].value===""){
                    if(moment(single.createdAt).isSameOrAfter(arr[0].value)){
                        newfields.push(single)
                    }
                }
                else if(arr[0].value==="" && arr[1].value!=="" ){
                    if(moment(single.createdAt).isSameOrBefore(arr[1].value))
                        newfields.push(single)
                }
                else{
                    if(moment(single.createdAt).isSameOrAfter(arr[0].value) && moment(single.createdAt).isSameOrBefore(arr[1].value))
                        newfields.push(single)
                }
            })
        }
        else{
            newfields=this.state.datas;
        }

        _.each(arr, obj => {
            let tem=[];
            if(obj.value!=="" && obj.name!=='from' && obj.name!=='to'){
                _.each(newfields, single => {
                    let arra = obj.value.split(',');
                    let n=single[obj.name];
                    let a=n.split(',');
                    _.each(a, k => {
                        _.each(arra, sin => k===sin?tem.push(single):'')
                    })
                });
                newfields=tem;
            }
        });

        this.setState({newfields:newfields});
    }

    formEvents(props){
        let self=this;
        let flowchart=props.data.workflow.flowchart;
        let steps=flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
        let pages=steps[0].detail;
        let rules=pages.page.form.rules;
        let dataRules=pages.page.form.dataRule;
        console.log(dataRules);
        self.setState({count:0,rules:rules,dataRule:dataRules});
    }

    defaultsData(){
        console.log('fdfdfdf');        
        
        const self = this;
        console.log(this.state.workflowdatas);
        let data=_.map(this.state.workflowdatas, single => {
             let d=single.formdata;
             d.createdAt=moment(single.createdAt).format('YYYY-MM-DD');
             d.incidentId=single._id;

            if (self.props.data.workflow.type != 'public') {

                let user = _.findWhere(self.props.data.incidentUser, {user: single.user});
                    d.user = user.name;
                    d.roles = user.roles.toString();
                
            } else {
                d.user = 'other user';
                d.roles = 'participants';
            }               
            // let user = _.findWhere(this.props.data.incidentUser, {user: single.user});
            // let d=single.formdata;
            // d.incidentId=single._id;
            // d.user = user.name;
            // d.roles = user.roles.toString();
            return d;
        });

        let forms=this.props.data.workflow.flowchart;
        let defaults=[];
        let fields=[];

        if(forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']){
            let steps=forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
            _.each(steps, single => {
                fields=fields.concat(single.detail.page.form.fields);
                defaults=defaults.concat(single.detail.page.form.defaultFields)
            })
        }

        let d=_.map(defaults, single => {
            return single.label;
        });
        d.push("incidentId");
        d.push("createdAt");
        d.push("user");
        d.push("roles");
        this.setState({defield:d});
        let datas= _.map(data, obj => {
            return _.omit(obj,'images')
        });
        this.setState({newfields:datas});
        this.setState({defaultsData:datas});
        this.setState({perfields:datas});
    }

    getDatas(){
        let self=this;
        let data=_.map(this.state.workflowdatas, single => {
            let d =single.formdata;
            d.createdAt=moment(single.createdAt).format('YYYY-MM-DD');
            d.incidentId=single._id;
            if (self.props.data.workflow.type != 'public') {
                let user = _.findWhere(self.props.data.incidentUser, {user: single.user});
                    d.user = user.name;
                    d.roles = user.roles.toString();
                
            } else {
                d.user = 'other user';
                d.roles = 'participants';
            }
                return d;
        });
        
        let forms=this.props.data.workflow.flowchart;
        let table_fields=[];
        let fields=[];
        let searchFields=[];

        if(forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']){
            let p=forms['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
            _.each(p, single => {
                fields=fields.concat(single.detail.page.form.fields);
                //for search keys
                _.map(single.detail.page.form.searchKeys, search => {
                    let field=_.findWhere(single.detail.page.form.fields,{name:search.name});
                    if(field!==undefined){
                        searchFields.push(field);
                    }
                });
                // for default fields
                _.map(single.detail.page.form.defaultFields, field => {
                    let label=_.findWhere(single.detail.page.form.fields,{name:field.name});
                    table_fields.push(label);
                })
            });
            self.setState({searchFields:searchFields});
        }
        let datas= _.map(data, obj => {
            return _.omit(obj,'images')
        });
        this.setState({datas:datas});
    }

    addFields(fields){
        let newf=[];
        let allDatas=this.state.datas;
        _.map(allDatas, data => {
            let st={};
            _.map(fields, field => {
                st[field]=data[field];
            });
            newf.push(st);
        });
        this.setState({newfields:newf});
    }

    actionTrigger (value, incident){
        let id=incident;
        this.setState({dataId:id});
        let self=this;
        let data=_.findWhere(this.props.data.workflowdatas,{_id:id});
        this.setState({currentData:data});

        if(value==="assign"){

            $('#assignbutton').trigger('click');
            $('.modal-backdrop').css('z-index','-1');
        }

        if(value==="approve"){
            let user=self.props.data.user.roles;
            let approvelvl=_.find(data.approveRoles, (single, index) => {
                if(single.status==='open'){
                    self.setState({approveLevel:index});
                    return single;
                }
            });

            let common=approvelvl ? _.intersection(approvelvl.roles,user):[];
            this.setState({approve:common});
            $('#apprButton').trigger('click');
            $('.modal-backdrop').css('z-index','-1');
        }

        if(value==="update"){
            
            $('#updateButton').trigger('click');
            $('.modal-backdrop').css('z-index','-1');
        }

    }

    componentWillMount(){
        this.getDatas();
        this.defaultsData();
    }


    otherDatas(){
        let all=_.keys(this.state.datas[0]);
        let defaults=_.keys(this.state.defaultsData[0]);
        let others=_.difference(all,defaults);
        this.setState({otherfields:others})
    }

    componentDidMount(){
        console.log('sdfsdf');
        this.addFields(this.state.defield);
        let js=this.props.data.workflow.flowchart;
        let page=js['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'][0]['detail']['page'];
        $("#showform").formRender({
            dataType: 'json',
            formData:JSON.parse(page.form.form)
        });
        this.formEvents(this.props);
        this.otherDatas();
    }
    componentWillReceiveProps(nextProps) {
        this.formEvents(nextProps)
    }

    render(){
        let self=this;
        return(
                <div>
                    <div className="row">
                        <div className="block" style={{minHeight:186}}>
                            <div className="block-title">
                                <h2>Filters</h2>
                            </div>
                            <form  className="form-horizontal form-bordered" id = "search">
                                <div className="form-group ">
                                    <label className="col-md-3 control-label" for="example-daterange1">Date Range</label>
                                    <div className="col-md-9">
                                        <div className="input-group input-daterange" data-date-format="mm/dd/yyyy">
                                            <input className="form-control" 
                                                name="from" type="date" onChange={(e)=>{
                                                let arr=$("#search").serializeArray();
                                                this.searchForm(arr);
                                                this.setState({from:e.target.value})
                                                }} id="example-daterange1" placeholder="From" />
                                                <span className="input-group-addon"><i className="fa fa-chevron-right"></i></span>
                                                <input className="form-control" name="to" type="date" onChange={(e)=>{
                                                    this.setState({to:e.target.value});
                                                    let arr=$("#search").serializeArray();
                                                    this.searchForm(arr);
                                                    }} 

                                                    id="example-daterange2" 
                                                    placeholder="To" />
                                        </div>
                                    </div>
                                </div>
                            <br/>
                            <div className="col-md-8"> &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#" className="btn btn-effect-ripple btn-primary" 
                                    style={{overflow: "hidden", position: "relative"}} data-toggle="collapse" data-target="#searchForm">
                                    <span className="btn-ripple animate" style={{height: 132,width: 132,top: -56, left: -42.9219}}></span>
                                                        Advance search
                                </a>
                            </div>
                           
                            <div className="col-md-2">
                                Graphical View
                                <ul className="nav nav-pills pull-right on-off">
                                    
                                    <li className="active">
                                        <a href="#tab_default_1" data-toggle="tab">
                                            <b>OFF</b>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_default_2" data-toggle="tab">
                                            <b>ON</b> 
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <br/>
                            <br/>
                            <div className="collapse" id="searchForm" style={{marginButtom:10,fontSize:15}}>
                                    {this.state.searchFields.map((search)=>{
                                            let searchName = $(`#${search.name}`).find(`option`);
                                            if(search.name.search('select')>-1 && searchName.length!==0){
                                            let options=_.map(searchName, option => {
                                            return(<option value={option.value}>{option.value}</option>)
                                            });
                                        return (<div><label>{search.label}</label><select className="form-control" name={search.label}><option value="">choose options</option>{options}</select></div>)}
                                        if(search.name.search('radio')>-1){
                                                                    let radios=_.map(document.getElementsByName(search.name), radio => {
                                                                        return(<div><label><input type="radio" name={search.label} value={radio.value}/>{radio.value}</label></div>)
                                                                    });
                                                                    return(<div><label>{search.label}</label>
                                                                        {/*<span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={`radio-${search.label}`}/>Show in charts?</span>*/}
                                                                        {radios}
                                                                    </div>)
                                                                }
                                                                if(search.name.search('textarea')>-1){
                                                                    return(<div><label>{search.label}</label>{/*<span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={search.label}/>Show in charts?</span>*/}
                                                                        <div><input type="text" className="form-control" name={search.label}/></div></div>)
                                                                }
                                                            })}
                                
                                <button className="btn ra-100 btn-info"  
                                    onClick={(e)=>{
                                        e.preventDefault();
                                        let arr=$("#search").serializeArray();
                                        this.searchForm(arr);}}><i className="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </div>
                            </form> 
                        </div>
                        <div className="col-md-12 col-sm-12 col-lg-12">
                            <div className="block full">
                                <div className="block-title">
                                    <h2>{self.props.data.workflow.name}</h2>
                                </div>
                                <div className="row">
                                    <div className="tab-content">
                                        <div className="tab-pane active" id="tab_default_1">
                                            <div className="table-responsive">
                                                <div id="example-datatable_wrapper" className="dataTables_wrapper form-inline no-footer">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-xs-5">
                                                            <div className="dataTables_length" id="example-datatable_length">
                                                                <label>
                                                                    <select name="example-datatable_length" aria-controls="example-datatable" className="form-control">
                                                                        <option value="5">5</option><option value="10">10</option><option value="20">20</option>
                                                                    </select>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-xs-7">
                                                            <div id="example-datatable_filter" className="dataTables_filter">
                                                                <label>
                                                                    <div className="input-group">
                                                                        <input type="search" className="form-control" placeholder="Search" aria-controls="example-datatable"/>
                                                                            <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ReportTable datas={this.props.data.workflowdatas} incidentUser={this.props.data.incidentUser} workflow={this.props.data.workflow} actionTrigger={this.actionTrigger.bind(this)}/>
                                                    
                                                    <div className="row">
                                                        <div className="col-sm-5 hidden-xs">
                                                            <div className="dataTables_info" id="example-datatable_info" role="status" aria-live="polite"><strong>1</strong>-<strong>1</strong> of <strong>1</strong>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-7 col-xs-12 clearfix">
                                                            <div className="dataTables_paginate paging_bootstrap" id="example-datatable_paginate">
                                                                <ul className="pagination pagination-sm remove-margin">
                                                                    <li className="prev disabled"><a href="javascript:void(0)"><i className="fa fa-chevron-left"></i> </a></li>
                                                                    <li className="active"><a href="javascript:void(0)">1</a></li>
                                                                    <li className="next disabled"><a href="javascript:void(0)"> <i className="fa fa-chevron-right"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                        <div className="tab-pane" id="tab_default_2">
                                            <Charts datas={this.state.newfields} chartFields={this.state.searchFields} workflow={this.props.data.workflow}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*assign model */}
                        <button id="assignbutton" className="hidden" data-toggle="modal" data-target="#assign"></button>
                        <div className="modal fade" id="assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 className="modal-title" id="myModalLabel">Assign to User</h4>
                                    </div>
                                    <div className="modal-body">
                                        {this.state.currentData.action?<div>
                                            {this.state.currentData.action.status==="in-progress"?<div><h3>In Progress</h3><span>Started At:&nbsp;&nbsp;{this.state.currentData.action.started}</span></div>:""}
                                            {this.state.currentData.action.status==="assigned"?<h3>Open</h3>:""}
                                            {this.state.currentData.action.status==="completed"?<div><h1>Completed</h1><span>Complete At:&nbsp;&nbsp;{this.state.currentData.action.finished}</span></div>:<div>
                                                <div className="form well">
                                                    <label>Task description</label>
                                                    <input type="text" id="taskDescription" className="form-control"/>
                                                    <label>ETA</label>
                                                    <Datetime  id="ETA"/>
                                                    <label>Choose time interval for notification message</label>
                                                    <select className="form-control" id="msgInterval">
                                                        <option>Choose options</option>
                                                        <option value="3">every 3hrs</option>
                                                        <option value="4">every 4hrs</option>
                                                        <option value="4">every 4hrs</option>
                                                        <option value="5">every 5hrs</option>
                                                    </select>
                                                </div>
                                                {this.props.data.workflow.roles.map((single)=>{
                                                    return( <label><input type="radio" name="role" onClick={(e)=>{
                                                        let clients=_.filter(this.props.data.clientUsers, single => {
                                                            let common=_.intersection([e.target.value],single.roles);
                                                            return common.length>0;
                                                        });
                                                        this.setState({assignClients:clients})
                                                    }} value={single}/>{single}</label>)

                                                })}
                                                {this.state.assignClients.map((single)=>{
                                                return(<div><input type="radio" name="assign" id={single.email} value={single.user}/>{single.name}
                                                </div>)
                                            })}<button className="btn btn-primary" onClick={()=>{
                                                let formdata=this.state.currentData.formdata;
                                                formdata=_.omit(formdata,'incidentId','createdAt');
                                                let assignedElement = $('input[name="assign"]:checked');
                                                let user=assignedElement.val();
                                                let role=$('input[name="role"]:checked').val();
                                                let description=$('#taskDescription').val();
                                                let email=assignedElement.attr('id');
                                                let ETA=$('.rdt').children()[0];
                                                let msgInterval=$('#msgInterval').val();
                                                let assgnedAt=new moment().format('YYYY-MM-DD HH-MM');
                                                let data={action:{user:user,role:role,name:'assign',status:'assigned',description:description,ETA:ETA.value,msgInterval:msgInterval,lastmsg:new Date(),assgnedAt:assgnedAt},id:this.state.currentData._id};
                                                let mail={to:email,cc:[],bcc:[],subject:"Finish task at the given time",body:ETA };
                                                Meteor.call('updateWorkflowAction',data, err => {
                                                    if(!err){
                                                        Meteor.call('sendEmail',mail, err => {
                                                            if(!err){
                                                                console.log("message send successful");
                                                            }
                                                        })
                                                    }
                                                });
                                            }}>Assign</button></div>}
                                        </div>:<div>
                                            <div className="form well">
                                                <label>Task description</label>
                                                <input type="text" id="taskDescription" className="form-control"/>
                                                <label>ETA</label>
                                                <Datetime id="ETA"/>
                                                <label>Choose time interval for notification message</label>
                                                <select className="form-control" id="msgInterval">
                                                    <option value="">Choose options</option>
                                                    <option value="3">every 3hrs</option>
                                                    <option value="4">every 4hrs</option>
                                                    <option value="4">every 4hrs</option>
                                                    <option value="5">every 5hrs</option>
                                                </select>
                                            </div>{this.props.data.workflow.roles.map((single)=>{
                                            return( <label><input type="radio" name="role" onClick={(e)=>{
                                                let clients=_.filter(this.props.data.clientUsers, single => {

                                                    let common=_.intersection([e.target.value],single.roles);
                                                    return common.length>0;
                                                });
                                                this.setState({assignClients:clients})
                                            }} value={single}/>{single}</label>)

                                        })}
                                        
                                        {this.state.assignClients.map((single)=>{
                                            return(<div><input type="radio" name="assign" id={single.email} value={single.user}/>{single.name}
                                            </div>)
                                        })}
                                        <div>
                                        <button className="btn btn-primary" onClick={()=>{
                                            let formdata=this.state.currentData.formdata;
                                            formdata=_.omit(formdata,'incidentId','createdAt');
                                            let user=$('input[name="assign"]:checked').val();
                                            let role=$('input[name="role"]:checked').val();
                                            let description=$('#taskDescription').val();
                                            let ETA=$('.rdt').children()[0];
                                            let msgInterval=$("#msgInterval").val();
                                            let email=$('input[name="assign"]:checked').attr('id');
                                            let assgnedAt=new moment().format('YYYY-MM-DD HH-MM');
                                            let data={action:{user:user,name:'assign',role:role,status:'assigned',description:description,ETA:ETA.value,msgInterval:ETA.value,lastmsg:new Date(),assgnedAt:assgnedAt},id:this.state.currentData._id};
                                            let mail={to:email,cc:[],bcc:[],subject:"Finish task at the given time",body:ETA.value };
                                            Meteor.call('updateWorkflowAction',data, err => {
                                                if(!err){
                                                    Meteor.call('sendEmail',mail, err => {
                                                        if(!err){
                                                            console.log("message send successful");
                                                        }
                                                    });
                                                    $('input[name="assign"]:checked').val("");
                                                    $('input[name="role"]:checked').val("");
                                                    $('#taskDescription').val("");
                                                    $('.rdt').children()[0].val("");
                                                    $("#msgInterval").val("");
                                                }
                                            });

                                        }}>Assign</button></div></div>}
                                    </div>
                                    <div className="modal-footer">
                                    </div>
                                </div>
                            </div></div>
                        {/*FormData*/}
                        <button id="updateButton" className="" data-toggle="modal" data-target="#update"></button>
                        <div className="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 className="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <div className="modal-body">
                                        <div className="col-md-12">
                                            <form id="showform" className="hidden">
                                            </form>
                                            <div>
                                                {this.state.currentData.formdata?_.map(this.state.currentData.formdata, (value,key) => {
                                                    if(key!=="images"){
                                                        return(<div><label>{key}&nbsp;&nbsp;:</label>&nbsp;&nbsp;&nbsp;{value}</div>)
                                                    }
                                                }):""}
                                            </div>
                                            <div className="images">
                                                <h2>Uploaded Images</h2>
                                                {this.state.currentData.formdata?this.state.currentData.formdata.images.map((image)=>{
                                                    return(<div className="col-xs-6 col-md-3">
                                                        <a href="#" className="thumbnail">
                                                            <img src={image} alt="..."/>
                                                        </a>
                                                    </div>)  }):""}
                                            </div>
                                        </div>
                                        <hr/>
                                        <div>
                                            <br/>
                                            <label>Remark</label>
                                            <textarea type="textarea" className="form-control" value={this.state.currentData.remark}  id="remark"></textarea>
                                            <a className="btn btn-primary" id="addRemark" onClick={(e)=>{
                                                let remark=$('#remark').val();
                                                Meteor.call('addRemark',{id:this.state.currentData._id,remark:remark}, err => {
                                                    if(!err){
                                                        $('#remark').val("")
                                                    }
                                                })
                                            }}>Submit</a>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                    </div>
                                </div>
                            </div></div>
                        {/*approve Modal*/}
                        <button id="apprButton" className="" data-toggle="modal" data-target="#approve"></button>
                        <div className="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 className="modal-title" id="myModalLabel">Approve Data</h4>
                                    </div>
                                    <div className="modal-body">
                                        {this.state.approve.length>0?<div><button id="as" onClick={()=>{
                                            this.state.currentData.approveRoles[this.state.approveLevel]['status']='close';
                                            console.log(this.state.currentData.approveRoles);
                                            Meteor.call('updateWorkflowDataApprove',{id:this.state.currentData._id,approveRoles:this.state.currentData.approveRoles}, err => {
                                                if(!err)
                                                {
                                                    $('#as').text('Approved');
                                                    $('#as').attr('disabled',true);
                                                }
                                            })
                                        }} className="btn btn-success btn-large">Approve</button>
                                        <button id="reject" style={{"margin-left":20}} className = "btn btn-danger btn-large" onClick={()=>{
                                           $('#reject_description').show();
                                           $('#as').hide();
                                        }}>Reject</button></div>:<h4>Do not have approve authorization</h4>}

                                        <div style={{display:'none'}} id= 'reject_description'>
                                            <label>Reject description</label>
                                            <textarea id = 'rj_description'></textarea>
                                            <br/>
                                            <button className = "btn btn-primary" onClick={()=>{
                                                this.state.currentData.approveRoles[this.state.approveLevel]['status']='reject';
                                                this.state.currentData.approveRoles[this.state.approveLevel]['description']= $("#rj_description").val();
                                                Meteor.call('updateWorkflowDataApprove',{id:this.state.currentData._id,approveRoles:this.state.currentData.approveRoles}, err => {
                                                    if(!err)
                                                    {
                                                        $('#reject').text('Rejected');
                                                        $('#reject').attr('disabled',true);
                                                        $('#reject_description').hide();
                                                        $('#as').show();
                                                        $('.close').trigger('click');

                                                    }
                                                })
                                            }}>Done</button>
                                        </div>
                                        <div>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*detail view*/}
                            <button id="modalclick" data-toggle="modal" data-target="#mymodals" className="hidden"></button>
                            <div className="modal fade" id="mymodals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div className="modal-dialog" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 className="modal-title" id="myModalLabel">Choose other fields</h4>
                                            </div>
                                            <div className="modal-body">
                                                <CheckboxGroup name="fruits" value='' onChange={(fields)=>{
                                                    let d=this.state.defield;
                                                    let newf=d.concat(fields);
                                                    this.addFields(newf);
                                                }}>
                                                    <ul style={{listStyleType:"none"}}>
                                                        {this.state.otherfields.map((single)=>{
                                                            return(<li><label><Checkbox value={single} Checked/>{single}</label></li>)
                                                        })}
                                                    </ul>
                                                </CheckboxGroup>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                       
                        </div>
                    </div>)
    }
}