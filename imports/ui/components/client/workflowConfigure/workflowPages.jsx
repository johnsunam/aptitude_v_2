import React,{ Component } from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

export default class WorkflowPages extends Component{
    constructor(props) {
        super(props);
        this.state = {
        choosedField:null,
        values:[],
        choosedRoles:[],
        users:[],
        chooseUser:[]

        }
    }

    handlingState(){
            console.log('handlingState');
    }
    componentWillReceiveProps(nextProps) {
        this.setState
    }
    render(){
        let self = this;
        return(
            <div className="row col-md-9 col-lg-9">
                <div className="panel">
                    <div className="panel-body">
                        <h3 className="title-hero">Workflow Pages</h3>
                        <div className="example-box-wrapper">
                            <div className="panel-group" id="accordion">
                            {this.props.pages.map((obj,key)=>{
                                console.log(obj);
                                let page = obj.detail.page;
                                let name = page.name;
                                let heading = page.name+"  #"+key;
                                let configurableField = obj.detail.page.form.configurableField;
                                let roles = obj.detail.roles;
                                name = name.indexOf(' ') != -1 ? name.split(' ').join("_"):name;
                                
                                let href = '#'+name;

                                return (<div class="panel">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href={href} className="collapsed" aria-expanded="false">
                                        {heading}
                                    </a>
                                </h4>
                            </div>
                            <div id={name} className="panel-collapse  collapse" aria-expanded="false">
                                <div className="panel-body">
                                <div className="well">
                                <h3>Field List</h3>
                                <ul style={{listStyleType:"none"}}>
                                    {configurableField.map((obj)=>{
                                        console.log(obj);
                                            return(<li><input type="radio" value = {obj.name} id="field" name="field" onClick={e=>this.setState({choosedField:e.target.value, choosedRoles:[]})} /><label htmlFor="">{obj.value}</label></li>)
                                    })}
                                </ul>
                                </div>
                                   <div>
                                    <h3>Add values to Field</h3>
                                    <input type="text" className="form-control"  id="field_value" />
                                    <ul>
                                        {self.state.values.map((obj)=>{
                                            return (<li>{obj.value}</li>)
                                    })}
                                    </ul>
                                    
                                    <button className="btn btn-primary" onClick={(e)=>{
                                        let val = $("#field_value").val();
                                        data = {value:val,label:val};
                                        console.log(data);
                                        let tem = self.state.values;
                                        tem.push(data);
                                        console.log(tem);
                                        self.setState({values:tem});
                                        
                                    }}>add values</button>&nbsp;&nbsp;&nbsp;

                                     <button className="btn btn-primary" onClick={(e)=>{
                                         let name = $("input[name='field']:checked").val();
                                        
                                         let fields = {name:name};
                                         let setting = [];
                                       Meteor.call("addConfigurableValue",{worflow:this.props.workflow._id, page:obj.detail.page._id, key:key, field_name:self.state.choosedField,values:self.state.values});
                                       self.setState({values:[]});
                                       
                                     }}>Done</button>
                                   </div> 
                                </div>
                            </div>
                        </div>)
                            })}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}       