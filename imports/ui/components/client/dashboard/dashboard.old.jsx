//pages listed according to the client
import React,{Component} from 'react';
import AddPage from '../../../container/addPage.js'
import crudClass from '../../common/crudClass.js'

export default class ClientAdminPages extends Component {
  constructor(props) {
    super(props)

  }

  render(){
      console.log(this.props.workflows)
    var count=0;
    return (
    <div className="row">
    <div className="col-md-12">
      <h1>
          Dashboard
          <small>Control panel</small>
        </h1>
        <div className="box">
         <div className="box-header with-border">
            <h3 className="box-title">Workflows</h3>
            <div className="box-body">
            <div className="row mt25">
             {this.props.workflows.map((workflow)=>{

               count=count+1;
          return(<div className="col-lg-3">
                  <div className="small-box bg-primary">
                    <div className="inner">
                      <h3>{count}</h3>

                      <p><b>{workflow.name}</b></p>
                      <p>{workflow.description}</p>
                    </div>
                    <div className="icon">
                      <i className="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" id={workflow._id} onClick={(e)=>{
            FlowRouter.go(`/reports/${e.target.id}`)
          }} className="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>

              </div>

            )
        })}
            </div>
            </div>
          </div>
        </div>
     </div>
     </div>
)

  }
}