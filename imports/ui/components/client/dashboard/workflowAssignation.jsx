                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              import React,{Component} from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

export default class WorkflowAssignation extends Component{
    constructor(props){
        super(props);
        this.state = {
          choosedRoles : [],
          otherWorflowRoles : []
        }

    }
  
    
    render(){
      
        let pages = this.props.workflow.flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
        let self = this;
        return(<div className="modal fade" id="assignWorkflow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Assign Roles to Workflow</h5>
                <button type="button" className="close" onClick = {()=>this.setState({choosedRoles:[]})} data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
              <h4>Worflow pages</h4>
              <hr/>
              <ul style={{listStyleType: "none"}}>
                {pages.map((obj,key)=>{
                  return (<li><input type="radio" name="page"  value={key} onClick={e=>{
                    let allRoles = _.difference(pages[e.target.value].detail.roles,self.props.workflow.roles); 
                    self.setState({
                      choosedRoles : pages[e.target.value].detail.roles,
                      otherWorflowRoles : allRoles
                      })
                  }}/>&nbsp;&nbsp;&nbsp;
                  <label>{obj.detail.page.name}</label></li>)
                })}
                </ul>
                <div>
                <h4>Choose Roles for Worflow pages</h4>
                <hr/>
                <ul style={{listStyleType: "none"}}>
                <CheckboxGroup 
                value = {self.state.choosedRoles}  
                onChange={(newRoles)=>{
                  self.setState({choosedRoles:newRoles});
                  let page=$("input[name='page']:checked").val();
                  pages[page].detail.roles = pages[page].detail.roles ? _.union( self.state.otherWorflowRoles,newRoles) : newRoles;
                  Meteor.call('updateWorkflowPageRoles',{id:this.props.workflow._id, workflow:this.props.workflow});
                }}>
                {self.props.workflow.roles.map((single)=>{
                    return (<li><Checkbox id="checkbox"  
                    value={single}
                    
                    />&nbsp;&nbsp;&nbsp;<label>{single}</label></li>)
                })}
                </CheckboxGroup>
                  
                </ul>
                </div>

              </div>
              <div className="modal-footer">
                <button type="button" onClick = {()=>this.setState({choosedRoles:[]})} className="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>)
    }
}