//pages listed according to the client
import React,{Component} from 'react';
import AddPage from '../../../container/addPage.js'
import crudClass from '../../common/crudClass.js'
import '../client.css';

export default class ClientAdminPages extends Component {
  constructor(props) {
    super(props)
    this.state={
        workflow_id:null,
        workflows:[]
    }
  }

  componentDidMount() {
      this.setState({workflows:this.props.datas});
  }

  render(){
      let count = 0;
      let self =  this;
      return (
          <div className="col-md-12 col-sm-12 col-lg-12">
                        <div className="block full">
                            <div className="block-title">
                                <h2>List of all Workflows</h2>
                            </div>
                            <div className="table-responsive">
                                <div id="example-datatable_wrapper" className="dataTables_wrapper form-inline no-footer">
                                    <div className="row">
                                        <div className="col-sm-6 col-xs-7">
                                                <div id="example-datatable_filter" className="dataTables_filter">
                                                        <div className="input-group">
                                                            <input type="search" 
                                                                className="form-control" 
                                                                placeholder="Search" 
                                                                aria-controls="example-datatable"
                                                                 onKeyUp={ e=>{
                                                                        if(e.target.value == ""){
                                                                        this.setState({workflows: this.props.datas});
                                                                        } else {
                                                                            let searchResult = [];
                                                                            _.each(this.props.datas,(obj)=>{
                                                                                console.log(obj.name.includes(e.target.value));
                                                                                obj.name.includes(e.target.value) ? searchResult.push(obj) :"";
                                                                            });
                                                                            console.log(searchResult);
                                                                            this.setState({workflows:searchResult});
                                                                        }
                                                                    }} 
                                                            />
                                                                <span className="input-group-addon"><i className="fa fa-search"></i></span>
                                                        </div>
                                                    
                                                </div>
                                        </div>
                                    </div>
                                    <table id="example-datatable" className="table table-striped table-bordered table-vcenter dataTable no-footer" role="grid" aria-describedby="example-datatable_info">
                                        <thead>
                                            <tr role="row">
                                            <th className="text-center sorting_asc" style={{width: 49}} tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="S. No.: activate to sort column descending">S. No.</th><th className="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style={{width: 172}}>Name</th><th className="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style={{width: 293}}>Description</th><th style={{width: 119}} className="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1" aria-label="# Incidents: activate to sort column ascending"># Incidents</th><th className="text-center sorting_disabled" style={{width: 74}} rowspan="1" colspan="1" aria-label=""><i className="fa fa-flash"></i></th></tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.workflows.map((workflow) => {
                                            count = count + 1;
                                            let url = '/reports/'+workflow._id;
                                            let configurable = '/client/configurable/'+workflow._id;
                                            return ( <tr role="row" className="odd">
                                                    <td className="text-center sorting_1">{count}</td>
                                                    <td><strong>{workflow.name}</strong></td>
                                                    <td>{workflow.description}</td>
                                                    <td><span className="label label-info">{workflow.count}</span></td>
                                                    <td className="text-center">
                                                    
                                                        <a 
                                                        href={url} id={workflow._id} onClick={(e)=>FlowRouter.go(`/reports/${e.target.id}`)} 
                                                        data-toggle="tooltip" 
                                                        title="" 
                                                        className="btn btn-effect-ripple btn-xs btn-success" 
                                                        style={{overflow: "hidden", position: "relative"}} data-original-title="Report">
                                                            <i className="fa fa-file-excel-o"></i>
                                                        </a>

                                                        <a href={configurable} data-toggle="tooltip" 
                                                            title="" className="btn btn-effect-ripple btn-xs btn-danger" 
                                                            style={{overflow: "hidden", position: "relative"}} 
                                                            data-original-title="Configurable Fields">

                                                                <i className="fa fa-pencil-square-o"></i>
                                                        </a>
                                                    </td>
                                                </tr>)
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <div className="row">
                                        <div className="col-sm-5 hidden-xs">
                                            <div className="dataTables_info" id="example-datatable_info" role="status" aria-live="polite"><strong>1</strong>-<strong>1</strong> of <strong>1</strong></div>
                                        </div>
                                        <div className="col-sm-7 col-xs-12 clearfix">
                                            <div className="dataTables_paginate paging_bootstrap" id="example-datatable_paginate">
                                                <ul className="pagination pagination-sm remove-margin">
                                                    <li className="prev disabled">
                                                        <a href="javascript:void(0)"><i className="fa fa-chevron-left"></i></a>
                                                    </li>
                                                    <li className="active"><a href="javascript:void(0)">1</a></li>
                                                    <li className="next disabled"><a href="javascript:void(0)"> <i className="fa fa-chevron-right"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>)
  }
}