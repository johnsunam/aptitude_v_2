import React ,{Component} from 'react'
import Paginate from '../../common/paginator.jsx'
import crudclass from '../../common/crudClass.js'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ManageClient extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas:{},
            rows:props.clients
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({rows:nextProps.clients})
    }


    render(){
        return(
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <div>
                            <h4 className="title">Manage Clients</h4>
                            <p className="category">Here is list of clients you can manage.</p>
                            <a  className="btn btn-primary" href="/aptitude/add-client" style={{position: 'absolute', top: 7, right: 11}} >Add Client</a>
                        </div>
                        <hr/>
                        <div className="card-content table-responsive">
                            <Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true}>
                                {this.state.rows.map(function(row) {
                                    let url = "/aptitude/edit-client/"+row._id;
                                    return (
                                        <Tr key={row.key} className="myTableRow">
                                            <Td column="Company Name">{row.companyName}</Td>
                                            <Td column="Email">{row.email}</Td>
                                            <Td column="Phone">{row.phone}</Td>
                                            <Td column="Website">{row.website}</Td>
                                            <Td column="Code">{row.code}</Td>
                                            <Td column="Action">
                                                <div>
                                                    <ReactTooltip id='edit' data-tip  type='info'>
                                                        <span>Edit</span>
                                                    </ReactTooltip>
                                                    <a href={url} className="btn btn-round btn-success" data-tip data-for='edit'><i className="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                                    <ReactTooltip id='delete' type='info'>
                                                        <span>Delete</span>
                                                    </ReactTooltip>
                                                    <a href="#"  className="btn btn-round btn-danger" data-tip data-for='delete'  onClick={()=>{
                                                        let obj=new crudclass();
                                                        obj.delete('deleteClient',row._id)
                                                    }} data-toggle="confirmation"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </div>
                                            </Td>
                                        </Tr>
                                    )
                                })}
                            </Table>
                        </div>
                    </div>
                </div>
            </div>)
    }
}