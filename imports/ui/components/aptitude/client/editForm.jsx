import React,{Component} from 'react'

export default class EditForm extends Component{
    constructor(props) {
        super(props);
        this.state={
            form:JSON.parse(this.props.form.form)
        }
    }
    componentDidMount(){
        let buildWrap = $(document.getElementById('form-editors'));
        let options={
            formData:this.state.form,
            dataType:'json'
        };
        $(buildWrap).formBuilder(options).data('formBuilder')
    }
    render(){
        return(
            <div>
                <div id="form-editors"></div>
            </div>
        )
    }
}
