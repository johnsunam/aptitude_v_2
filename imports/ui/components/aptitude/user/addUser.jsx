import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
var Confirm = require('react-confirm-bootstrap');
import {FlowRouter} from 'meteor/kadira:flow-router';

export default class addUser extends Component {
    constructor(props) {
        super(props)
        this.state={
            saveResult:false,
            edit:this.props.edit,
            user:this.props.user,
            canSubmit: false,
            res: "",
            name:'',
            dob:'',
            address:'',
            mobile:'',
            email:'',
            secQuestion:'',
            secAnswer:'',
            roleName:'',
            showMessage:'',
            message:''
        }
    }

    componentDidMount(){
        console.log(this.props.user)
        this.props.edit?this.setState({name:this.props.user.name,
            dob:this.props.user.dob,
            address:this.props.user.address,
            mobile:this.props.user.mobile,
            email:this.props.user.email,
            secQuestion:this.props.user.secQuestion,
            secAnswer:this.props.user.secAnswer,
            roleName:this.props.user.roleName}):this.setState({name:'',
            dob:'',
            address:'',
            mobile:'',
            email:'',
            secQuestion:'',
            secAnswer:'',
            roleName:''})
    }
    shouldComponentUpdate(nextProps, nextState){
        let self=this;
        Tracker.autorun(function(){
            if(Session.equals('confirm',true)){
                if(Session.get('res')==true){
                    console.log('helo')
                    self.setState({showMessage:true,message:"User Saved Sucessfully"})
                    $('.message').addClass('su')
                }else{
                    self.setState({showMessage:true,message:"User Already exits"})
                    $('.message').addClass('er')
                }
                Session.set('confirm',false)
            }
        })

        return true;
    }

    enableButton() {
        this.setState({ canSubmit: true });
    }
    disableButton() {
        this.setState({ canSubmit: false });
    }

    // saving user to userDb
    submit(e){
        self=this;
        let obj= new crudClass();
        let name=$("#name").val(),
            dob=$("#dob").val(),
            address=this.refs.address.value,
            mobile=$("#mobile").val(),
            email=$("#email").val(),
            secQuestion=$("#secQuestion").val(),
            secAnswer=$("#secAnswer").val(),
            roleName=$("#roleName").val();
        let user=window.localStorage.getItem('user')

        let record=this.props.edit?{id:this.props.user._id,data:{name:name,dob:dob,address:address,mobile:mobile,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roleName:roleName}}:
            {user:user,data:{name:name,dob:dob,address:address,mobile:mobile,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roleName:roleName}}
        let res=self.state.edit ? obj.create('editUser',record) : obj.create('createAptitudeAdmin',record);
        self.setState({saveResult:res})
        self.refs.form.reset()
        $('select').prop('selectedIndex',0);
        FlowRouter.go('/aptitude/manage-user');
    }

    triggerConfirm()
    {
        $("#confirm").trigger('click');
    }
    render(){
        let user=this.props.user;
        let submitButton=<button className="btn  btn-primary" type="submit" disabled={!this.state.canSubmit} ><span>Save</span></button>
        return(
            <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div >
                            <h4 className="title">{this.state.edit?"Edit User":"Create User"}</h4>
                        </div>
                        <hr/>
                        <div className="card-content">
                            <Formsy.Form ref="form" onValidSubmit={this.triggerConfirm.bind(this)} id="addUser" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>

                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="input-container">
                                            <MyInput type="text" className="form-control" help="Pick your username" id="name" name="name" value={this.props.edit?this.props.user.name:""} title="User Name" ref="name" required/>
                                            <div className="bar"></div>
                                        </div>

                                        <div className="input-container">
                                            <MyInput title="Birth Date" type="date" help="Enter your birthdate" className="form-control" id="dob" name="dob" value={this.props.edit?this.props.user.dob:""} ref="dob" required/>
                                            <div className="bar"></div>
                                        </div>

                                        <div className="input-container">
                                            <MyInput help="Enter your address" className="form-control" placeholder="Address" id="address" name="address" title="Address" value={this.props.edit?this.props.user.dob:""} required ref="address" />
                                            <div className="bar"></div>
                                        </div>
                                        <div className="input-container">
                                            <MyInput type="number" help="Enter your valid mobile number" className="form-control" id="mobile" name="mobile" title="Mobile Number" ref="mobile" required value={this.props.edit?this.props.user.mobile:""}/>
                                            <div className="bar"></div>
                                        </div>

                                    </div>

                                    <div className="col-md-6">
                                        <div className="input-container">
                                            <MyInput type="email" help="Enter your valid email address" name="email" id="email" className="form-control" title="Email ID" required value={this.props.edit?this.props.user.email:""} ref="email" />
                                            <div className="bar"></div>
                                        </div>
                                        <div className="input-container">
                                            <MyInput type="text" help="Enter your question" className="form-control" name="secQuestion" id="secQuestion" title="Security Question" required value={this.props.edit?this.props.user.secQuestion:""} ref="secQuestion" />
                                            <div className="bar"></div>
                                        </div>
                                        <div className="input-container">
                                            <MyInput type="text" help="Enter the answer of the security question" className="form-control" id="secAnswer" name="secAnswer" required title="Security Answer" value={this.props.edit?this.props.user.secAnswer:""} ref="secAnswer"/>
                                            <div className="bar"></div>
                                        </div>
                                        <div className="input-container">
                                            <MyInput type="text" help="Enter your role name" name="roleName" className="form-control" id="roleName" required title="Role Name" ref="roleName" value={this.props.edit?this.props.user.roleName:""}/>
                                            <div className="bar"></div>
                                        </div>
                                        <div style={{float: 'right'}}>
                                            {submitButton}
                                            &nbsp;
                                            <a className="btn btn-warning "
                                               onClick={()=>{
                                                   this.props.edit?this.refs.form.reset(this.props.user):this.refs.form.reset();
                                                   $('select').prop('selectedIndex',0);
                                               }}>Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </Formsy.Form>
                            <Confirm onConfirm={this.submit.bind(this)}
                                     body="Are you sure you want to submit?"
                                     confirmText="Confirm"
                                     title="Submit Form">
                                <button id="confirm" className="hidden"></button>
                            </Confirm>
                        </div>
                    </div>
                </div>
            </div>)
    }
}
