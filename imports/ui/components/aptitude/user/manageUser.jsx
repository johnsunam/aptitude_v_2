//edit,delete and lists client
import React ,{Component} from 'react'
import Paginate from '../../common/paginator.jsx'
import AddUser from './addUser.jsx'
import crudClass from '../../common/crudClass.js'
import orderBy from 'lodash/orderBy';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ManageUser extends Component {
  constructor(props) {
   super(props)
   this.state = {datas:''
   }
  }

  render(){
    let self=this;
      return(
        <div className="row">
          <div className="col-md-12">
            <div className="card">
                <div>
                    <h4 className="title">Manage Users</h4>
                    <p className="category">Here is list of users you can manage.</p>
                    <a  className="btn btn-primary" href="/aptitude/add-user" style={{position: 'absolute', top: 7, right: 11}} >Add User</a>
                </div>
                <hr/>
              <div className="card-content table-responsive">
                <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No User found." itemsPerPage={30} sortable={true}>
                {this.props.users.map(function(row) {
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Name">{row.name}</Td>
                            <Td column="Phone">{row.mobile}</Td>
                            <Td column="Email">{row.email}</Td>
                            <Td column="Action" className="actionTd">
                              <div>
                               <ReactTooltip id='edit'  type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                                <a href="#" data-tip data-for="edit" className="btn btn-round btn-success edit" onClick={()=>{
                                  self.setState({datas:row})
                                }}  data-toggle="modal" data-target="#myModal" ><i 
                                className="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
                              <ReactTooltip id='delete'  type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                              <a href="#" className="btn btn-round btn-danger" data-tip data-for="delete" onClick={()=>{
                                  let obj=new crudClass()
                                  obj.delete('deleteUser',row.id)
                                }} disabled={row.status?'disabled':''} ><i className="fa fa-trash-o"></i></a>
                            </div>
                            </Td>
                        </Tr>)
                      })}
            </Table>
            {this.state.datas._id?<div className="modal fade" id='myModal' tabindex="-1" user="dialog" aria-labelledby="myModalLabel">
            <AddUser edit="true" user={this.state.datas}/>
            </div>:<span></span>}
              </div>
            </div>
          </div>
        </div>);
  }
}