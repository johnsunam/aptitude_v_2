import React,{Component} from 'react'
import {FlowRouter} from "meteor/kadira:flow-router";
export default class Save extends Component{
    constructor(props){
        super(props)
        this.state={message:false,formCredentials:props.stateValues.formCredentials};
    }
    componentDidMount(){
         $(".mesg").hide();
      this.props.stateValues.from=='main'?'':this.props.myForm();
   
   }
    render(){
        return(<div className="">
        <div className="content-sub">
        <div className="input-group row col-md-12">
        <div className="col-md-3">
         <label>Form Name</label>
        </div>
       <div className="col-md-6">
       {this.props.stateValues.edit?<input type="text" id="name" 
       value={this.state.formCredentials.name} className="form-control" disabled/>:
        <input type="text" placeholder="Form name" disabled={this.props.stateValues.disable?this.props.stateValues.disable:false} id="name" className="form-control"/>}
       
           <p className="mesg" style={{color:"red"}}>Enter form name first</p>
        </div>
           
        </div><br/>
        <div className="input-group  row col-md-12">
        <div className="col-md-3">
          <label>Form Description</label>
        </div>
       <div className="col-md-6">
       <textarea disabled={this.props.stateValues.disable?this.props.stateValues.disable:false} type="text" id="description" className="form-control" placeholder="Describe form" row="8" col="5"></textarea>
        </div><br/><br/>
           <a href="#" className="btn btn-primary formsubmit col-md-offset-8 pull-left" style={{marginTop:20}} onClick={()=>
                     {
                        let name=$("#name").val();
                        let description=$("#description").val()
                        if(name!=""){
                     this.props.saveForm(name,description);
                     this.setState({message:true})
                     $('.formsubmit').hide()
                     $('.back-btn').attr('disabled',true);
                      FlowRouter.go('/aptitude/manage-form');
                        }
                        else{
                                $(".mesg").show()
                               

                        }
                    
                    }}
                   >{this.props.edit?"Save Changes":"Save Form"}</a>
        </div>

        </div>
                   
                   <div className="" id="save-alert">
                <span style={{"fontSize":20,"color":"green"}}>{this.state.message?"form sucessfully saved":""}</span>
                </div>
        <button className="btn btn-sm btn-primary back-btn <pull-right></pull-right> navigate-btn" onClick={()=>{
                       this.props.changeCompnent('search');
                   }}>Previous</button>
               </div>)
    }
}