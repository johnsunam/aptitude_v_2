import React,{Component} from 'react'
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
export default class SearchKey extends Component{
    constructor(props){
        super(props);
        this.state={
            searchFieldData:[]
        }
    }
    render(){
        return(<div className="text-center">
        <div className="content-sub">
                    <h2>Define Search Keys</h2>
                    {/*Logic for checkbox      */}
                    
                    <CheckboxGroup name="searchKeys"  onChange={(newroles)=>{
                    let fields=[]; 
                    let self=this;
                    _.map(newroles,function(single){
                      let field=_.findWhere(self.props.stateValues.labelName,{label:single});
                      fields.push(field);
                    })
                    
                    this.setState({keyfields:fields});
                  this.setState({searchFieldData:fields})
                  this.props.searchKey(fields);
                }}>
                <table className="col-md-offset-4" style={{width:302,height: 272}}>
                  {this.props.stateValues.labelName.map((labelValue)=>{
                return(<tr><td><Checkbox id="checkbox"  disabled={this.props.stateValues.disable?this.props.stateValues.disable:false} value={labelValue.label}/></td>
                        <td><span>{labelValue.label}</span></td>
                      </tr>)
                })}
                </table>
                </CheckboxGroup>
                </div>
                   <button className="btn btn-sm btn-primary back-btn pull-left navigate-btn" onClick={()=>{
                       this.props.searchKey(this.state.searchFieldData);
                       this.props.changeCompnent('default');
                   }}>Previous 
                  </button>
                   <button className="btn btn-sm btn-primary next-btn pull-right navigate-btn" onClick={()=>{
                       
                       this.props.changeCompnent('save');
                   }} >Next </button>
               </div>)
    }
}