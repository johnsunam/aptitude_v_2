//edits,deletes and lists the forms

import React,{Component} from 'react'
import crudClass from '../../common/crudClass.js'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from "react-tooltip"
export default class ManageForm extends Component {
    constructor(props) {
        super(props)
        this.state={
            forms:props.forms?props.forms:[]
        }
    }
    componentDidMount(){
        let renderWrap = $(document.getElementById('fb-rendered-form'))

    }
    enableButton() {
        this.setState({ canSubmit: true });
    }
    disableButton() {
        this.setState({ canSubmit: false });
    }
    render(){
        let forms=this.props.forms?this.props.forms:[];
        return(
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div>
                    <h4 className="title">Manage Form</h4>
                    <p className="category">Here is list of forms you can manage.</p>
                    <a  className="btn btn-primary" href="/aptitude/add-form" style={{position: 'absolute', top: 7, right: 11}} >Add Form</a>
                  </div>
                  <hr/>
                  <div className="card-content table-responsive">
                    <Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true}>
                        {forms.map((form)=> {
                            let path='/aptitude/edit-form/'+form._id;
                            let modal='#'+form._id;
                            let showform="#"+form._id+"form";
                            return (
                                <Tr key={form.key} className="myTableRow">
                                  <Td column="Form Name">{form.name}</Td>
                                  <Td column="Description">{form.description}</Td>
                                  <Td column="Action">
                                    <div>
                                      <ReactTooltip id='view'  type='info'>
                                        <span>Preview</span>
                                      </ReactTooltip>
                                      <a href="#" className="btn btn-round btn-azure" data-tip data-for='view'  onClick={()=>{
                                          $('.modal-backdrop').css('z-index','-1');
                                          let data=JSON.parse(form.form);
                                          $(showform).formRender({
                                              dataType: 'json',
                                              formData:data
                                          })
                                      }}
                                         data-toggle="modal" data-target={modal}><i className="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                      <div className="modal fade" id={form._id} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div className="modal-dialog" role="document">
                                          <div className="modal-content">
                                            <div className="modal-header">
                                              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 className="modal-title" id="myModalLabel">Form View</h4>
                                            </div>
                                            <div className="modal-body">
                                              <div id={`${form._id}form`}></div>
                                            </div>
                                            <div className="modal-footer">
                                              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <ReactTooltip id='clone'  type='info'>
                                        <span>Clone</span>
                                      </ReactTooltip>
                                      <a href="#"  className="btn btn-round btn-yellow" data-tip data-for="clone" data-toggle="modal" data-target={`#clone${form._id}`} onClick={()=>{
                                          $('.modal-backdrop').css('z-index','-1');
                                      }}><i className="fa fa-clone" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                      <div className="modal fade" id={`clone${form._id}`} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div className="modal-dialog" role="document">
                                              <div className="modal-content">
                                                  <div className="modal-header">
                                                      <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                      <h4 className="modal-title" id="myModalLabel">Form Title</h4>
                                                  </div>
                                                  <div className="modal-body">
                                                      <Formsy.Form ref="form" onValidSubmit={(e)=>{
                                                          var obj=new crudClass();
                                                          obj.create('addForm',{name:e.formtitle,description:form.description,form:form.form,user:form.user})

                                                      }} id="addPage" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                                                          <MyInput className="form-control" type="text" name="formtitle" ref="formtitle"/>
                                                          <button className="btn btn-success" type="submit">save</button>
                                                      </Formsy.Form>
                                                  </div>
                                                  <div className="modal-footer">
                                                      <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <ReactTooltip id='edit'  type='info'>
                                        <span>Edit</span>
                                      </ReactTooltip>
                                      <a href={path} data-tip data-for="edit" className="btn btn-round btn-success"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                      <ReactTooltip id='delete'  type='info'>
                                        <span>Delete</span>
                                      </ReactTooltip>
                                      <a id={form._id} href="#" data-tip data-for="delete" className="btn btn-round btn-danger" onClick={(e)=>{
                                          let forms=this.state.forms;
                                          let newForms=_.reject(forms,function(form){
                                              if(form._id == e.target.id){
                                                  return form;
                                              }

                                          })
                                          Meteor.call('deleteForm',e.target.id)
                                          this.setState({forms:newForms});

                                      }}><i className="fa fa-trash-o" id={form._id} aria-hidden="true"></i></a>
                                    </div>
                                  </Td>
                                </Tr>
                            )
                        })}
                    </Table>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
