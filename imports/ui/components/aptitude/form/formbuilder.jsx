import React,{Component} from "react"

export default class FormBuilder extends Component{
    constructor(props){
        super(props)
    }

    componentDidMount(){
       

        var buildWrap = $(document.getElementById('fb-editors')),

    renderWrap = $(document.getElementById('fb-rendered-form')),
    fbOptions = {
      disableFields:['autocomplete','hidden','paragraph','radio-group'],
     dataType: 'json',
     inputSets: [
                {
         label: 'Rating',
         fields: [
         {
           type: 'number',
           subtype: 'h2',
           label: 'Rating',
           className:'rating'
         }
       ]
     },
     {
       label:'toggle button',
       fields:[
         {
           label:"choose toggle",
           type:"checkbox",
           "data-toggle":"toggle",
           "data-on":"yes",
           "data-off":"no"
         }
       ]
     },
     {
       label:"Hidden Input",
       fields:[
         {
          type:"text",
          className:"hidden-input",

         }
       ]
     },
     {
        label:"Take Picture",
        fields:[
                {
                type:'button',
                label:'Camera',
                className:'take-picture',
                id:'take-picture'
                }
               ]
      },

    ],
    typeUserEvents:{
       text:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       autocomplete:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       button:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       checkbox:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       'checkbox-group':{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       date:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       file:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       header:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       hidden:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       paragraph:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       number:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       'radio-group':{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       select:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       textarea:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

     }
    
   }
     formBuilder = $(buildWrap).formBuilder(fbOptions).data('formBuilder');
    this.props.formData(formBuilder.formData)
    }
    render(){
        return(<div> 
        <div className="row" style={{"marginTop":30,"marginBottom":20,"marginLeft":10}}>

      <div className="col-md-5 input-container">
      
      <h4 className="" style={{color:"red"}}>Please choose atleast one element</h4>
      </div>

      </div>

      <div  id="fb-editors">
      </div></div>)
    }
}