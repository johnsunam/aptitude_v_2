import React,{Component} from 'react'
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

export default class DefaultField extends Component{
    constructor(props){
        super(props)
        this.state={
            fields:[],
            defaultFieldData:[]
        }
    }
    render(){
        return(<div className="text-center">
                <div className="row">
                <div className="">
                  <div className="col-md-11 col-md-offset-1">
                   <table className="table table-striped">
                     <thead>
                       <tr>
                       {this.state.fields.map((labelValue)=>(
                       <th>{labelValue.label}</th> 
                       ))}
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                        {this.state.fields.map((labelValue)=>(
                       <th>data</th> 
                       ))}
                       </tr>
                     </tbody>
                   </table>
                   </div>
               </div>

               <div className="row">
                <div className="content-sub">
                 <h2 className="title-hero">Choose Default fields</h2>
                 <div className="">
                  <CheckboxGroup name="default fields"  onChange={(newroles)=>{
                    let fields=[]; 
                    let self=this;
                    _.map(newroles,function(single){
                      let field=_.findWhere(self.props.stateValues.labelName,{label:single});
                      fields.push(field);
                    })
                    this.setState({fields:fields});
                  this.setState({defaultFieldData:fields})
                  this.props.defaultField(fields);
                }}>
                <table className="col-md-offset-4" style={{width:302,height: 272}}>
                  
                    {this.props.stateValues.labelName.map((labelValue)=>{
                return(<tr><td><Checkbox id="checkbox"  disabled={this.props.stateValues.disable?this.props.stateValues.disable:false} value={labelValue.label}/></td>
                        <td><span>{labelValue.label}</span></td>
                      </tr>)
                })}
                 
                </table>
                
                </CheckboxGroup>
                </div>
            </div>
                 <button className="btn btn-sm btn-primary back-btn pull-left navigate-btn" onClick={()=>{
                     this.props.defaultField(this.state.defaultFieldData);
                     this.props.changeCompnent('logic');
                 }}>Previous</button>
                 <button className="btn btn btn-sm btn-primary next-btn pull-right navigate-btn" onClick={()=>{
                     this.props.changeCompnent('search');
                 }} >Next </button>
                 
                </div>
              </div>

           </div>)
    }
} 