import React,{Component} from 'react';

export default class FormLogic extends Component{
    constructor(props){
        super(props);
        this.state={
            formCredentials:this.props.stateValues.formCredentials
        }
    }

   componentDidMount(){
      this.props.stateValues.from=='main'?'':this.props.myForm();
   
   }

    render() {

        return(<div className="">        
        <div>
        <a href="#" disabled={this.props.stateValues.disable?this.props.stateValues.disable:false}  onClick={()=>{
            this.props.addLogic();
            $('.modal-backdrop').css('z-index',-1);
        }} data-toggle="modal" data-target="#myModal" className="btn btn-primary">
        <i className="fa fa-plus" aria-hidden="true">
        </i> Add Logic</a><br />
        </div>
         <div  id="mainForm">
         </div>
        <button className="btn btn-sm btn-primary next-btn pull-left navigate-btn" 
        onClick={()=>{
            this.props.changeCompnent('create-form')
            this.props.changeFrom('other')
    }}>previous</button>
        <button className="btn btn-sm btn-primary next-btn pull-right navigate-btn" 
        onClick={()=>{
            this.props.changeCompnent('default')
            this.props.changeFrom('other')
    }}>Next</button>
        </div>)
    }
}