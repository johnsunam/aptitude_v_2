import React ,{Component} from 'react'
import crudClass from '../../common/crudClass.js'
import go from 'gojs';
import Alert from 'react-s-alert';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
$$ = go.GraphObject.make;// for conciseness in defining templates

export default class DefineWorkFlow extends Component {
  constructor(props) {
   super(props)
   this.state = {charts:[],myDiagram:'',data:'',pages:[],pageModel:[],fieldlist:[],action:'',emailDetail:'',cc:[],bcc:[],email:props.data.workflow.email!=undefined?props.data.workflow.email:{}};
  }
  showLinkLabel(e) {
        var label = e.subject.findObject("LABEL");
        if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }
    
  nodeStyle() {
    let self=this;
        return [
            // The Node.location comes from the "loc" property of the node data,
            // converted by the Point.parse static method.
            // If the Node.location is changed, it updates the "loc" property of the node data,
            // converting back using the Point.stringify static method.
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify), {
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function(e, obj) {
                    self.showPorts(obj.part, true);
                },
                mouseLeave: function(e, obj) {
                    self.showPorts(obj.part, false);
                }
            }
        ];
    }
    makePort(name, spot, output, input) {
        // the port is basically just a small circle that has a white stroke when it is made visible
    
        return $$(go.Shape, "Circle", {
            fill: "transparent",
            stroke: null, // this is changed to "white" in the showPorts function
            desiredSize: new go.Size(8, 8),
            alignment: spot,
            alignmentFocus: spot, // align the port on the main Shape
            portId: name, // declare this object to be a "port"
            fromSpot: spot,
            toSpot: spot, // declare where links may connect at this port
            fromLinkable: output,
            toLinkable: input, // declare whether the user may draw links to/from here
            cursor: "pointer" // show a different cursor to indicate potential link point
        });
    }
  init(self){
  
    myDiagram = $$(go.Diagram, "myDiagramDiv", // must name or refer to the DIV HTML element
        {
            initialContentAlignment: go.Spot.Center,
            allowDrop: true, // must be true to accept drops from the Palette
            "LinkDrawn": self.showLinkLabel, // this DiagramEvent listener is defined below
            "LinkRelinked": self.showLinkLabel,
            "animationManager.duration": 800, // slightly longer than default (600ms) animation
            "undoManager.isEnabled": true // enable undo & redo
        });
        var myModel=$$(go.GraphLinksModel);
       console.log(this.props.data.workflow.flowchart)

       if(this.props.data.workflow.flowchart!=undefined){
        let f=_.omit(JSON.parse(this.props.data.workflow.flowchart),'flowchart');
        myModel.nodeDataArray=f.nodeDataArray;
        myModel.linkDataArray=f.linkDataArray;
        myDiagram.model=myModel;
       }
       
    self.setState({myDiagram:myDiagram});
    myDiagram.addDiagramListener("ExternalObjectsDropped",function(e){
        let elements=JSON.parse(myDiagram.model.toJson());
        console.log(elements)
        let diamond=_.last((elements.nodeDataArray));
        console.log(diamond)
        if(diamond.figure){
             $('#taskButton').trigger('click');

        }
         if(e.parameter.Kb.id === "task"){
            $('#triggerModal').trigger('click');
             e.subject.each(function(node) {
            let model = e.diagram.model;
            let lastObject = model.If[model.If.length - 1];
            console.log(lastObject);
            self.setState({lastPageObject: lastObject});
         });
         }

         console.log(e.parameter)
        // return;
    });
    //track data change
    myDiagram.addModelChangedListener(function(e){

        self.setState({data: myDiagram.model.toJson()});
        console.log(myDiagram.model)
        let obj=JSON.parse(myDiagram.model.toJson());
        let ele=obj.nodeDataArray;
        let pages=[];
        _.map(ele,function(single){
            if(single.type=="page"){
                pages.push(single)
            }
        })
        self.setState({pages:pages})
    })
    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
            if (idx < 0) document.title += "*";
        } else {
            if (idx >= 0) document.title = document.title.substr(0, idx);
        }
    });
    // helper definitions for node templates
    // define the Node templates for regular nodes
    var lightText = 'whitesmoke';
    myDiagram.nodeTemplateMap.add("", // the default category
        $$(go.Node, "Spot", self.nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $$(go.Panel, "Auto", $$(go.Shape, "Rectangle", {
                fill: "#00A9C9",
                stroke: null
            }, new go.Binding("figure", "figure")), $$(go.TextBlock, {
                font: "bold 11pt Helvetica, Arial, sans-serif",
                stroke: lightText,
                margin: 8,
                maxSize: new go.Size(160, NaN),
                wrap: go.TextBlock.WrapFit,
                editable: true
            }, new go.Binding("text").makeTwoWay())),
            // four named ports, one on each side:
            self.makePort("T", go.Spot.Top, false, true), self.makePort("L", go.Spot.Left, true, true), self.makePort("R", go.Spot.Right, true, true), self.makePort("B", go.Spot.Bottom, true, false)));
    myDiagram.nodeTemplateMap.add("Start", $$(go.Node, "Spot", self.nodeStyle(), $$(go.Panel, "Auto", $$(go.Shape, "Circle", {
            minSize: new go.Size(40, 40),
            fill: "#79C900",
            stroke: null
        }), $$(go.TextBlock, "Start", {
            font: "bold 11pt Helvetica, Arial, sans-serif",
            stroke: lightText
        }, new go.Binding("text"))),

        // three named ports, one on each side except the top, all output only:
        self.makePort("L", go.Spot.Left, true, false), self.makePort("R", go.Spot.Right, true, false), self.makePort("B", go.Spot.Bottom, true, false)));
    myDiagram.nodeTemplateMap.add("End", $$(go.Node, "Spot", self.nodeStyle(), $$(go.Panel, "Auto", $$(go.Shape, "Circle", {
            minSize: new go.Size(40, 40),
            fill: "#DC3C00",
            stroke: null
        }), $$(go.TextBlock, "End", {
            font: "bold 11pt Helvetica, Arial, sans-serif",
            stroke: lightText
        }, new go.Binding("text"))),
        // three named ports, one on each side except the bottom, all input only:
        self.makePort("T", go.Spot.Top, false, true), self.makePort("L", go.Spot.Left, false, true), self.makePort("R", go.Spot.Right, false, true)));
    myDiagram.nodeTemplateMap.add("Comment", $$(go.Node, "Auto", self.nodeStyle(), $$(go.Shape, "File", {
            fill: "#EFFAB4",
            stroke: null
        }), $$(go.TextBlock, {
            margin: 5,
            maxSize: new go.Size(200, NaN),
            wrap: go.TextBlock.WrapFit,
            textAlign: "center",
            editable: true,
            font: "bold 12pt Helvetica, Arial, sans-serif",
            stroke: '#454545'
        }, new go.Binding("text").makeTwoWay())
        // no ports, because no links are allowed to connect with a comment
    ));
    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate = $$(go.Link, // the whole link panel
        {
            routing: go.Link.AvoidsNodes,
            curve: go.Link.JumpOver,
            corner: 5,
            toShortLength: 4,
            relinkableFrom: true,
            relinkableTo: true,
            reshapable: true,
            resegmentable: true,
            // mouse-overs subtly highlight links:
            mouseEnter: function(e, link) {
                link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)";
            },
            mouseLeave: function(e, link) {
                link.findObject("HIGHLIGHT").stroke = "transparent";
            }
        }, new go.Binding("points").makeTwoWay(), $$(go.Shape, // the highlight shape, normally transparent
            {
                isPanelMain: true,
                strokeWidth: 8,
                stroke: "transparent",
                name: "HIGHLIGHT"
            }), $$(go.Shape, // the link path shape
            {
                isPanelMain: true,
                stroke: "gray",
                strokeWidth: 2
            }), $$(go.Shape, // the arrowhead
            {
                toArrow: "standard",
                stroke: null,
                fill: "gray"
            }), $$(go.Panel, "Auto", // the link label, normally not visible
            {
                visible: false,
                name: "LABEL",
                segmentIndex: 2,
                segmentFraction: 0.5
            }, new go.Binding("visible", "visible").makeTwoWay(), $$(go.Shape, "RoundedRectangle", // the label shape
                {
                    fill: "#F8F8F8",
                    stroke: null
                }), $$(go.TextBlock, "Yes", // the label
                {
                    textAlign: "center",
                    font: "10pt helvetica, arial, sans-serif",
                    stroke: "#333333",
                    editable: true
                }, new go.Binding("text").makeTwoWay())));
    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

    let pageModel=self.props.data.pages.map((single)=>{
        //let formData=_.findWhere(self.props.data.forms,{_id:single.formId});
            let formData=single.form;
                return {id:single._id,text:single.name,type:'page',formData:formData}
    })

    this.setState({pageModel:pageModel});
     let taskModel=[{id:1,text:"email",type:'task'}]
    // console.log(taskModel)
    // load an initial diagram from some JSON text
    // initialize the Palette that is on the left side of the page
     tools = $$(go.Palette, "tools", // must name or refer to the DIV HTML element
        {
            "animationManager.duration": 800, // slightly longer than default (600ms) animation
            nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
            model: new go.GraphLinksModel([ // specify the contents of the Palette
                {
                    category: "Start",
                    text: "Start"
                },{
                    text: "???",
                    figure: "Diamond",
                    title:"diamond"
                }, {
                    category: "End",
                    text: "End"
                }, {
                    category: "Comment",
                    text: "Comment"
                }
            ])
        });
    pages = $$(go.Palette, "pages", // must name or refer to the DIV HTML element
        {
            "animationManager.duration": 800, // slightly longer than default (600ms) animation
            nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
            model: new go.GraphLinksModel(pageModel)
        });
        task = $$(go.Palette, "task", // must name or refer to the DIV HTML element
        {
            "animationManager.duration": 800, // slightly longer than default (600ms) animation
            nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
            model: new go.GraphLinksModel(taskModel)
        });
        

  }
  inputRoles(){
    console.log("clicked");
  }
  showPorts(node, show) {
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
    node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
    });
  }
  saveFlowchart(){
      let flowchartData=myDiagram.model.toJson();
      let data={charts:this.state.charts,flowchartData:flowchartData,workflow:this.props.data.workflow._id,action:this.state.action,email:this.state.emailDetail}
      console.log(data);
       Meteor.call('saveFlowchart',data);
       Alert.success("flowchart saved sucessfully", {
               position: 'top-right',
               effect: 'bouncyflip',
               timeout: 1000
           })
           $('.save-flowchart').attr('disabled',"ture")
  }
    componentDidMount(){
        let self=this;
        self.init(self);
    }
    
  createSelectOptions() {
        let self = this;
        let items = [];        
        if(self.state.data){
            var json = JSON.parse(self.state.data);
            var obj  = json.nodeDataArray;
            for(i=0;i<obj.length;i++){
                if(obj[i].type==="page")
                    items.push(<option key={i} id={obj[i].id} index={i} value={obj[i].text}>{obj[i].text}</option>);
            }
        }
         return items;
   }  
   
   changePages(e){
        //load roles
        var self = this;
        var index = $('option:selected', '#selectRoles #pages').prop('index');
        var json = JSON.parse(self.state.data);
        var obj  = json.nodeDataArray;
       var roles = obj[index].roles || [];
        $('#rolesCheck .checkbox').each(function(){
            if(roles.indexOf($('input',this).val())===-1)
                $('input',this).prop("checked",false);
            else
                $('input',this).prop("checked",true);
        });
   }
   updateRoles(e){
        var self = this;
       var index = $('option:selected', '#selectRoles #pages').prop('index');
       console.log(index)
        var roles = [];
        $('#rolesCheck .checkbox').each(function(){
            if($('input',this).is(':checked')){
             roles.push($('input',this).val());
            }
        });
        var json = JSON.parse(self.state.data);
        var obj  = json.nodeDataArray;
        console.log(obj[index])
        obj[index].roles = roles;
        console.log(obj[index],roles)
        let myDiagram = self.state.myDiagram;
        myDiagram.model = go.Model.fromJson(json);
   }

  render(){
      let self=this;
      console.log(self.state.email.length);
    return(<div className="col-md-10 registration_form pad_t50">
      <div className="row">
        <div className="col-md-3" style={{"backgroundColor":"#f9f9f9"}}>
        <h3 className="col-md-offset-6">Tools</h3>
          <div id="tools" className="work_flow_menu">
        </div>
        <h3 className="col-md-offset-6" >Pages</h3>
        <div id="pages" className="work_flow_menu">
        </div>
        <h3 className="col-md-offset-6">Tasks</h3>
          <div id="task" className="work_flow_menu">
          </div>
          
        </div>

        <div className="col-md-9">
        <div className="workflow_cont">

        <h1 className="title">define work flow</h1><br />
     <br />
     <p>Create flowchart in the manner that flowchart flows i.e start drag and drop the the start component</p>
     <div className="container">
    <div style={{width:"100%", whiteSspace:"nowrap"}}>
    

    <span style={{display:"inline-block",verticalAlign:"top",padding:5, width:"80%"}}>
      <div id="myDiagramDiv" style={{border:" solid 1px gray",height:720,width:700}}></div>
    </span> 
  </div>
  
    </div>
     

     <br />
     <div>
     <div>
     <h3>Choose charts for flowchart</h3>
        <CheckboxGroup onChange={(newCheck)=>{
            var charts={};
            _.each(newCheck,(check)=>{
               charts[check]=true
            })
            this.setState({charts:charts});
            
        }}>
        <ul style={{listStyleType:"none",margin:0,padding:0,display:'inline'}}>
        <li><Checkbox value="barChart"/>Bar chart</li>
        <li><Checkbox value="lineChart"/>Line Chart</li>
        <li><Checkbox value="pieChart"/>Pie Chart</li>
        
        </ul>
        
        </CheckboxGroup>
     </div>
     <div className="pull-right">
     <button className="btn btn-primary " id="triggerModal" data-toggle="modal" href='#email'>add email
    </button>&nbsp;&nbsp;&nbsp;
    <button className="btn btn-primary" data-toggle="modal" href='#modal'>Assign Roles
    </button>&nbsp;&nbsp;&nbsp;
    <button className="btn btn-primary hidden" data-toggle="modal" href="#exampleModalLong" id="taskButton"></button>
     <button className="btn btn-primary save-flowchart" onClick={this.saveFlowchart.bind(this)}>save
    </button>
     </div> 
     
<div className="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 className="modal-title">Assign task</h4>
                    </div>
                    <div className="modal-body">
                        <legend>Select Page</legend>
                        <form id='selecttask' >
                        <select name="pages" id="pages" onChange={(e)=>{
                            console.log(this.props.data.forms)
                            let page=_.findWhere(this.state.pageModel,{id:e.target.value})
                            console.log(page.formData.fields)
                           
                            let fieldlist=_.map(page.formData.fields,function(per){
                               return per.label;
                            })
                            console.log(fieldlist)
                            this.setState({fieldlist:fieldlist});
                        }} className="form-control" required="required">
                        <option value=""> Choose fields for action</option>
                           {this.state.pages.map((s)=>{
                               return(<option value={s.id}>{s.text}</option>)
                           })}
                        </select>
                        <hr/>

                        <div id="taskcheck">
                               <ul style={{ listStyleType:"none"}}>
                               {this.state.fieldlist.map((single)=>{
                                   return(<li><input name="field" type="radio" value={single}/><label>{single}</label></li>)
                               })}
                               </ul>              
                        </div>
                        <div>
                        <h2>Task</h2>
                        <input name="task" type="radio" value="email"/>Email
                        </div>
                        </form>                        

                    </div>
                    <div className="modal-footer">
                        <button type="button" id="dismissModal" onClick={()=>{
                                        let field=$('input[name=field]:checked').val()
                                       console.log(field)
                                       let task={field:field,task:"email"}
                                       this.setState({action:task})
                        }} className="btn btn-default" >Done</button>
                    </div>
                </div>
            </div>
</div>
   
    </div>
     <br />


        </div>


        </div>
      </div>
    {/*modal for email*/}
      <div className="modal fade" id="email">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 className="modal-title">Enter email detail</h4>
                    </div>
                    <div className="modal-body">
                <div className="input-group">
                <label>To</label>
                <input type="email" value={self.state.email.length!=0?self.state.email.body:''} id="emails" className="form-control"
                 placeholder="email" />
                </div> 
                <div className="input-group">
                <label>CC</label>
                <input type="email" placeholder="Add CC" className="form-control" onKeyUp={(e)=>{
                    if(e.keyCode==13){
                        console.log(e.target.value)
                        let m=self.state.cc;
                        m.push(e.target.value)
                        self.setState({cc:m})
                        e.target.value=''
                }
                }}/>
                <ul className="cc">
                
                {self.state.email.length!=undefined && self.state.email.length!==0?self.state.email.cc.map((single)=>{
                return(<li >{single}<i className=""></i></li>)
                }):<li></li>}
                
                </ul>
                </div> 
                <div className="input-group">
                <label>BCC</label>
                <input type="email" placeholder="Add BCC" className="form-control" onKeyUp={(e)=>{
                    if(e.keyCode==13){
                        let m=this.state.bcc;
                        m.push(e.target.value)
                        self.setState({bcc:m})
                        e.target.value=''
                }
                }}/>
                <ul className="cc">
                {self.state.email.length!=undefined && self.state.email.length!==0?this.state.email.bcc.map((single)=>{
                    return(<li>{single}</li>)
                
                }):<li></li>}
                
                </ul>
                </div>              
                <div className="input-group">
                <label>Subject</label>
                <input type="text" id="subject" value={self.state.email.length!=0?self.state.email.subject:''} className="form-control"
                 placeholder="subject" />
                </div>  
                <div className="input-group">
                <label>Body</label>
                <textarea type="text" id="body"  value={self.state.email.length!=0?self.state.email.body:''} className="form-control"
                 placeholder="body"></textarea>
                </div>    
                <button className="btn btn-sucess" onClick={()=>{
                             let data=JSON.parse(this.state.data)
                             let length=data.nodeDataArray.length;
                             data.nodeDataArray[length-1].emailTo=$('#emails').val();
                             data.nodeDataArray[length-1].subject=$('#subject').val();
                             data.nodeDataArray[length-1].body=$('#body').val();
                              myDiagram.model = go.Model.fromJson(data);
                              let to=$('#emails').val(),
                              subject=$('#subject').val(),
                              body=$('#body').val(),
                              cc=this.state.cc,
                              bcc=this.state.bcc,
                              email={to:to,subject:subject,body:body,cc:cc,bcc:bcc};
                              this.setState({emailDetail:email})
                         }} >Done</button>   
                    </div>
                    <div className="modal-footer">
                        <button type="button" id="dismissModal"
                         className="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div></div>
            
      {/*Roles modal*/}
        <div className="modal fade" id="modal">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 className="modal-title">Assign Roles</h4>
                    </div>
                    <div className="modal-body">
                        <legend>Select Page</legend>
                        <form id='selectRoles' >
                        <select name="pages" id="pages" onChange={this.changePages.bind(this)} className="form-control" required="required">
                           <option>choose page</option>
                            {this.createSelectOptions()}
                        </select>
                        <hr/>
                        <div id="rolesCheck">
                        {this.props.data.workflow.roles.map((role)=>{
                            return (<div className="checkbox" onChange ={this.updateRoles.bind(this)}>
                            <label>
                                <input type="checkbox" name={role} value={role}/>
                                {role}
                            </label>
                        </div>   )
                        })}
                                              
                        </div>
                        </form>                        

                    </div>
                    <div className="modal-footer">
                        <button type="button" id="dismissModal" className="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    )
  }
}