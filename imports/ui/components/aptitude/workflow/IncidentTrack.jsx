import React, { Component } from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

export default class IncidentTrack extends Component {
    constructor(props) {
        super(props);
        this.state = {
            interval_type:'',
            interval:'',
            selected:''
        }
    }

    render() {
        console.log(this.props);
        return(<div className="modal fade" id="setinterval" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display: 'none'}} aria-hidden="true">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 className="modal-title">Assign worlflow to client</h4>
                </div>
                <div className="modal-body">
                    <div>
                        Set interval in hour or minutes.
                        <div>
                            <label><input type="radio" checked= {this.state.interval_type === 'hour'}  name="interval_type" onClick={e=>{
                                this.setState({interval_type: e.target.value});

                            }} value="hour"/>hour</label>&nbsp;&nbsp;&nbsp;
                            <label><input type="radio" checked= {this.state.interval_type === 'minute'} name="interval_type" onClick={e=>{
                                this.setState({interval_type: e.target.value});
                            }} value="minute"/>minute</label>
                        </div>
                        <span>Every</span><input type="number" value ={this.state.interval} onChange ={e=>{ this.setState({interval:e.target.value})}}/> <span>{this.state.interval_type}</span>
                    </div>
                    <div>
                        <button className="btn btn-sm btn-primary set-button" onClick={e=>{
                            let workflow = this.props.workflow;
                            let interval = {interval: this.state.interval, type:this.state.interval_type}
                            workflow.interval = interval;
                            Meteor.call('updateWorkflowPageRoles',{id: this.props.id, workflow: workflow},(err, res) =>{
                                if (!err) {
                                    this.setState({
                                        interval_type: '',
                                        interval: ''
                                    })
                                }
                            });
                        }}>Set interval</button>
                    </div>
                    <div>
                       <label> Assign master roles </label>
                        <div>
                            <CheckboxGroup value={this.props.workflow.masterRole ? this.props.workflow.masterRole:[]} onChange={ newRoles =>{
                                let workflow = this.props.workflow;
                                workflow.masterRole = newRoles;
                                let page = workflow.flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'][0];
                                page.detail.roles = newRoles;
                                Meteor.call('updateWorkflowPageRoles',{id: this.props.id, workflow: workflow});
                            }}>
                                {this.props.workflow.roles.map(single => {
                                return (<label><Checkbox id ={single} value={single}/>{single}</label>)
                                })}
                            </CheckboxGroup>
                        </div>
                    </div>
                    <div>
                       <label> Assign client roles </label>
                        <div>
                            <CheckboxGroup value={this.props.workflow.clientRole ? this.props.workflow.clientRole:[]} onChange={ newRoles =>{
                                let workflow = this.props.workflow;
                                workflow.clientRole = newRoles;
                                let page = workflow.flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'][0];
                                page.detail.roles = newRoles;
                                Meteor.call('updateWorkflowPageRoles',{id: this.props.id, workflow: workflow});
                            }}>
                                {this.props.workflow.roles.map(single => {
                                return (<label><Checkbox id ={single} value={single}/>{single}</label>)
                                })}
                            </CheckboxGroup>
                        </div>
                    </div>
                    <div>
                        Set start time:<input type="datetime-local" onChange={ e =>{
                            let workflow = this.props.workflow;
                            workflow.start_time = e.target.value;
                            Meteor.call('updateWorkflowPageRoles',{id: this.props.id, workflow: workflow});
                        }}/>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
            </div>
        </div>
    </div>);
    }
}