//edit,delete and lists client
import React ,{Component} from 'react'
import crudClass from '../../common/crudClass.js'
import orderBy from 'lodash/orderBy';
import Paginate from '../../common/paginator.jsx'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
import ClientModal from '../../../container/clientModal.js';
import WorkFlowAssignation from '../../../container/worflowAssignation.js';
import IncidentTrack from '../../../container/IncidentTrack';
import CopyPublicLink from './copyPublicLink';
import './worflow.css';
export default class ManageWorkFlow extends Component {
  constructor(props) {
   super(props)

   this.state = {
     rows:props.data.workflows,
     workflow_id:null,
     assignClient:null,
     pageRoles:null,
     incident_track:null,
   }
  }

componentDidMount(){
  this.setState({rows:this.props.data.workflows})
}

componentWillReceiveProps(){
  this.setState({rows:this.props.data.workflows})
}
  render(){
    var self=this;
    console.log(self.state.incident_track);
      return(

      <div>
      <Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true} noDataText="No Workflow found.">
            {this.props.data.workflows.map(function(row) {
              {/*https://aptitude-bpmn.herokuapp.com/*/}
              {/*http://localhost:4000/*/}
              let url ="http://54.169.152.181:8082/"+row._id;
              
              let edit = "/aptitude/edit-workflow/"+row._id;
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Name">{row.name}</Td>
                            <Td column="Description">{row.description}</Td>
                            <Td column="Status">{row.status}</Td>
                            <Td column="Action">
                              <div>
                              <ReactTooltip id='edit' type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                                <a href={edit} data-tip data-for='edit' className="btn btn-round btn-success">
                                    <i className="fa fa-pencil-square-o"></i>
                                </a>&nbsp;&nbsp;

                                <ReactTooltip id='delete' type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                                 <a href="#" className="btn btn-round btn-danger" data-tip data-for='delete'   onClick={()=>{
                                  let obj=new crudClass()
                                  obj.delete('deleteWorkFlow',row._id)
                                }}><i className="fa fa-trash-o"></i></a>&nbsp;&nbsp;

                              <ReactTooltip id='define' type='info'>
                                <span>Define workflow</span>
                              </ReactTooltip>
                              <a href={url} target="_blank" data-tip data-for='define' className="btn btn-round btn-warning">
                                <i className="fa fa-file-code-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                
                              <ReactTooltip id='assign' type='info'>
                                <span>Assign workflow to client</span>
                              </ReactTooltip>
                              <a href="#" onClick={()=>{
                                $('.modal-backdrop').css('z-index',-1);
                                self.setState({assignClient:row._id});
                                $('input[type="radio"]').attr('checked', false);
                                //$('input[type="checkbox"]').attr('checked', false);
                              }}  data-tip data-for='assign' data-toggle="modal" data-target="#clients" className="btn btn-round btn-client">
                                <i className="fa fa-user" aria-hidden="true"></i></a>
                              &nbsp;&nbsp;

                              <ReactTooltip id='pageRoles' type='info'>
                                <span>Assign roles  to workflow pages</span>
                              </ReactTooltip>
                              { row.type != "public" && row.type !="incident_tracking"? <a href="#" onClick={()=>{
                                $('.modal-backdrop').css('z-index',-1);
                                self.setState({pageRoles:row._id});
                                $('input[type="radio"]').attr('checked', false);
                              }}  data-tip data-for='pageRoles' data-toggle="modal" data-target="#assignWorkflow" className="btn btn-round btn-page">
                                <i className="fa fa-file-text" aria-hidden="true"></i></a>:"" }
                              &nbsp;&nbsp;

                              <ReactTooltip id='time_interval' type='info'>
                                <span>Set time interval for notification</span>
                              </ReactTooltip>

                              { row.type == "incident_tracking" ? <a href="#" onClick={()=>{
                                $('.modal-backdrop').css('z-index',-1);
                                self.setState({incident_track:row._id});
                                $('input[type="radio"]').attr('checked', false);
                              }}  data-tip data-for='time_interval' data-toggle="modal" data-target="#setinterval" className="btn btn-round btn-interval">
                                <i className="fa fa-clock-o" aria-hidden="true"></i></a>:"" }
                              &nbsp;&nbsp;

                               { row.type == "public" ? <CopyPublicLink id={row._id}/>:"" }
                            </div>
                                   {/* https://aptitude-workflow.herokuapp.com*/}
                            </Td>
                             
                        </Tr>
                        
                    )
                })}

        </Table>
          {self.state.assignClient ? <ClientModal id={self.state.assignClient}/> : <span></span>}  
          {self.state.pageRoles ?<WorkFlowAssignation id = {self.state.pageRoles} /> : <span></span>}
          {self.state.incident_track ? <IncidentTrack id = {self.state.incident_track}/> : <span></span>}
 </div>);
}
}