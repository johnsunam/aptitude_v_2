import React,{Component} from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
export default function ClientModal(props,context){
    console.log(props.workflow);
    return (<div className="modal fade" id="clients" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style={{display: 'none'}} aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 className="modal-title">Assign worlflow to client</h4>
                                </div>
                                <div className="modal-body">
                                <CheckboxGroup 
                                value ={props.workflow.client}  
                                onChange={(newClients)=>{
                                    Meteor.call('addWorkflowClient',{clients:newClients,id:props.id});
                                }}>
                                    <ul style={{listStyleType:"none"}}>
                                     {props.clients.map(obj=>{
                                        return(<li>
                                        <Checkbox id={obj.user} value={obj.user} />
                                        <label htmlFor="">{obj.companyName}</label>
                                    </li>)
                                    })}                                  
                                    </ul>
                                </CheckboxGroup>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>)
}