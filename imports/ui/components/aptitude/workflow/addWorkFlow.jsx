//add workflow to WorkflowDb

import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
let message = require('../../common/message.json');
import Alert from 'react-s-alert';
import { Session } from 'meteor/session';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
let Confirm = require('react-confirm-bootstrap');
import {FlowRouter} from 'meteor/kadira:flow-router';

export default class AddWorkFlow extends Component {
    constructor(props) {
        super(props);
        this.state={
            saveResult:false,
            edit:props.edit,
            workflow:props.workflow,
            canSubmit: false,
            res:'',
            name:'',
            description:'',
            status:'',
            type:'',
            roles:[],
            selectedClient:[],
            selectedRoles:[],
            showMessage:'',
            message:''
        }
    }

    componentDidMount(){
        this.props.edit?this.setState({name:this.props.workflow.name,
            description:this.props.workflow.description,type:this.props.workflow.type}):this.setState({name:'',
            description:'',type:''});
        this.refs.description.value=this.state.edit?this.props.workflow.description:'';

        this.initVar(this.props);
    }

    initVar(props) {
        let workflow = props.workflow;
        workflow = workflow ? workflow : [];
        if (this.state.edit) {
            this.setState({
                name: workflow.name,
                description: workflow.description,
                type: workflow.type
            })
        } else {
            this.setState({
                name: '',
                description: '',
                type: ''
            })
        }
    }

    componentWillReceiveProps(nextProps){
        this.initVar(nextProps);
    }

    shouldComponentUpdate(nextProps, nextState){
        let self=this;
        Tracker.autorun(function(){

            if(Session.equals('confirm',true)){
                if(Session.get('res')==true){
                    console.log('helo')
                    self.setState({showMessage:true,message:"WorkFlow Saved Sucessfully"})
                    $('.message').addClass('su')
                }else{
                    self.setState({showMessage:true,message:"Error Saving WorkFlow"})
                    $('.message').addClass('er')
                }
                Session.set('confirm',false)
            }
        })

        return true;
    }

    enableButton() {
        this.setState({ canSubmit: true });
    }
    disableButton() {
        this.setState({ canSubmit: false });
    }
    
    submit(e){
        let self=this;
        let obj= new crudClass();
        let name=$("#name").val();
        let description=$("#description").val();
        let type = $("#workflow_type").val();
        let status=$('#status:checked').val() !== undefined;
        let admin=window.localStorage.getItem('user');
        let user=admin?admin:Meteor.userId();
        let record=this.props.edit?{id:this.props.workflow._id,data:{name:name,description:description,type:type}}:
            {user:user,name:name,description:description,type:type};
        self.state.edit?obj.create('editWorkFlow',record):obj.create('addWorkFlow',record);
        self.setState({roles:[]});
        self.refs.form.reset();
        
        FlowRouter.go('/aptitude/manage-workflow');

    }
    triggerConfirm()
    {
        $("#confirm").trigger('click');
    }
    render(){
        let submitButton=<button className="btn btn-primary" type="submit" disabled={!this.state.canSubmit} ><span>Save</span></button>
        return(
            <div className="row">
                <div className="col-md-8">
                    <div>
                        <div>
                            <h4 className="title">{this.props.edit?'Edit Workflow':'Add Workflow'}</h4>
                        </div>
                        <hr/>
                        <div className="card-content">
                            <Formsy.Form ref="form" onValidSubmit={this.triggerConfirm.bind(this)} id="addWorkflow" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="input-container">
                                            <MyInput type="text" className="form-control" maxLength="20" name="name" id="name" title="Name" help="Enter the name of the workflow" ref="name" value={this.props.edit?this.props.workflow.name:''} required/>
                                        </div>
                                        <div className="input-container">
                                            <MyInput type="text" maxLength="200" className="form-control"  title="Description" id="description" help="Enter the name fo description" ref="description" name="description"value={this.props.edit?this.props.workflow.description:''} required/>
                                        </div>
                                        <div className="input-container" >
                                            <select name="workflow_type" className="form-control" id="workflow_type">
                                                <option value="">{this.props.edit? this.state.workflow.type : 'Choose type'}</option>
                                                <option value="login_survey">Login Survey</option>
                                                <option value="logout_survey">Logout Survey</option>
                                                <option value="normal">Normal</option>
                                                <option value="public">Public</option>
                                                <option value="track_incident">Track incident</option>
                                            </select>
                                        </div>
                                        <br/>
                                    </div>
                                </div>
                                <br/>
                                <div style={{float: 'right'}}>
                                    {submitButton}
                                    &nbsp;
                                    <a className="btn btn-warning "
                                       onClick={()=>{
                                           this.props.edit?this.refs.form.reset(this.props.workflow):this.refs.form.reset();
                                           $('select').prop('selectedIndex',0);
                                           this.setState({roles:[]})
                                       }}>Reset</a>
                                </div>
                            </Formsy.Form>
                            <Confirm onConfirm={this.submit.bind(this)}
                                     body="Are you sure you want to submit?"
                                     confirmText="Confirm"
                                     title="Submit Form">
                                <button id="confirm" className="hidden"></button>
                            </Confirm>
                        </div>
                    </div>
                </div>
            </div>)
    }
}
