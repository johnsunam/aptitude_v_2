import React, {Component} from "react";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ReactTooltip from 'react-tooltip'

export default class CopyPublicLink extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            copied: false,
        };  

    }
    componentDidMount() {
        this.setState({
            value : `${location.origin}/aptitude/public/${this.props.id}`
        })
    }
    
    render() {
        return (<div>
            <input value={this.state.value}
            onChange={({target: {value}}) => this.setState({value, copied: false})}  style={{display:"none"}}/>

            <ReactTooltip id={this.props.id} type='info'>
                <span>{`${location.origin}/aptitude/public/${this.props.id}`}</span>
            </ReactTooltip>
            <CopyToClipboard text={this.state.value}
            
            onCopy={() => this.setState({copied: true})}>
            <a href="#" className="btn btn-round btn-primary" data-tip data-for={this.props.id}><i className="fa fa-clipboard" aria-hidden="true" ></i></a>
            </CopyToClipboard>
    
            {this.state.copied ? <span style={{color: 'red'}}>Copied.</span> : null}
      </div>)
    }
}