import React,{Component} from 'react';
import ManageWorkFlow from '../../../container/manageWorkFlow.js';

export default class ChoooseClient extends Component{
    constructor(props){
        super(props);
        this.state={
            client:null
            
        }
    }
    render(){
       console.log(this.state.client);
        return( 
        <div className="row">
          <div className="col-md-12">
            <div className="card">
                <div>
                    <h4 className="title">Manage Workflow</h4>
                    <p className="category">Here is list of workflows you can manage.</p>
                    <a  className="btn btn-primary" href="/aptitude/add-workflow" style={{position: 'absolute', top: 7, right: 11}} >Add Workflow</a>
                </div>
                <hr/>
                <div className="row">
                    <div className="col-md-4  col-md-offset-4">
                        <div className="panel-box" style={{width: 'inherit', color: 'white', fontSize: 15}}>
                            <div className="panel-content bg-gradient-9">
                                <i className="fa fa-user" style={{fontSize: 20}}></i>
                                <p>Choose Client Id</p>
                            </div>
                            <div className="panel-content pad15A bg-white">
                                <div className="center-vertical">
                                    <select className="form-control" onChange={(e)=>{
                                        console.log(e.target.value);
                                        this.setState({client:e.target.value==null || e.target.value==""?null:e.target.value})
                                    }}>
                                    <option value="" >Choose Client</option>
                                        {this.props.clients.map((single)=>{
                                            console.log(single);
                                            return(<option value={single.user}>{single.companyName}</option>)
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
              <div className="card-content table-responsive">
                 <ManageWorkFlow client={this.state.client}/>
              </div>
            </div>
          </div>
        </div>
        )
    }
}