import React, { Component } from 'react';
import FieldList from '../../../container/fieldList';

export default class ConfigureField extends Component {
    constructor(props){
        super(props);

        this.state = {

            configurable :[],
            page:'',
            values:[],
            data:{}
        }
        
        
    }

    render(){
       console.log(this.props);
       let self = this;
       console.log(this.state.values);
        let pages = this.props.workflow.flowchart["bpmn2:definitions"]['bpmn2:process']['bpmn2:userTask'];
        return( <div className="row">
                    <div className="col-md-12">
                        <div className="panel">
                            <div className="panel-body">
                            <div className="well">
                            <h3>Pages</h3>
                            <ul>
                              {pages.map((value,key)=>{
                                return(<li><input type="radio" name="page" value={value.detail.page._id} onChange={(e)=>{
                                        self.setState({configurable:value.detail.page.form.configurableField, page:e.target.value});
                                }}/><label htmlFor="">{value.detail.page.name}</label></li>)
                              })}
                        
                            </ul>
                            </div>
                            <div className="well">
                            <h3>Fields</h3>
                            {self.state.configurable.map((obj,key)=>{
                                return(<li><input type="radio" name="field" value={obj.name} onChange={(e)=>{
                                    self.setState({field:e.target.value});
                                    console.log(e.target.value, self.state.page);
                                    let data = _.findWhere(self.props.configurableValue,{field_name:e.target.value, page:self.state.page});
                                    self.setState({data:data});
                                    self.setState({values:data.values})
                                }}/><label>{obj.value}</label></li>)
                            })}
                            </div>
                            <div className="well row">

                                <div className="col-md-6">
                                <h3>Field Values</h3>
                                    <ul>
                                    {self.state.values.map((obj)=>{
                                        console.log(obj);
                                        return (<li><input type="checkbox" name="val" value={obj.value} /><label htmlFor="">{obj.value}</label></li>)
                                    })}
                                    </ul>
                                </div>
                                <div className="col-md-6">
                                  <h3>User</h3>
                                    <ul>
                                        {self.props.clientUser.map((obj)=>{
                                            return (<li>
                                                <input type="radio" name="user" value={obj.user} />
                                                <label htmlFor="">{obj.name}</label>
                                            </li>)
                                        })}
                                    </ul>
                                <button className="btn-default block btn-primary" onClick={(e)=>{
                                    let workflow =  self.props.id;
                                    let page = $('input[name="page"]:checked').val();
                                    let field_name = $('input[name="field"]:checked').val();
                                    let values= [];

                                    _.each($('input[name="val"]:checked'),(obj)=>{
                                        console.log(obj);
                                            values.push({value:$(obj).val(), label:$(obj).val()});
                                    });

                                    console.log(values)
                                    let user =  $('input[name="user"]:checked').val();
                                    let data = self.state.data
                                    console.log(data);
                                    let userData = data.userData;
                                    console.log(userData);

                                    if(userData){  
                                        let ex = _.findWhere(self.state.data.userData,{user:user});
                                        ex ? ex.values = _.union(ex.values,values):userData.push({values:values,user:user});
                                    }
                                    else {
                                        data.userData= [{values:values,user:user}];
                                    }
                                    console.log(data);

                                     Meteor.call('addValues',data);
                                }}>add value</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>)

    }
}