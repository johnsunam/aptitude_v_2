//edits,lists and deletes pages

import React ,{Component} from 'react'
import ClientPages from '../../../container/clientPage.js'
import orderBy from 'lodash/orderBy';
import { Table,SearchColumns, search,sort} from 'reactabular';


export default class ManagePage extends Component {
    constructor(props) {
        super(props);
        this.state={
            choosedClient:props.data.clients[0]?props.data.clients[0].companyName:null
        }
    }

    render(){
        console.log(this.props);
        return(
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <div>
                            <h4 className="title">Manage Pages</h4>
                            <p className="category">Here is list of pages you can manage.</p>
                            <a  className="btn btn-primary" href="/aptitude/add-page" style={{position: 'absolute', top: 7, right: 11}} >Add Page</a>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-md-4  col-md-offset-4">
                                <div className="panel-box" style={{width: 'inherit', color: 'white', fontSize: 15}}>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <ClientPages/>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
