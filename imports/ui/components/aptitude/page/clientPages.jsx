
import React,{Component} from 'react';
import AddPage from '../../../container/addPage.js'
import crudClass from '../../common/crudClass.js'
import orderBy from 'lodash/orderBy';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ClientPages extends Component {

  constructor(props) {
   super(props)
   this.state={
    datas:false,pages:[],products:props.pages,currentRows:[],rows:this.props.pages,row:""
    }
  }
enableButton() {
  this.setState({ canSubmit: true });
}
disableButton() {
  this.setState({ canSubmit: false });
}
onChangePage(page) {
  this.setState({currentPage:page})
  let pages=this.state.pages[page-1];
  this.setState({currentRows:pages})

}
  componentDidMount(){
  this.setState({pages:this.props.pages})

}
  componentWillReceiveProps(nextProps){
  this.setState({pages:nextProps.pages});
  }

 

  render(){
    var self=this;
      return(
        <div className="card-content table-responsive">
          <Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true}>
            {this.props.pages.map(function(row) {
              let url = '/aptitude/edit-page/'+row._id;
              let configurable = "/aptitude/configure/"+row._id;

                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Page Name">{row.name}</Td>
                            <Td column="Form Name">{row.formName}</Td>
                            <Td column="Status">{row.status}</Td>
                            <Td column="Action">
                          <div>
                           <ReactTooltip id='edit'  type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
            <a href={url} data-tip data-for="edit" className="btn btn-round btn-success">
            <i className="fa fa-pencil-square-o"></i>
          </a>&nbsp;&nbsp;
       <ReactTooltip id='delete'  type='info'>
          <span>Delete</span>
       </ReactTooltip>
      <a className="btn btn-round btn-danger" data-tip data-for="delete" href="#" id={row._id} onClick={(e)=>{
        let obj=new crudClass()
        obj.delete('deletePage',e.target.id)
  }} disabled={row.state?'disabled':''} >
        <i className="fa fa-trash-o"></i></a>&nbsp;&nbsp;
         <ReactTooltip id='clone'  type='info'>
                                <span>Clone</span>
                              </ReactTooltip>
        <a className="btn btn-round btn-azure" data-toggle="modal" data-tip data-for="clone"  href="#" data-target={`#clone${row._id}`} onClick={(e)=>{
            $('.modal-backdrop').css('z-index','-1');
        }}><i className="fa fa-clone" aria-hidden="true"></i></a>&nbsp;&nbsp;
         <ReactTooltip id='pageEdit'  type='info'>
                                <span>Page Form edit</span>
                              </ReactTooltip>
        <a className="round-primary" data-tip data-for="pageEdit"  id={row._id} onClick={(e)=>{
          let editPath='/aptitude/page-form/'+e.target.id;
          FlowRouter.go(editPath);

        }}><i className="fa fa-file-text-o"  id={row._id} aria-hidden="true"></i></a>&nbsp;&nbsp;
         {/* <ReactTooltip id='configurableFields' type='info'>
                                <span>Add Configurable fields to workflow pages</span>
                              </ReactTooltip>
                              <a href={configurable} data-tip data-for='configurableFields' className="btn btn-round btn-yellow">
                                <i className="fa fa-plus-circle" aria-hidden="true"></i></a>*/}&nbsp;&nbsp;
     <div  className="modal fade" id={`clone${row._id}`} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Page Clone</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <Formsy.Form ref="form" onValidSubmit={(e)=>{
            let data={user:row.user,formName:row.formName,name:e.pagename,status:row.status}
              var obj= new crudClass()
              obj.create('addPage',data);
              $("#pagename").val('');
              }} id="addPage" onValid={self.enableButton.bind(self)} onInvalid={self.disableButton.bind(self)}>
            <MyInput className="form-control" type="text" placeholder="New Page Name" name="pagename" ref="pagename"/>
            <button className="btn btn-primary" type="submit">save</button>
          </Formsy.Form>
        </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-warning" onClick={()=>{
          $("#pagename").val('');
        }}>reset</button>
      </div>
            </div>
           </div>
          </div>
         </div>
        </Td>
      </Tr>)})}
    </Table>
    <div className="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <AddPage edit="true" page={this.state.datas}/>
  </div>
</div>);
}
}
 
 