//adding new pages to the pagedb
import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
var Confirm = require('react-confirm-bootstrap');
import {FlowRouter} from 'meteor/kadira:flow-router';
export default class AddPage extends Component {
    constructor(props) {
        super(props)
        this.state={
            saveResult:false,
            edit:props.data.edit,
            canSubmit: false,
            confirm:Session.get('confirm'),
            res:"",
            name:'',
            clName:'',
            formName:'',
            previewURL:'',
            publishURL:'',
            metakeys:'',
            showMessage:'',
            message:''
        }

    }
    componentDidMount(){
        let page=this.props.data.page;
        this.state.edit?this.setState({name:page.name,
            formName:page.formName,
            previewURL:page.previewURL,
            publishURL:page.publishURL,
            metakeys:page.metakeys,
            status:page.status}):this.setState({name:"",
            formName:"",
            previewURL:"",
            publishURL:"",
            metakeys:""})
    }
    shouldComponentUpdate(nextProps, nextState){
        var self=this;
        Tracker.autorun(function(){
            if(Session.equals('confirm',true)){
                if(Session.get('res')==true){
                    self.setState({showMessage:true,message:"Page Saved Sucessfully"})
                    $('.message').addClass('su')
                }else{
                    self.setState({showMessage:true,message:"Error Saving Page"})
                    $('.message').addClass('er')

                }
                Session.set('confirm',false)
            }

        })

        return true;
    }


    enableButton() {
        this.setState({ canSubmit: true });
    }
    disableButton() {
        this.setState({ canSubmit: false });
    }
    //add page to PageDb
    submit(e){
        var self=this;
        let name=$('#name').val(),
            formName=$( "#formName option:selected" ).text(),
            formId=this.refs.formName.value,
            admin=window.localStorage.getItem('user'),
            user=admin?admin:Meteor.userId();
        form=_.findWhere(this.props.data.forms,{_id:formId});
        let record=this.props.edit?{id:this.props.page._id,data:{name:name,form:form,formName:formName, status:status}}:
            {user:user,name:name,form:form,formName:formName}
        let obj= new crudClass();
        let res;
        if(self.state.edit){
            res=obj.edit('editPage',record)
        }
        else{
            var exits=_.findWhere(this.props.data.pages,{name:record.name})
            if(exits){
                alert('page name exits')
            }else{
                res=obj.create('addPage',record)
            }
        }
        this.setState({name:"",
            formName:"",
            previewURL:"",
            publishURL:"",
            metakeys:""})
        this.setState({saveResult:res})
        this.refs.form.reset();
        $('select').prop('selectedIndex',0);
        FlowRouter.go('/aptitude/manage-page');
    }
    triggerConfirm()
    {
        $("#confirm").trigger('click');
    }
    render(){
        let submitButton=<button type="submit" className="btn btn-primary" disabled={!this.state.canSubmit}><span>Save</span></button>
        let page=this.props.data.page;
        return(
            <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header" data-background-color="purple">
                            <h4 className="title">{this.state.edit?"Edit Page":"Create page for the client"}</h4>
                        </div>
                        <hr/>
                        <div className="card-content">
                            <Formsy.Form ref="form" onValidSubmit={this.triggerConfirm.bind(this)} id="addPage" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="input-container">
                                            <MyInput type="text" className="form-control" help="Enter your page name" maxLength="30" title="Page Name" name="name" id="name" value={this.props.data.edit?page.name:''}  ref="name" required/>
                                            <div className="bar"></div>
                                        </div>
                                        
                                        <br/>
                                        <div className="input-container" placeholder="form">
                                            <label className="control-label"><b>Choose form</b></label>
                                            <select ref="formName" id="formName" className="form-control">
                                                <option>{this.props.data.edit?page.formName:'choose form'}</option>
                                                {this.props.data.forms.map((form)=>{
                                                    return(<option value={form._id}>{form.name}</option>)
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div style={{float: 'right'}}>
                                    {submitButton}
                                    &nbsp;
                                    <a className="btn btn-warning "
                                       onClick={()=>{
                                           this.props.data.edit?this.refs.form.reset(this.props.page):this.refs.form.reset();
                                       }}>Reset</a>
                                </div>
                            </Formsy.Form>
                            <Confirm onConfirm={this.submit.bind(this)}
                                     body="Are you sure you want to submit?"
                                     confirmText="Confirm"
                                     title="Submit Form">
                                <button id="confirm" className="hidden"></button>
                            </Confirm>
                        </div>
                    </div>
                </div>
            </div>)
    }
}
