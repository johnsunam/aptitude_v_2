import React, { Component } from 'react';
var Confirm = require('react-confirm-bootstrap');
import Alert from 'react-s-alert';

export default class FieldsModal extends Component {
    constructor (props) {
        super(props)
        // this.state = {
        // }
    }

    render(){
        return(<div style={{marginTop:85}} className="well">
            <div>
            <label htmlFor="">Field Name</label>
            <input type="text" className="form-control" ref = "label" />
            </div>
          {this.props.keys ? <button style={{marginTop:10}} className="btn btn-primary"  onClick = {() => {
          
          let fieldname = this.refs.label.value;
          let newField;
          let label = this.refs.label.value;
          let name =this.props.fieldType+'-'+ Math.floor(Math.random() * 10000000000000);
          
          if(label != "") {
                 
                         newField ={
                            className:'form-control',
                            label:this.refs.label.value,
                            name:name,
                            type:this.props.fieldType,
                            values:[]
                         }
                           let tem = this.props.form;
                            if(this.props.fieldType == 'before') {
                                tem.splice(this.props.keys,0,newField)
                            } else {
                                tem.splice(this.props.keys + 1,0,newField)
                            }

                            let formDetail = this.props.formDetail;
                            formDetail.defaultFields.push({name:name, label:this.refs.label.value});
                            
                            let obj = {name:name, value:this.refs.label.value};
                            formDetail.configurableField ? formDetail.configurableField.push(obj) : formDetail.configurableField = [obj];
                            console.log(formDetail);
                            formDetail.form = JSON.stringify(JSON.stringify(tem));
                            Meteor.call('updateConfigurationField',this.props.page._id,formDetail)
           
          } else {

                Alert.warning("Field name cannot be empty", {
                    position: 'top-right',
                    effect: 'bouncyflip',
                    timeout: 1000
                });
          }

          
         
        
            }}>Add field</button> : <span></span>}
          
        </div>)
    }
}