import React, { Component } from 'react';
import FieldsModal from '../../../container/fieldsModal';


export default class  FieldList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedField :null
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({selectedField:null})
    }
    
    render(){
        console.log(this.state.selectedField);
        let self = this;
         return (<div>
        <ul style = {{listStyleType: "none"}}>
            {this.props.label.map((value,key)=>{
                return (<li><input 
                type="radio" 
                onClick = {e=>{
                    $('.modal-backdrop').css('z-index',-1);
                    this.setState({selectedField:key})
                    }}
                value = {this.props.name[key]}
                data-toggle="modal"  
                data-target="#fields" 
                
               name="field"/> <label>{value}</label></li>)
            })}
        </ul>

        <FieldsModal page = { this.props.page } insertType = { this.props.insertType } keys = { this.state.selectedField } fieldType={this.props.fieldType} /> 
    </div>)
    }
   
}