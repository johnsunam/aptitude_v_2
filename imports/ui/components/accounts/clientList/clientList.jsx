import React, {Component} from 'react'
export default class ClientList extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        let clientlist=this.props.clientlist.length !== 0 ? this.props.clientlist.map((single)=>{
            return(<label><input type="radio" name="optradio" id={single._id} onClick={(e)=>{
                this.props.choosedLogin({choosedUser:e.target.id,choosedName:single.companyName,email:single.email,userType:'client'});
            }}/>{single.companyName}</label>)
        }):<h2>this is no admin to choose</h2>;
        return(<div>
                <button type="button" className="btn btn-primary btn-lg" data-toggle="modal" data-target="#client">
                    client
                </button>
                <div className="modal fade" id="client" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Choose client for login</h4>
                            </div>
                            <div className="modal-body">
                                {clientlist}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
