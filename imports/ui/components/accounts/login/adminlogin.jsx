import React, {Component} from 'react';
import {FlowRouter} from 'meteor/kadira:flow-router';
import Alert from 'react-s-alert';
import ChooseAccount from '../../../container/chooseAccounts.js'

export default class AdminLogin extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            showMessage: false,
            loggedIn: undefined,
            choosed: false,elements:''
        }
        
                                                                                                                                                                                                                               }

    enableButton() {
        this.setState({canSubmit: true});
    }

    disableButton() {
        this.setState({canSubmit: false});
    }

    submit(e) {

        let user = e.username;
        let password = e.password;
        let self = this;
        Meteor.loginWithPassword(user, password, function(err) {
            //  Code for error check if then alert them.
            if (err) {
                Alert.warning(err.reason, {
                    position: 'top-right',
                    effect: 'bouncyflip',
                    timeout: 1000
                });
            } else {
                let now = new Date();
                let time = now.getTime();
                let expireTime = time + 1000 * 3600;
                now.setTime(expireTime);
                document.cookie = 'loginStatus=ok;expires=' + now.toGMTString();
                window.localStorage.setItem('userAccess', Meteor.user().roles);
                self.setState({loggedIn: Meteor.userId()});
                $('#userbutton').trigger('click');
                $('.modal-backdrop').css('z-index',-1);
            }
        })
    }

    static closeModal() {
        $('#userbutton').trigger('click');
    }

    static componentDidMount() {
        window.sessionStorage.setItem("logout",false);
    }

    componentWillReceiveProps(nextProps){
        if(window.localStorage.getItem("appType") === "aptitude"){
            let component = (
                <div>
                    <button className="btn btn-primary btn-sm pull-left" onClick={()=>{
                        window.localStorage.setItem('user',nextProps.data.aptitudeAdmin.user);
                        AdminLogin.closeModal();
                        FlowRouter.go('/aptitude/add-form')
                    }}>
                        Admin
                    </button>
                    {nextProps.data.companies.length !== 0 ?
                    <button className="btn btn-primary btn-sm pull-right" onClick={()=>{
                         $('.chooseAccess').hide();
                         this.setState({choosed:true})
                    }}>
                        Clients
                    </button> : <span></span>}
                </div>
            );
            this.setState({elements:component});
        }
        else if (window.localStorage.getItem("appType") === "client-admin") {
            let component = (
                <div><button className="btn btn-primary btn-sm pull-left" onClick={()=>{
                    window.localStorage.setItem('subType','client');
                    this.setState({choosed:true});
                    $('.chooseAccess').hide();
                    }}>
                    Admin
                    </button>
                    <button className="btn btn-primary btn-sm pull-right" onClick={()=>{
                        window.localStorage.setItem('subType','App-User');
                        $('.chooseAccess').hide();
                        this.setState({choosed:true})
                    }}>
                        User
                    </button>
                </div>
            );
            this.setState({elements:component});
        } else {
            let component=nextProps.clientUser.userTypes.map((type)=>{
                return(
                    <button className="btn btn-primary btn-sm pull-left col-md-offset-2" id={type} onClick={(e)=>{
                        window.localStorage.setItem('subType',e.target.id);
                        this.setState({choosed:true});
                        $('.chooseAccess').hide();
                    }}>
                        {type === "client" ? "Admin" : type}
                    </button>
                )
            });
            this.setState({elements:component})
        }
    }

    render() {
        let rolestr = window.localStorage.getItem('userAccess');
        let roles = Meteor.userId()
            ? rolestr.split(',')
            : [];

        return (<div id="login-container">
                    <h1 className="h2 text-light text-center push-top-bottom animation-slideDown">
                        <i className="fa fa-map-marker"></i> <strong>Aptitude</strong>
                    </h1>
                    <div className="block animation-fadeInQuickInv" style={{height:272}}>
                        <div className="block-title">
                            <div className="block-options pull-right">
                                <a href="forgotpass.html" className="btn btn-effect-ripple btn-primary" data-toggle="tooltip" data-placement="left" title="" style={{overflow: "hidden", position: "relative"}} data-original-title="Forgot your password?"><i className="fa fa-exclamation-circle"></i></a>
                            </div>
                            <h2>Please Login</h2>
                        </div>
                        <Formsy.Form ref="form" class="form-horizontal" onValidSubmit={this.submit.bind(this)}  onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                            <MyInput className="form-control name" type="text" help="Please enter the valid email or username" title="Email or username" name="username" ref="username"/>
                            <MyInput className="form-control pass" type="password" help="Enter the password" id="password" title="Password" name="password" ref="password"/>
                            <div className="form-group row">
                                <div className="col-xs-8">
                                    <label className="csscheckbox csscheckbox-primary">
                                        <input type="checkbox" id="login-remember-me" name="login-remember-me" />
                                        <span></span>
                                    </label>
                                    Remember Me?
                                </div>
                                <div className="col-xs-4 text-right">
                                    <button type="submit" disabled={!this.state.canSubmit} className="btn btn-primary submit">Sign in</button>
                                </div>
                            </div>
                        </Formsy.Form>
                    </div>
                    <button id='userbutton' data-toggle="modal" data-target="#chooseUser" className="hidden"></button>
                        <div className="modal fade" id="chooseUser" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 className="modal-title" id="myModalLabel">Access</h4>
                                    </div>
                                    <div className="modal-body">
                                        <div className="chooseAccess">
                                            <div className="alert alert-info" style={{"fontSize": 18}}>
                                                Sytems show you have access as admin/client the app.What would you like to do?</div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        {this.state.elements}
                                        <div className="listedAccount">
                                            {this.state.choosed === true || window.localStorage.getItem('choosed') === 'true'
                                                ? <ChooseAccount companies={this.props.data?this.props.data.companies:''}
                                                    clientAdmin={this.props.clientAdmin}
                                                    clientUser={this.props.clientUser}
                                                    closeModal={AdminLogin.closeModal.bind(this)}/>
                                                : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer className="text-muted text-center animation-pullUp">
                            <small>2017 &copy; <a href="" target="_blank">Aptitude</a></small>
                        </footer>
                         <Alert stack={{limit: 3}}/>
                </div>)
    }
}