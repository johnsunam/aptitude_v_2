import React, {Component} from 'react '

export default ClientCode = () => {
    return(
        <div className="mid_content">
            <div className="login_col">
                <div className="card">
                    <h1 className="title">Enter Code</h1>
                    <div className="input-container">
                        <input type="text" id="code" required="required" ref="clientCode"/>
                    </div>
                    <div className="button-container">
                        <button onClick={()=>{
                            let clientCode=this.refs.clientCode.value;
                        }}>
                            <span>LOGIN</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
