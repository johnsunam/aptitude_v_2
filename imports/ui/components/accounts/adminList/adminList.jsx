import React, {Component} from 'react'

export default class AdminList extends Component {
    constructor(props) {
        super(props);
        this.state={
            choosedUser:null,
            choosedName:null,
        }
    }
    render(){
        let adminlist=this.props.adminlist.length !== 0 ? this.props.adminlist.map((single)=>{
            return(<label><input type="radio" name="optradio" id={single._id} onClick={(e)=>{
                this.props.choosedLogin({choosedUser:e.target.id,choosedName:single.name,email:single.email,userType:'admin'})
            }}/>{single.name}</label>)
        }):<h2>this is no admin to choose</h2>;
        return(<div>
                <button type="button" className="btn btn-primary btn-lg" data-toggle="modal" data-target="#admin">
                    admin
                </button>
                <div className="modal fade" id="admin" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Choose admin for login</h4>
                            </div>
                            <div className="modal-body">
                                {adminlist}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
