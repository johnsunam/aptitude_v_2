import React,{Component} from 'react'

export default class UserList extends Component {
    constructor(props) {
        super(props);
        this.state={
            user:null,

        }
    }
    render(){
        let data=this.props.data?this.props.data:[];
        let content=data.length===0?<h1>NO any users</h1>:data.map((user)=>{
            let userType=  window.sessionStorage.getItem('userType');
            let name=userType==='client'?user.contactName:user.name;
            return(
                <li>
                    <input type="radio" name="user" id={name} onClick={(e)=>{
                    this.setState({user:e.target.id});
                    window.sessionStorage.setItem('user',user.user)
                    }}/>
                    {name}
                </li>
            )
        });

        return(
            <div>
                <div>
                    <ul>
                        {content}
                    </ul>
                    {data.length===0?<div>NO any users</div>:""}
                </div>
                {data.length===0 ? <div></div> :
                    <button className="btn btn-primary" data-dismiss="modal" onClick={this.props.recentUser}>
                    Login with {this.state.user}
                    </button>}
            </div>
        )
    }
}
