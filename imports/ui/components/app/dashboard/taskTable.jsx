import React,{Component} from 'react';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
import moment from 'moment';

export default class TaskTable extends Component {
    constructor(props){
        super(props);
        this.state={status:[]}
    }
   
    assigningStatus(props){
        _.each(props.tasks,function(single){
            $(`#${single._id}`).val(single.action.status)
        })
    }

    componentDidMount(){
         this.assigningStatus(this.props);
    }

     componentWillReceiveProps(nextProps) {
 this.assigningStatus(nextProps);
}
    render(){
        var self=this;
        return(<div className="panel">
                <div className="panel-body">
                    <h3 className="title-hero">
                        Assiged Task
                    </h3>
                    <div className="example-box-wrapper">
                        <div style={{overflowX:"auto"}}>
                            <div id="datatable-tabletools_wrapper" className="dataTables_wrapper form-inline">
                                <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found."> 
                                    {this.props.tasks.map((single)=>{
                                        var newdata=(_.omit(single.formdata,'images'));
                                            return(<Tr className="myTableRow" data={newdata}>
                                                    <Td column="Action" className="actionTd">
                                                    <div className="dropdown">
                                                    <ReactTooltip id={`tip${single._id}`} data-tip  type='info'>
                                                        <span>{single.action.description}</span>
                                                    </ReactTooltip>
                                                    <select className="form-control" data-tip data-for={`tip${single._id}`} id={single._id} name="status" onChange={(e)=>{
                                                            var action=single.action;
                                                            action.status=e.target.value;
                                                            action.status=="completed"?action.finished=new moment().format('YYYY-MM-DD HH-MM'):action.started=new moment().format('YYYY-MM-DD HH-MM');
                                                            Meteor.call('updateWorkflowAction',{id:single._id,action:action});
                                                        }}  ref={single._id}>
                                                        <option value="completed">completed</option>
                                                        <option value="in-progress">In-progress</option>
                                                        <option value="assigned">assigned</option>
                                                    </select>
                                                    </div>
                                                    </Td>
                                                </Tr>)
                                    })}
                                        </Table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>)
    }
}