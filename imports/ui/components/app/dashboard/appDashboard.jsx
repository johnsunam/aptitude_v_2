import React , {Component} from 'react'
import FillForm from '../../../container/fillForm.js';
export default class AppDashboard extends Component {
    constructor(props) {
        super(props);
        this.state={
            survey:null
        }
    }

    componentDidMount() {
        let self =  this;
        if(window.localStorage.getItem('login_survey') !== "true") {

            _.each(this.props.workflows,function(obj)
            {
                if(obj.type === 'login_survey')
                {
                    self.setState({survey:obj._id});
                    window.localStorage.setItem('login_survey',true);
                    $(".survey").trigger("click");
                    $(".modal-backdrop").css("z-index",-1);
                }
            })
        }
    }

    render(){
        let self = this;
        return(
            <div>
                <button type="button" className="btn btn-primary btn-lg hidden survey" data-toggle="modal" data-target="#login">
                    Launch Survey Dialog
                </button>
                <div className="modal fade" id="login" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="myModalLabel">Survey Form</h4>
                            </div>
                            <div className="modal-body">
                                {this.state.survey !== null ? <FillForm id={self.state.survey } type="survey" />:<span></span>}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
