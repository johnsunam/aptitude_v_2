import React,{Component} from 'react';
let Confirm = require('react-confirm-bootstrap');

export default class ShowForm extends Component{
    constructor(props){
        super(props);
        this.state={
            count:'',
            rules:[],
            dataRule:[],
            images:[]
        }
    }

    fillFormData(formData) {
        let labels = document.getElementsByTagName('label');
        _.each(labels,element=> {
           let key  = element.textContent.trim();
           $(`#${element.htmlFor}`).val(formData[key]);
        });
    }

    formEvents(props){
        var self=this;
        var pg = props.page.page ? props.page.page.detail.page: props.page.detail.page;
        self.setState({detail:props.page.page ? props.page.page.detail: props.page.detail})
        let rules=pg.form.rules;
        let dataRules=pg.form.dataRule;
        self.setState({count:0,rules:rules,dataRule:dataRules})
        let form=JSON.parse(pg.form.form);
        let element;
        if (props.page.page && props.page.page.detail.page.form.configurableField) {
            let forms = JSON.parse(form);
            _.each(props.fieldValue,(obj)=>{
                let field = _.findWhere(forms,{name:obj.field});
                field.values = obj.values;
            });
            form = forms;
            
            form = JSON.stringify(form);
        }
        $("#showform").formRender({
        dataType: 'json',
        formData: form
        });

        

		rules.map((rule)=>{
		    $(`#${rule.selectbox}`).parent().addClass('hidden');
            $(`#${rule.textbox}`).parent().addClass('hidden');
            rule.camera?$('.take-picture').addClass('hidden'):''

        })

    //logic implementation on form click
     $('#showform').click(function(e){
   
        let dataRule=_.findWhere(self.state.dataRule,{parent:e.target.name,parentValue:$(`#${e.target.name} option:selected`).text()})
        if(dataRule){
            let a="#"+dataRule.child
            $(a).find('option').remove().end()
            $.each(dataRule.childValue, function(index, value) {
                $(a).append($('<option>').text(value).attr('value', value));
            });
        }
    
        let element=[];
        let select=[];
        let radio=[];

        if(e.target.name!=undefined){
            element=_.where(self.state.rules,{checkbox:e.target.name});
            select=_.where(self.state.rules,{select:e.target.name});
            radio=_.where(self.state.rules,{radio:e.target.name})
        
        }

        if(radio.length>0){
            let opt=$(`input[name=${e.target.name}]:checked`).val();
            let rules=_.where(radio,{radio:e.target.name,radioOptions:opt})
            _.map(rules,function(obj){
                console.log(<object data="" type=""></object>)
                let box=document.getElementsByName(obj.textbox);
                let selectbox=document.getElementsByName(obj.selectbox);
                $(box).parent().removeClass('hidden');
                $(selectbox).parent().removeClass('hidden')
                obj.camera?$('.take-picture').removeClass('hidden'):'';
            })

            _.map(radio,function(obj){
                if(obj.radioOptions!=opt){
                let box=document.getElementsByName(obj.textbox);
                let selectbox=document.getElementsByName(obj.selectbox);
                    $(box).parent().addClass('hidden');
                    $(selectbox).parent().addClass('hidden')
                    obj.camera?$('.take-picture').addClass('hidden'):'';
                }
            })
        }
        if(select.length>0){
            let opt=$(`#${e.target.name}`).val()
            let rules=_.where(select,{select:e.target.name,option:opt})
            _.map(select,function(obj){
                if(obj.option!=opt){
                let box=document.getElementsByName(obj.textbox);
                let selectbox=document.getElementsByName(obj.selectbox);
                    $(box).parent().addClass('hidden');
                    $(selectbox).parent().addClass('hidden')
                    obj.camera?$('.take-picture').addClass('hidden'):'';
                    obj.camera?'':$('video').addClass('hidden')
                    obj.camera?'':$('#b').addClass('hidden');
                }
            });
            _.map(rules,function(obj){
                let box=document.getElementsByName(obj.textbox);
                let selectbox=document.getElementsByName(obj.selectbox);
                $(box).parent().removeClass('hidden');
                $(selectbox).parent().removeClass('hidden')
                obj.camera?$('.take-picture').removeClass('hidden'):'';
                obj.camera?'':$('video').addClass('hidden');
                obj.camera?'':$('#b').addClass('hidden');
            })
        }

        if(element.length>0){
            _.map(element,function(obj){
                let checkbox=document.getElementsByName(obj.checkbox);
                let box=document.getElementsByName(obj.textbox);
                let selectbox=document.getElementsByName(obj.selectbox);
                if($(checkbox).prop('checked') == true){
                    obj.camera?$('.take-picture').removeClass('hidden'):'';
                    $(box).parent().removeClass('hidden')
                    $(selectbox).parent().removeClass('hidden')
                }
                else{
                    obj.camera?$('.take-picture').addClass('hidden'):'';
                    $(box).parent().addClass('hidden')
                    $(selectbox).parent().addClass('hidden')
                    
                    
                }
            })
        }
    
    });
    props.formdata?this.fillFormData(props.formdata):'';

    $('.take-picture').click(function(){
        if(Meteor.isCordova) {
            navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
                destinationType: Camera.DestinationType.DATA_URL});
            
            function onSuccess(imageData) {
                var image = document.getElementById('myImage');
                img = "data:image/jpeg;base64," + imageData;;
                let images=self.state.images;
                images.push(img)
                self.setState({images:images});
            }
            
            function onFail(message) {
                alert('Failed because: ' + message);
            }
        } else {
            navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;

            var canvas = document.getElementById("c");
            var button = document.getElementById("b");
            if (navigator.getUserMedia) {
                navigator.getUserMedia({ video: { width: 1280, height: 720 } },
                function(stream) {
                    button.disabled = false;
                    button.className = "show btn btn-default";
                    var video = document.querySelector('video');
                    button.onclick = function() {
                        video.className="show"
                        canvas.getContext("2d").drawImage(video, 0, 0, 300, 300, 0, 0, 300, 300);
                        var img = canvas.toDataURL("image/png");
                        let images=self.state.images;
                        images.push(img)
                        self.setState({images:images});
                        alert("done");
                        $('.videos').addClass('hidden');
                    };
                    video.className="show"
                    video.src = window.URL.createObjectURL(stream);
                    video.onloadedmetadata = function(e) {
                        video.play();
                    };
                },
                function(err) {
                    console.log("The following error occurred: " + err.name);
                }
                );

            } else {
                console.log("getUserMedia not supported");
            }
        }
        
    

    });


    //handled form submittion
     if($(":submit").length==0){
        $("#showform").append('<button type="submit">submit<button/>');
      }
	//submit form data
	 $("#showform"). submit(function(e){
    
    e.preventDefault();
    let arr=$("#showform").serializeArray()
    var json=''
    var newdata={}
    var prv='';
   
    _.each(arr,function(single){
        if(prv!==single.name){
            newdata[single.name]=single.value;
        }
        else{
            let present=newdata[single.name];
        if(present.constructor===Array){
            present.push(single.value)
            newdata[single.name]=present;
        }
        else{
            newdata[single.name]=[present,single.value]
        }
        }
        prv=single.name;  
    })
   let field=Object.keys(newdata);
   let vals=Object.values(newdata)
   let datas=[];
   let c=0;
   let obj={}
   let record=_.map(vals,function(value){
     let fld=field[c];
     let label=$("label[for='"+fld+"']").text()
     label=label.replace(".","");
     obj[label.trim()]=value
    c++
   })

   let createdAt=new Date();
   obj.images=self.state.images
   newdata.images=self.state.images
   let savedata={user:window.localStorage.getItem("user"),formdata:obj,workflow:self.props.workflow._id,createdAt:createdAt,approveRoles:self.props.workflow.approve}
   let workflowdata={user:window.localStorage.getItem("user"),formdata:obj,workflow:self.props.workflow._id,createdAt:createdAt,approveRoles:self.props.workflow.approve,level:self.props.page.lvl+1}
   
   let email=self.state.detail.emailDetail;
   workflowdata.level=self.props.totallvl===workflowdata.level?false:workflowdata.level;
   workflowdata.status = 'open';

   Meteor.call('addFormData',savedata);
    console.log(self.props.page.lvl);
     if(self.props.page.lvl==0){
     self.setState({finalData:workflowdata})
      $('#submitform').trigger('click');
       
   }else{
    var d={formdata:workflowdata.formdata,id:self.props.incidentId,level:workflowdata.level}
    self.setState({finalData:d})
      $('#submitform').trigger('click');
   }
   self.state.image?data.image=self.state.image:''
    self.setState({formData:newdata})
    let keys=_.keys(newdata);
    let values=_.values(newdata);
     self.setState({keys:keys});
     
     self.refs.form.reset();
    
    })

}


workflowData(){
    let self=this;
        
        if(self.props.page.lvl==0 && !this.props.formdata){
            Meteor.call('addWorkflowData',self.state.finalData,function(err){
                if(!err){
                    
                    self.props.hideForm();
                    if(self.state.detail.emailDetail!==undefined){
                        self.state.detail.emailDetail.constructor===Array?_.each(self.state.detail.emailDetail,function(single){
                            self.sendMail(single);
                        }):self.sendMail(email);
                    }
                 //   console.log(window.localStorage.getItem('logout_survey'));
                    self.setState({showMessage:true,message:"Form submitted"});
                    $('.message').addClass('su')
                }
                else{

                    self.setState({showMessage:true,message:"Form Not submitted"});
                    $('.message').addClass('er')
                }
            })
        }else {
            console.log(self.state.finalData);
            Meteor.call('updateWorkflowData',self.state.finalData,function(err){
                if(!err){
                    if(self.state.detail.emailDetail!==undefined){
                        self.state.detail.emailDetail.constructor===Array?_.each(self.state.detail.emailDetail,function(single){
                            self.sendMail(single);
                        }):self.sendMail(email);
                    }
                    self.props.hideForm();
                    self.setState({showMessage:true,message:"Form submitted"});
                    $('.message').addClass('su')
                }
                else{

                    self.setState({showMessage:true,message:"Form Not submitted"});
                    $('.message').addClass('er')
                }
            })
        }
        if(window.localStorage.getItem("logout_survey") == "true") {
            window.localStorage.removeItem("login_survey");
            $(".logout_close").trigger("click");
            window.localStorage.removeItem("logout_survey");
            Meteor.logout();
        }
    }

    componentDidMount(){
        console.log(this.props);
        this.formEvents(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.formEvents(nextProps)
    }

    //sending mail
    sendMail(email){

        let mail={to:email.to,cc:email.cc,bcc:email.bcc,subject:email.subjec,body:email.body};

        if(email.conditionalField!==undefined){
            let sel=$(`label:contains(${email.conditionalField})`);
            let id=sel[0].htmlFor;
            let val=$(`#${id}`).val();
            if(val=='yes'){
                Meteor.call("sendEmail",mail,function(err){
                    if(!err){
                        console.log('mail sent')
                    }
                    else{
                        console.log(err)
                    }

                });
            }
        }else{
            Meteor.call("sendEmail",mail,function(err){
                if(!err){
                    console.log('mail sent')
                }
                else{
                    console.log(err)
                }

            });
        }


    }
    onConfirm(){
        console.log('helo')
    }

    render(){
        console.log(this.props.page);
        return(
            <div>
                <form id="showform" ref="form">
                </form>
                <Confirm onConfirm={this.workflowData.bind(this)}
                         body="Are you sure you want to submit?"
                         confirmText="Confirm"
                         title="Submit Form">
                    <button type="button" id="submitform" className="hidden">Delete Stuff</button>
                </Confirm>
                <video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
                <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
                <canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
                <div className="images ">
                    {this.state.images.length!=0?<div className="row col-md-12">
                        {this.state.images.map((image)=>{
                            return(<div className="col-xs-6 col-md-3">
                                <a href="#" className="thumbnail">
                                    <img src={image} style={{height:150}} alt="..."/>
                                </a>
                            </div>)  })}

                    </div>:<span></span>}
                </div>
            </div>
        )
    }
}
