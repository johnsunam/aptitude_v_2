import React,{Component} from 'react';
import ShowForm from '../dashboard/showForm';
export default class PublicPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            page:'',
            form:false
        }
    }

    formEvents(props){
        let self=this;
        let flowchart=props.workflow.flowchart;
        let level=props.workflow.level;
        self.setState({level:props.workflow.level})
        let steps=flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']
        self.setState({page:{page:steps[0], lvl:0}});
    }

    componentDidMount(){
        this.formEvents(this.props);
    }
    
    componentWillReceiveProps(nextProps) {
        this.formEvents(nextProps)
        this.setState({show:false})
    }
    
    hideForm() {
        this.setState({form:true});
    }

    render() {
        let content = this.state.form ?<div>Thanks for your answer</div>:<div className="col-md-8 col-md-offset-2"><h2>Answer the question below.</h2>{this.state.page != ''? <ShowForm page={this.state.page} workflow={this.props.workflow} hideForm={this.hideForm.bind(this)}/>:""}</div>;
        return content;
    }
}