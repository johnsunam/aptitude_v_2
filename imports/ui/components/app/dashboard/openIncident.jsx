import React,{Component} from 'react';
import {Table,Tr,Td,Th} from 'reactable';

export default  class OpenIncident extends  Component {
        constructor(props) {
                super(props);
                this.state = {
                        currentData:null
                }
        }

        render () {
                let self = this;
                return(<div><Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found.">
                    {this.props.datas.map((data, index)=> { 
                            return(<Tr className="myTableRow" data={data}>
                                <Td column="Show Detail">
                                        <a href="#" data-toggle="modal" onClick={e=>{
                                                let data = this.props.openIncidents[index]['formdata'];
                                                this.setState({currentData:data});
                                                $('.modal-backdrop').css('z-index','-1');
                                    }} data-target="#show_details">Show Details</a>
                                </Td>
                                <Td column="Form" className="formTd">
                                        <a href="#" onClick={e=> {
                                                this.props.openIncidentAction({page:this.props.page, id:this.props.openIncidents[index], formdata: this.props.openIncidents[index]['formdata']});
                                        }}>Fill form</a>
                                </Td>
                                <Td column="Form status" className="form_status">
                                        <button className="btn btn-success btn-sm" onClick={(e) => {
                                                Meteor.call('closeIncident',self.props.openIncidents[index]['_id']);
                                        }}>Close</button>
                                </Td>
        
                            </Tr>)
                    })}
                </Table>
                <div className="modal fade" id="show_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                        <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 className="modal-title" id="myModalLabel">Filled Data</h4>
                                        </div>
                                        <div className="modal-body">
                                                <div>
                                                        {_.map(this.state.currentData, (value,key) => {
                                                        if(key!=="images" && key!=='createdAt' && key!=='incidentId' && key!=='user' && key!=='roles'){
                                                        return(<div><div className=" question"><i className="fa fa-chevron-right"></i><label>{key}</label></div><div className="answer">&nbsp;&nbsp;{value}</div></div>)
                                                        }
                                                        })}
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>)
        }
}