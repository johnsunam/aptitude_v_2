import React,{Component} from 'react';
import Alert from 'react-s-alert';
import ShowForm from '../../../container/showform.js';
import IncidentList from '../../../container/incidentList.js'
import TaskTable from '../../../container/taskTable.js';
import RejectTable from '../../../container/reject.js';
import OpenedIncident from '../../../container/openIncident';

export default class FillForm extends Component {
  constructor(props) {
    super(props)
	this.state={rules:[],images:[],count:'',
    showMessage:'',message:'',detail:'',pages:[],incidentId:'',formdata:'',show:false,userRoles:[]}
  }
  
  formEvents(props){
    let self=this;
    let flowchart=props.data.workflow.flowchart;
    let level=props.data.workflow.level;
    self.setState({level:props.data.workflow.level})
    var steps=flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']
    var userRoles=self.props.data.user.roles;
    self.setState({userRoles:userRoles});
    var pages=[]

    _.map(steps,function(single,key){
        var common=_.intersection(single.detail.roles,userRoles)
        if(common.length>0)
           pages.push({lvl:key,page:single});
    }) 
    self.setState({pages:pages}); 
    var startingPage=_.findWhere(pages,{lvl:0});

  }


componentDidMount(){
    this.formEvents(this.props);
}

componentWillReceiveProps(nextProps) {
    this.formEvents(nextProps)
    this.setState({show:false})
}
openIncidentAction(data) {
    this.setState({show:true, page:data.page, incidentId:data.id._id, formdata:data.formdata});
}

chooseForm(data){
   this.setState({show:true,page:data.page,incidentId:data.id,formdata:data.formdata});
}

hideForm(){
  this.setState({show:false})
}
  render(){
      let norms = <div className="container">
        <div id="page-title"><h2>{this.props.data.workflow.name}</h2></div>
        <div className="tab">
            <ul className="nav nav-tabs" role="tablist">
                <li role="presentation" className="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">New Incidents</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Assigned Tasks</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Rejected Incidents</a></li>
            </ul>
        </div>
        <div className="tab-content">
            <div role="tabpanel" id="Registrationform" className="tab-pane tabcontent active" id="home">
                <div className="panel">
                    <div className="panel-body">
                        <div id="upper">
                            { this.state.pages.map((single)=>{
                            return single.lvl===0?<h4><a href='#' 
                            onClick={()=>{
                                
                                this.setState({show:true})
                                this.setState({page:single})
                                this.setState({formdata:''})
                            }}>Register new event</a></h4>:<div><IncidentList chooseForm={this.chooseForm.bind(this)} page={single} workflow={this.props.data.workflow._id}/></div>
                            })}
                            {this.props.data.open_incident ? <OpenedIncident openIncidentAction = {this.openIncidentAction.bind(this)} workflow={this.props.data.workflow} openIncidents={this.props.data.open_incident}/>:""}
                        </div>
                        <div className="example-box-wrapper">
                            {this.state.show?<div id="forms">
                            <ShowForm page = {this.state.page} workflow={this.props.data.workflow}
                            totallvl = { this.props.data.workflow.totallvl } 
                            incidentId = { this.state.incidentId }
                            formdata = { this.state.formdata }
                            hideForm={ this.hideForm.bind(this) }
                            user= { this.props.data.user.user }
                            />
                          </div>:<span></span>}
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" className="tab-pane tabcontent" id="profile"><TaskTable workflow={this.props.id}/></div>
            <div role="tabpanel" className="tab-pane tabcontent" id="messages"><RejectTable workflow = {this.props.id}/></div>
        </div>
    </div>;    
    let survey =    <div>
                        <div id="upper">
                            { this.state.pages.map((single)=>{
                            return single.lvl=== 0?<h4><a href='#' 
                            onClick={()=>{
                                this.setState({show:true})
                                this.setState({page:single})
                            }}>Register new event</a></h4>:<div><IncidentList chooseForm={this.chooseForm.bind(this)} page={single} workflow={this.props.data.workflow._id}/></div>
                            })}
                        </div>
                        <div className="example-box-wrapper">
                            {this.state.show?<div id="forms">
                            <ShowForm page = {this.state.page} workflow={this.props.data.workflow}
                            totallvl = { this.props.data.workflow.totallvl } 
                            incidentId = { this.state.incidentId }
                            formdata = { this.state.formdata }
                            hideForm={ this.hideForm.bind(this) }
                            user= { this.props.data.user.user }
                            />
                          </div>:<span></span>}
                        </div>
                    </div>;
  	return this.props.type == "survey" ? survey:norms;
   
  }
}