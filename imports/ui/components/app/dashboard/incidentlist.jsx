import React,{Component} from 'react';
import {Table,Tr,Td,Th} from 'reactable';


export default class IncidentList extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(<div className="panel">
            <div className="panel-body">
                <h3 className="title-hero">
                    Incident part
                </h3>
                <div className="example-box-wrapper">
                    <div style={{overflowX:"auto"}}>
                        <div id="datatable-tabletools_wrapper" className="dataTables_wrapper form-inline">
                        <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found.">
                            {this.props.datas.map((single)=>{
                                return(<Tr className="myTableRow" data={single.formdata}>
                                    <Td column="Action" className="actionTd">
                                        <a href="#" onClick={(rowData)=>{
                                            this.props.chooseForm({page:this.props.page,id:single._id,formdata:single.formdata})
                                        }}>fill form</a>
                                    </Td>
                                </Tr>)
                            })}
                        </Table>
                        </div>
                    </div>
                </div>
            </div>
            </div>)
    }
}