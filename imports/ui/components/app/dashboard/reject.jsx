import React,{Component} from 'react';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
import moment from 'moment';

export default class  RejectTable extends Component {
    constructor (props){
        super(props);
        this.state = {
            currentDescription:'',
            currentDataId:''
        }
    }

    render() {
        let self = this;
        return(<div className="panel">
                <div className="panel-body">
                    <h3 className="title-hero">
                        Rejected Tasks
                    </h3>
                    <div className="example-box-wrapper">
                        <div style={{overflowX:"auto"}}>
                            <div id="datatable-tabletools_wrapper" className="dataTables_wrapper form-inline">
                                <Table id="datatable-tabletools" className="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-tabletools_info" style={{"width": "100%"}} noDataText="No records found."> 
                                {this.props.rejectData.map((single)=>{
                                    var newdata=(_.omit(single.formdata,'images'));
                                            return(<Tr className="myTableRow" data={newdata}>
                                                <Td column="Action" className="actionTd">
                                                    <a href="#" data-toggle="modal" onClick={()=>{
                                                        $('.modal-backdrop').css('z-index','-1');
                                                        self.setState({
                                                            currentDescription:single.approveRoles[single.approvelvl]['description'],
                                                            currentDataId:single._id
                                                            });
                                                    }} data-target="#mymodal">Reject Description</a>
                                                </Td>
                                            </Tr>)
                                            })}
                                </Table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id="myModalLabel">Reject Description</h4>
                        </div>
                        <div className="modal-body">
                        <div className="well">{this.state.currentDescription}</div>
                        
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}