import React, { Component } from 'react';

export default class ClientAdminSideNavBar extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

    }
    render() {
        return (
            <div className="subnavbar">

                <div className="subnavbar-inner">

                    <div className="container">

                        <ul className="mainnav">

                            <li>
                                <a href="">
                                    <i className="icon-dashboard"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>



                            <li className="active">
                                <a href="">
                                    <i className="icon-plus-sign"></i>
                                    <span>Add Client User</span>
                                </a>
                            </li>

                            <li className="subnavbar-open-right">
                                <a href="">
                                    <i className="icon-user"></i>
                                    <span>Manage Client User</span>
                                </a>
                            </li>

                        </ul>

                    </div>
                </div>

            </div>
        )
    }
}