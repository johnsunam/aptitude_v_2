import React,{Component} from 'react'

export default class ClientAdminSidebar extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    render() {
        return(
            <div id="page-sidebar" style={{position: 'absolute'}}>
                <div className="scroll-sidebar">
                    <ul id="sidebar-menu" className="sf-js-enabled sf-arrows">
                        <li>
                            <a href="/client/dashboard" title="Admin Dashboard" className="sfActive">
                                <i className="fa fa-dashboard" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                                <span>Admin dashboard</span>
                            </a>
                        </li>
                        <li className="divider"></li>
                        <li className="no-menu">
                            <a href="/client/add-user" title="Frontend template" className="sfActive">
                                <i className="fa fa-plus" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                                <span>Add User</span>
                            </a>
                        </li>
                        <li className="no-menu">
                            <a href="/client/manage-user" title="Frontend template" className="sfActive">
                                <i className="fa fa-user" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                                <span>Manage User</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
