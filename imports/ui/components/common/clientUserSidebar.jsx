import React,{Component} from 'react';
import { layout } from '../../../../client/lib/assets/themes/admin/layout.js';
export default  class ClientUserSidebar extends Component{
    constructor(props){
        super(props);
    }
    
    componentDidMount() {
        layout();
    }

    render() {
        return(<div id="page-sidebar">
                    <div className="scroll-sidebar">
                        <ul id="sidebar-menu">
                            {this.props.workflows.map((workflow) => {
                                if(workflow.type == 'normal'  || workflow.type == 'incident_tracking' || workflow.type == 'track_incident' )
                                        return(<li>
                                                    <a href="" className="sf-with-ul" id={workflow._id}  onClick={(e)=>{
                                                        // var video = document.querySelector('video');
                                                        // console.log(video);
                                                        // video.class="hidden";
                                                        $('#b').removeClass("show")
                                                        $('#b').addClass("hidden")
                                                    }}><i className="glyphicon glyphicon-menu-hamburger"></i><span className="">{workflow.name}</span>
                                                    </a>
                                                        <div className="sidebar-submenu">
                                                            <ul>
                                                                <li>
                                                                    <a href="#" title="" id={workflow._id} 
                                                                        onClick={(e)=>{
                                                                            console.log(e.target.id);
                                                                        FlowRouter.go(`/fillform/${e.target.id}`)
                                                                    }}><span id={workflow._id}>Create</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                </li>)
                                            })}
                                            <li>
                                                <a href="#" title="" className="sf-with-ul">
                                                    <span>Configure Workflow</span>
                                                </a>
                                            </li>
                                            {this.props.assignerWorkflow ?this.props.assignerWorkflow.map((workflow)=>{
                                                let url = '/app/configure/'+workflow._id;
                                                return(<li>
                                                            <a href={url} className="sf-with-ul"  id={workflow._id}>
                                                                <i class="glyphicon glyphicon-menu-hamburger"></i>
                                                                <span>{workflow.name}</span>
                                                            </a>
                                                        </li>)
                                            }):""}
                        </ul>
                    </div>
                </div>)
    }
    
}
