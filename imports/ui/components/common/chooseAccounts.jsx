import React,{Component} from 'react'
import AdminList from '../accounts/adminList/adminList.jsx';
import ClientList from '../accounts/clientList/clientList.jsx'
import UserType from '../../container/UserType.js';

export default class chooseAccounts extends Component {
  constructor(props) {
    super(props);
    this.state={currentList:[],
    currentUser:null,
    choosedUser:null,
    user:null,
    components:''
  }

  }
  componentDidMount(){
    //list of companies
    let component;
    if(window.localStorage.getItem('appType')=='aptitude'){
       component=<ul style={{listStyleType: "none"}}>{this.props.companies.map((single)=>{
        return(<li><input type="radio" name="company" onClick={(e)=>{
              window.localStorage.setItem('user',single.user);
              this.props.closeModal()
              FlowRouter.go('/client/dashboard');
        }}/>{single.companyName}</li>)

      })}</ul>
    }
    else{
      component=<ul style={{listStyleType: "none"}}>
      {this.props.data.companies.map((single)=>{
          return(<li><input type="radio" name="company" onClick={(e)=>{
            window.localStorage.setItem('company',single.user);
            if(window.localStorage.getItem('subType')=="client"){
              this.props.closeModal()
              FlowRouter.go('/client/dashboard');
            }
            else{
              this.props.closeModal()
              FlowRouter.go('/app/dashboard');
            }
          }}/></li>)
      })}
      </ul>
    }
    this.setState({component:component});
  }
  render(){
      return(<div>
      <div className="choosedList">
        {this.state.component}
      </div>
     </div>)
  }
}
