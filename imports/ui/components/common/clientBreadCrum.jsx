import React,{Component} from 'react';

export default ClientBreadCrum = (props) => {
    return (<div className="content-header">
                <div className="row">
                    <div className="col-sm-6">
                        <div className="header-section">
                            <h1>{props.type.toUpperCase()}</h1>
                        </div>
                    </div>
                    <div className="col-sm-6 hidden-xs">
                        <div className="header-section">
                            <ul className="breadcrumb breadcrumb-top">
                                <li><a href="/client/Dashboard">Dashboard</a></li>
                                <li><a href="#">{props.type}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>)
}