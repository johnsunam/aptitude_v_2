import React,{Component} from 'react';


export default class SiderBar extends Component{
 constructor(props) {
   super(props);
   
 }
  componentDidMount() {
  }
  render() {
    return(
    <div id="page-sidebar" style={{position: 'absolute'}}>
        <div className="scroll-sidebar">
            <ul id="sidebar-menu" className="sf-js-enabled sf-arrows">
                <li>
                    <a className="sfActive" href="/aptitude/manage-form">
                        <i className="fa fa-plus-square" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                        <span>Form</span>
                    </a>
                </li>
                
                <li>
                    <a className="sfActive" href="/aptitude/manage-client">
                        <i className="fa fa-user" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                        <span> Client </span>
                    </a>
                </li>
                <li>
                    <a className="sfActive" href="/aptitude/manage-page">
                        <i className="fa fa-file-text-o" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                        <span> Page </span>
                    </a>
                </li>
                <li>
                    <a className="sfActive" href="/aptitude/manage-workflow">
                        <i className="fa fa-briefcase" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                        <span> Workflow </span>
                    </a>
                </li>
                <li>
                    <a className="sfActive" href="/aptitude/manage-user">
                        <i className="fa fa-users" style={{paddingLeft: 10, paddingRight: 10, fontSize: 20}}></i>
                        <span> User </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    )
  }

  
}
