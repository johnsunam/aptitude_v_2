//header for layout
import React,{Component} from 'react'
import  './common.css';
export default class Header extends Component{
    constructor(props) {
        super(props);

    }
    componentDidMount() {
    }

    render() {
        return(
			<div id="page-header" className="bg-gradient-9">
            <div id="mobile-navigation">
                <button id="nav-toggle" className="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                <a href="index.html" className="logo-content-small" title="MonarchUI"></a>
            </div>
                <div id="header-logo" className="logo-bg">
                   
                    <a href="/aptitude/add-form" className="title-header"  title="aptitude">
                       Aptitude 
                    
                    </a>
                    <a id="close-sidebar" href="#" title="Close sidebar">
                        <i className="glyph-icon icon-angle-left"></i>
                    </a>
                </div>
				
				<div id="header-nav-right">
                    <div className="dropdown" id="dashnav-btn">
                    <a href="#" data-toggle="dropdown" data-placement="bottom" onClick={()=>{$('.dropdown-menu').toggle()}} className="popover-button-header tooltip-button" title="" data-original-title="Dashboard Quick Menu" aria-expanded="false">
                        <i className="fa fa-cog logout" ></i>
                    </a>
                    <div className="dropdown-menu dropdown-menu-left" style={{display: "none"}}>
                        <div className="box-sm" style={{width:124}}>
                            <div className="pad5A button-pane button-pane-alt text-center">
                                <a href="#" className="btn display-block font-normal btn-danger" onClick={()=>{
                        window.localStorage.setItem('choosed',null);
                        window.localStorage.setItem('users',null);
                        window.localStorage.setItem('user',null);
                        window.localStorage.setItem('type',null);
                        window.localStorage.setItem('userAccess',null);
                        window.sessionStorage.setItem('user',null);
                        Meteor.logout()}}>
                                    <i className="fa fa-cog"></i>
                                    Logout
                                </a>
                            </div>
                            
                            </div>
                        </div>
                    </div>
					
				</div>
			</div>
        )
    }
}
