    import React,{Component} from 'react'
import FillForm from '../../container/fillForm.js';
export default class ClientUserHeader extends Component{

 constructor(props) {
    super(props)
    this.state={
      survey:null
    }
  }

  iconToggle(present, upcome) {
      console.log(present, upcome);
    $(`#${present}-sidebar`).hide();
    $(`#${upcome}-sidebar`).show()
  }
 
componentDidMount() {
    let self =  this;
    $("#right-sidebar").hide();
}
  render(){
    return(<div id="page-header" className="bg-gradient-9">
                        <div id="mobile-navigation">
                            <button id="nav-toggle" className="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        </div>
                        <div id="header-logo" className="logo-bg">
                            <a href="index.html" className="logo-content-big" title="MonarchUI">
                                Aptitude <i>UI</i>
                                <span>The perfect solution for user interfaces</span>
                            </a>
                            <a href="index-2.html" className="logo-content-small" title="MonarchUI">
                                Monarch <i>UI</i>
                                <span>The perfect solution for user interfaces</span>
                            </a>
                            <a id="close-sidebar" className="left-sidebar" href="#" onClick={ this.iconToggle("left", "right") } title="Close sidebar">
                                <i className="glyph-icon icon-angle-left" ></i>
                            </a>
                           
                        </div> 
                        <div id="header-nav-right">
                            <div className="dropdown" id="header-nav-right progress-btn">
                                <a data-toggle="dropdown" href="#" title="">
                                    <span className="small-badge bg-azure"></span>
                                    <i className="fa fa-sliders"></i>
                                </a>
                                <div className="dropdown-menu pad0A box-sm float-right" id="progress-dropdown">
                                    <div className="slimScrollDiv" style={{position: "relative", overflow: "hidden", width: "auto", height: 45}}>
                                        <div className="scrollable-content scrollable-slim-box" style={{overflow: "hidden", width: "auto", height: 45}}>
                                            <ul className="no-border progress-box progress-box-links">
                                                <li>
                                                    <a href="#"  onClick={()=>{
                                                        window.localStorage.removeItem('choosed');
                                                        window.localStorage.removeItem('users');
                                                        window.localStorage.removeItem('user');
                                                        window.localStorage.removeItem('type');
                                                        window.localStorage.removeItem('userAccess');
                                                        window.localStorage.removeItem('user');
                                                        window.localStorage.removeItem("survey");
                                                        window.localStorage.removeItem("logout");
                                                        let self = this;
                                                        let workflow = _.findWhere(this.props.workflows,{type:'logout_survey'});
                                                        console.log(workflow)
                                                        if(workflow){
                                                            let page = workflow.flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
                                                            let commonRoles = _.intersection(page[0].detail.roles, self.props.client.roles);
                                                                if(commonRoles.length > 0)
                                                                {
                                                                    self.setState({survey:workflow._id});
                                                                    window.localStorage.setItem('logout_survey',true);
                                                                    $(".logout_survey").trigger("click");
                                                                    $(".modal-backdrop").css("z-index",-1);
                                                                }
                                                                else{
                                                                    window.localStorage.removeItem('login_survey');
                                                                    Meteor.logout();
                                                                }
                                                        } else {
                                                            window.localStorage.removeItem('login_survey');
                                                            Meteor.logout();
                                                        }
                                                       
                                                        
                                                        }} title="">Logout </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div className="slimScrollBar" style={{background: "#8da0aa", width: 6, position: "absolute", top: 0, opacity: 0.4, display: "block", borderRadius: 7, zIndex: 99, right: 1}}></div>
                                    <div className="slimScrollRail" style={{width: 6, height:"100%", position: "absolute", top: 0, display: "none", borderRadius: 7, background: "#333333", opacity: 0.2, zIndex: 90, right: 1}}></div>
                                </div>
                            </div>
                        </div>  
                       <button type="button" className="btn btn-primary btn-lg hidden logout_survey" data-toggle="modal" data-target="#logout">
                                        Launch demo modal
                                    </button>
                                    <div className="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 className="modal-title" id="myModalLabel">Survey Form</h4>
                                                </div>
                                                <div className="modal-body">
                                                    {this.state.survey != null ? <FillForm id={this.state.survey} type="survey" />:<span></span>}
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-default logout_close" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                 
                    </div>)

                }
}
