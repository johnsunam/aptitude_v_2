import React,{Component} from 'react';
export default InnerHeader=(props)=> {
    console.log(props);
    return(<div id="wrapper">
                <div className="row wrapper border-bottom white-bg page-heading">
                    <div className="col-lg-10">
                        <h2>{props.workflow.name}</h2>
                        <ol className="breadcrumb">
                            <li>
                                <a href="index-2.html">Home</a>
                            </li>
                            <li>
                                <a>Tables</a>
                            </li>
                            <li className="active">
                                <strong>Data Tables</strong>
                            </li>
                        </ol>
                    </div>
                    <div className="col-lg-2">

                    </div>
                </div>
            </div>)
}
