//header for layout

import React,{Component} from 'react'

export default class Header extends Component{
    constructor(props) {
      super(props);
      
    }
    componentDidMount() {
        $('#menu_toggle').click(function () {
            if ($('body').hasClass('nav-md')) {
                $('body').removeClass('nav-md').addClass('nav-sm');
                $('.left_col').removeClass('scroll-view').removeAttr('style');
                $('.sidebar-footer').hide();

                if ($('#sidebar-menu li').hasClass('active')) {
                    $('#sidebar-menu li.active').addClass('active-sm').removeClass('active');
                }
            } else {
                $('body').removeClass('nav-sm').addClass('nav-md');
                $('.sidebar-footer').show();

                if ($('#sidebar-menu li').hasClass('active-sm')) {
                    $('#sidebar-menu li.active-sm').addClass('active').removeClass('active-sm');
                }
            }
        });
        $('.dropdown-toggle').dropdown()
    }

  render() {
    return(
      <nav className="navbar navbar-transparent navbar-absolute">
				<div className="container-fluid">
					<div className="navbar-header">
						<button type="button" className="navbar-toggle" data-toggle="collapse">
							<span className="sr-only">Toggle navigation</span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
						</button>
						<a className="navbar-brand" href="#">Aptitude</a>
					</div>
					<div className="collapse navbar-collapse">
						<ul className="nav navbar-nav navbar-right">
							<li>
								<a href="#pablo" className="dropdown-toggle" data-toggle="dropdown">
	 							   <i className="material-icons">person</i>
	 							   <p className="hidden-lg hidden-md">Profile</p>
		 						</a>
								<ul className="dropdown-menu">
								<li><a href="#"> Profile </a></li>
								<li><a href="#" onClick={()=>{
									window.localStorage.setItem('choosed',null);
									window.localStorage.setItem('users',null);
									window.localStorage.setItem('user',null);
									window.localStorage.setItem('type',null);
									window.sessionStorage.setItem('user',null);
									window.localStorage.setItem('userAccess',null);
									Meteor.logout()
								}}> Logout </a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>)
    }
  }