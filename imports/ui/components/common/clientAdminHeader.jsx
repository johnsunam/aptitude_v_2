import React,{ Component } from 'react'

export default class ClientAdminHeader extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

    }

    render() {
        return(
            <header className="navbar navbar-inverse navbar-fixed-top navbar-glass">
                <ul className="nav navbar-nav-custom">
                    <li className="animation-fadeInQuick">
                        <a href="/client/dashboard" className="sidebar-title">
                            <i className="fa fa-map-marker"></i> <span className="sidebar-nav-mini-hide">Aptitude<strong>Aptitude</strong></span>
                        </a>
                    </li><li>&nbsp;&nbsp;</li>
                    <li className="animation-fadeInQuick">
                        <a href="/client/dashboard"><strong>Home</strong></a>
                    </li><li>&nbsp;&nbsp;</li>
                    <li className="animation-fadeInQuick">
                        <a href="/client/workflows"><strong>Workflow</strong></a>
                    </li><li>&nbsp;&nbsp;</li>
                    <li className="animation-fadeInQuick">
                        <a href="/client/add-user"><strong>Add User</strong></a>
                    </li>
                    <li className="animation-fadeInQuick">
                        <a href="/client/manage-user"><strong>Manage User</strong></a>
                    </li>
                </ul>
                <ul className="nav navbar-nav-custom pull-right">
                    <li className="dropdown">
                        <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/avatar10.jpg" alt="avatar"/>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#" onClick={()=>{
                                    window.localStorage.setItem('choosed',null);
                                    window.localStorage.setItem('users',null);
                                    window.localStorage.setItem('user',null);
                                    window.localStorage.setItem('type',null);
                                    window.localStorage.setItem('userAccess',null);
                                    window.sessionStorage.setItem('user',null);
                                    Meteor.logout()}}>
                                    <i className="fa fa-power-off fa-fw pull-right"></i>
                                    Log out
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </header>
        )
    }

}
