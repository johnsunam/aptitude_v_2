import React,{Component} from 'react'

export default class ClientAdminSidebar extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    render() {
        return(
            <div className="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <h4 className="sub-header">Page navigation</h4>
                <div id="easy-tree1">
                    <ul>
                        <li className="isFolder isExpanded">
                            Workflow
                            <ul>
                                <li><a href="">Maintenance</a></li>
                                <li><a href="">Daily Cleaning</a></li>
                            </ul>
                        </li>
                        <li className="isFolder isExpanded">
                            User
                            <ul>
                                <li className="isFolder"><a href="">Manager</a>
                                    <ul>
                                        <li><a href="">User1</a></li>
                                        <li><a href="">User2</a></li>
                                    </ul>
                                </li>

                                <li className="isFolder"><a href="">Cleaner</a>
                                    <ul>
                                        <li><a href="">User1</a></li>
                                        <li><a href="">User2</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
