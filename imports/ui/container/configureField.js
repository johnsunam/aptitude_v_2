import {composeWithTracker} from 'react-komposer';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ConfigurableValueDb} from '../../api/configurableValues/collection/configurableValues.collection';
import ConfigureField from '../components/aptitude/page/configureField';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';

const composer = (props, onData) => {

    let subcription = Meteor.subscribe('getConfigurableValue');
    let workflowSub = Meteor.subscribe('getWorkFlow');

    if (workflowSub.ready()) {
        let workflow = WorkflowDb.findOne({_id: props.id});

        let configurableValue = ConfigurableValueDb
            .find({worflow: props.id})
            .fetch();
        let clientUser = ClientUserDb
            .find({
            roles: {
                $in: workflow.roles
            }
        })
            .fetch();

        onData(null, {
            workflow: workflow,
            configurableValue: configurableValue,
            clientUser: clientUser
        });
    }
}

export default composeWithTracker(composer)(ConfigureField);