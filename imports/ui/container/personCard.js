import {composeWithTracker} from 'react-komposer';
import PersonCard from '../components/client/homePages/personCard.jsx';
import {ClientDb} from '../../api/clients/collection/client.collection';

const composer = (props, onData) => {
    const subscription = Meteor.subscribe('getClient');
    if (subscription) {
        const client = ClientDb.findOne({
            user: Meteor.userId()
        });
        console.log(client);
        onData(null, {client})
    }

};

export default composeWithTracker(composer)(PersonCard);
