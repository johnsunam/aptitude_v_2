import {composeWithTracker} from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import ClientModal from '../components/aptitude/workflow/clientModal.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
const composer = (props, onData) => {
    console.log(props.id);
    let subcription = Meteor.subscribe('getClient');
    let flow = Meteor.subscribe('getWorkFlow');
    if (flow.ready()) {
        const clients = ClientDb
            .find()
            .fetch();
        const workflow = WorkflowDb.findOne({_id: props.id});

        onData(null, {
            clients: clients,
            workflow: workflow
        })
    }

};

export default composeWithTracker(composer)(ClientModal);