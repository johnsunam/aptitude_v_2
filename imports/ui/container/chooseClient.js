import {composeWithTracker} from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js';
import ChooseClient from '../components/aptitude/workflow/chooseClient.jsx';
const composer = (props, onData) => {
    var subcription = Meteor.subscribe('getClient');
    if (subcription.ready()) {

        var clients = ClientDb
            .find()
            .fetch();
        onData(null, {clients})
    }

};

export default composeWithTracker(composer)(ChooseClient);
