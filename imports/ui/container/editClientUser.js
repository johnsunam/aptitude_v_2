import { composeWithTracker } from 'react-komposer';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import AddClientUser from '../../ui/components/client/clientUser/addClientUser.jsx'
const composer = ( props, onData ) => {
    let subscription=Meteor.subscribe('getClient');
    if(subscription.ready()){
        let user=window.localStorage.getItem('company');
        console.log(user);
        let data=ClientDb.findOne({user:user});
        const clientUser = ClientUserDb.findOne({_id: props.id});

        data=data?data:[];
        const edit = !!props.id;
        onData(null, {
            clientUser: clientUser,
            edit: edit,
            data: data
        })
    }

};


export default composeWithTracker(composer)(AddClientUser);
