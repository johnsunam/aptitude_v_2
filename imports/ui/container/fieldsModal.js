import {composeWithTracker} from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import FieldsModal from '../components/aptitude/page/fieldsModal';

const composer = (props, onData) => {
    //const subscribe = Meteor.call('get') if()
    let form = JSON.parse(JSON.parse(props.page.form.form));
    let formDetail = props.page.form;
    onData(null, {
        form: form,
        formDetail: formDetail
    })
};

export default composeWithTracker(composer)(FieldsModal);