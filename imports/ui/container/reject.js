import {composeWithTracker} from 'react-komposer';
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import RejectTable from '../components/app/dashboard/reject.jsx';

const composer = (props, onData) => {
    let subcription = Meteor.subscribe('getClientUser');
    let workflowDatas = Meteor.subscribe("getWorkflowDatas");

    if (workflowDatas.ready()) {
        let rejectedIncident = [];
        let rejectDescription = '';
        let clientUser = ClientUserDb.findOne({
            user: window
                .localStorage
                .getItem('user')
        });

        if (clientUser) {
            var datas = WorkflowDataDb.find({
                $and: [
                    {
                        workflow: props.workflow
                    }, {
                        approveRoles: {
                            $not: {
                                $eq: null
                            }
                        }
                    }, {
                        user: window
                            .localStorage
                            .getItem('user')
                    }
                ]
            }).fetch();
            _.each(datas, function (obj) {
                let client = obj.approveRoles;
                let clientApprove;
                _.each(client, function (single, key) {
                    if (single.status == "reject") {
                        console.log(key);
                        obj.approvelvl = key;
                        rejectedIncident.push(obj);
                    }
                });
            });
        }
        onData(null, {
            rejectData: rejectedIncident,
            client: clientUser
        });
    };

}
export default composeWithTracker(composer)(RejectTable);
