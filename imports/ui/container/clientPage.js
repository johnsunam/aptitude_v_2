import { composeWithTracker } from 'react-komposer';
import {PageDb} from '../../api/page/collection/page.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import clientPages from '../components/aptitude/page/clientPages.jsx'
const composer = ( props, onData ) => {
    var subcription=Meteor.subscribe('getPage');

    if(subcription.ready()){
        console.log(props.client)
        var pages=PageDb.find({clientName:props.client}).fetch();
       console.log(pages)
        onData( null, {pages} )
      }

  };


export default composeWithTracker(composer)(clientPages);
