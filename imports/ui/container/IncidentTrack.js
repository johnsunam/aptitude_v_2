import { composeWithTracker } from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import IncidentTrack from '../components/aptitude/workflow/IncidentTrack';
const composer = ( props, onData ) => {
    let subscription=Meteor.subscribe("getWorkFlow");
    if(subscription.ready()){
      var workflow =WorkflowDb.findOne({_id:props.id});
      
       onData(null,{workflow})
      }

  };


export default composeWithTracker(composer)(IncidentTrack);