import {composeWithTracker} from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import AddClient from '../components/aptitude/client/addClient.jsx'
const composer = (props, onData) => {
    const subscription = Meteor.subscribe('getClient');
    if (subscription.ready()) {
        const client = ClientDb.findOne({_id: props.id});
        const edit = !!props.id;
        onData(null, {
            client: client,
            edit: edit
        })
    }

};

export default composeWithTracker(composer)(AddClient);
