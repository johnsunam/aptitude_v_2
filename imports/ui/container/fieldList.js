import {composeWithTracker} from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import FieldList from '../components/aptitude/page/fieldList';

const composer = (props, onData) => {
    console.log(props);
    let data = JSON.parse(JSON.parse(props.page.form.form));
    console.log(data);
    let label = _.pluck(data, 'label');
    let name = _.pluck(data, 'name');
    let form = onData(null, {
        label: label,
        name: name,
        pageId: props.page._id
    });
};

export default composeWithTracker(composer)(FieldList);