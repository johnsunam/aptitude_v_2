import {composeWithTracker} from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import Header from '../components/common/clientUserHeader.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection'
const composer = (props, onData) => {
  const clientuser = Meteor.subscribe('getClientUser');
  const client = Meteor.subscribe('getClient');
  const workflow = Meteor.subscribe('getWorkFlow');
  if (workflow.ready()) {
    let data;
    if (window.localStorage.getItem('appType') == "client-admin") {

      let workflows = WorkflowDb
        .find({
        client: {
          $in: [
            window
              .localStorage
              .getItem('company')
          ]
        }
      })
        .fetch();
      let client = ClientDb.findOne({
        user: Meteor.userId()
      });
      onData(null, {
        client: client,
        workflows: workflows
      });
    } else {
      let client = ClientUserDb.findOne({
        user: Meteor.userId()
      });
      console.log(client);
      let workflows = WorkflowDb.find({
        $or: [
          {
            client: {
              $in: [
                window
                  .localStorage
                  .getItem('company')
              ]
            }
          }, {
            roles: {
              $in: client.roles
            }
          }
        ]
      }).fetch();

      onData(null, {
        client: client,
        workflows: workflows
      })
    }

  }

};

export default composeWithTracker(composer)(Header);
