import { composeWithTracker } from 'react-komposer';
import {SampleData} from '../../api/workflow/collection/sampleData.collection.js'
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import DefineWorkFlow from '../components/aptitude/workflow/defineWorkFlow.jsx'
import {PageDb} from '../../api/page/collection/page.collection.js';
import {TaskDb} from '../../api/task/collection/task.collection.js';
import {FormDb} from '../../api/form/collection/form.collection.js';
const composer = ( props, onData ) => {
    
    var subscription=Meteor.subscribe('getWorkFlow');
    var pages=Meteor.subscribe('getPage');
    var forms=Meteor.subscribe('getForm');
    var tasks=Meteor.subscribe('getTask');
    if(tasks.ready()){
       let user=window.localStorage.getItem('user');
       const workflow=WorkflowDb.findOne({_id:props.id});
       const pages= PageDb.find({user:user}).fetch();
       const tasks=TaskDb.find({user:user}).fetch();
       const data={workflow:workflow,pages:pages,tasks:tasks,forms:forms};
       onData(null,{data});
      }

  };


export default composeWithTracker(composer)(DefineWorkFlow);
