import {composeWithTracker} from 'react-komposer';
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import TaskTable from '../components/app/dashboard/taskTable.jsx'
const composer = (props, onData) => {
  let subcription = Meteor.subscribe('getClientUser');
  let workflowDatas = Meteor.subscribe("getWorkflowDatas");
  if (workflowDatas.ready()) {
    var clientUser = ClientUserDb.findOne({
      user: window
        .localStorage
        .getItem('user')
    });

    if (clientUser) {
      var tasks = WorkflowDataDb.find({
        $and: [
          {
            workflow: props.workflow
          }, {
            $or: [
              {
                "action.user": window
                  .localStorage
                  .getItem('user')
              }, {
                "action.user": {
                  $in: clientUser.roles
                },
              }

            ]
          },{
            $or:[
              {
                "action.status":"in-progress"
              },{
                "action.status":"assigned"
              }
            ] 
          }
        ]
      }).fetch();
    } else {
      tasks = []
    }
    console.log(tasks)
    onData(null, {tasks})
  };

}
export default composeWithTracker(composer)(TaskTable);
