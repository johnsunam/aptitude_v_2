import {composeWithTracker} from 'react-komposer';
import {PageDb} from '../../api/page/collection/page.collection'
import EditForm from '../components/aptitude/form/addForm.jsx'

const composer = (props, onData) => {
    var subcription = Meteor.subscribe('getPage');
    if (subcription.ready()) {
        var page = PageDb.findOne({_id: props.id});
        onData(null, {
            form: page.form,
            edit: true,
            pageform: true
        })
    }

};

export default composeWithTracker(composer)(EditForm);
