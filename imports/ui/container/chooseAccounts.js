import {composeWithTracker} from 'react-komposer';
import {UserAccountDb} from '../../api/account/collection/userAccount.js';
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import ChooseAccount from '../../ui/components/common/chooseAccounts.jsx'

const composer = (props, onData) => {

  const subscription = Meteor.subscribe('getAptitudeAdmin');
  const client = Meteor.subscribe('getClient');
  const subscript = Meteor.subscribe('getClientUser');

  if (subscript.ready()) {

    let data;
    // aptitude loggedIn to the to comapanies
    if (window.localStorage.getItem('appType') == "aptitude" && Roles.userIsInRole(Meteor.userId(), ['aptitude-admin'])) {
      if (props.companies.length == 1) {
        let company = props.companies[0];

         window.localStorage.setItem('company',company.user);
         window.localStorage.setItem('user',company.user);
         props.closeModal();

        FlowRouter.go('/client/dashboard');
      } else {
        onData(null, {null});
      }
    } else if (Roles.userIsInRole(Meteor.userId(), ['client-admin'])) {

      window
        .localStorage
        .setItem('user', props.clientAdmin.user)
      console.log(props.clientAdmin.user);
      let companies = ClientDb
        .find({
        user: {
          $in: props.clientAdmin.companies
        }
      })
        .fetch();
      if (companies.length != 1) {
        data = {
          companies: companies
        }
        onData(null, {data})
      } else {
        window
          .localStorage
          .setItem('company', companies[0].user);
        if (window.localStorage.getItem('appType') == "client-admin") {
          if (window.localStorage.getItem('subType') == "client") {
            props.closeModal();
            FlowRouter.go('/client/dashboard')
          } else {
            props.closeModal();
            FlowRouter.go('/app/dashboard')
          }
        }

      }
    } else {
      let companies = ClientDb
        .find({
        user: {
          $in: props.clientUser.companies
        }
      })
        .fetch();
      window
        .localStorage
        .setItem('user', props.clientUser.user);
      if (companies.length != 1) {
        data = {
          comapanies: companies
        };
        onData(null, {data});
      } else {
        window
          .localStorage
          .setItem('company', companies[0].user);
        if (window.localStorage.getItem('subType') == "client") {
          props.closeModal();
          FlowRouter.go('/client/dashboard')
        } else {
          props.closeModal();
          FlowRouter.go('/app/dashboard');
        }
      }
    }

  }
}

export default composeWithTracker(composer)(ChooseAccount);
