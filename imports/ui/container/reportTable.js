import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js';
import ReportTable from '../components/client/reports/reportTable';
const composer = (props, onData)=> {
    console.log(props);
    let rows = _.map(props.datas, (data, index)=> {
            let roles = props.incidentUser[index]['roles'];
        return {User:props.incidentUser[index]['name'], Roles:roles.toString(), Created_At: moment(data.createdAt).format("dddd, MMMM Do YYYY, h:mm a")}
    });

    onData(null,{rows});

}

export default composeWithTracker(composer)(ReportTable);
