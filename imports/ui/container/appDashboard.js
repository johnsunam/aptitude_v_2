import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import AppDashboard from '../components/app/dashboard/appDashboard.jsx'

const composer = (props, onData) => {
  var subcription = Meteor.subscribe('getPage');
  let client = Meteor.subscribe('getClientUser');
  let workflow = Meteor.subscribe('getWorkFlow')

  if (subcription.ready()) {

    let clientUser = ClientUserDb.findOne({
      user: window
        .localStorage
        .getItem('user')
    })

    if (window.localStorage.getItem('appType') == "client-admin") {
      let workflows = WorkflowDb
        .find({
        client: {
          $in: [
            window
              .localStorage
              .getItem('company')
          ]
        }
      })
        .fetch();
      onData(null, {workflows})
    } else {

      let workflows = WorkflowDb.find({
        $or: [
          {
            client: {
              $in: [
                window
                  .localStorage
                  .getItem('company')
              ]
            }
          }, {
            roles: {
              $in: clientUser.roles
            }
          }
        ]
      }).fetch();
      onData(null, {workflows})

    }
  }

};

export default composeWithTracker(composer)(AppDashboard);
