import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import PublicPage from '../components/app/dashboard/publicPage.jsx';

const composer = (props, onData) => {
    const subcription = Meteor.subscribe('getPublicWorkflow');

    if (subcription.ready()) {
        const workflow = WorkflowDb.findOne({_id: props.id});
        onData(null, {workflow})
    }
}

export default composeWithTracker(composer)(PublicPage);