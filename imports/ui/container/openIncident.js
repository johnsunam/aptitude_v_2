import {composeWithTracker} from 'react-komposer';
import OpenIncident from '../components/app/dashboard/openIncident';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';

const composer = (props, onData) => {
    const subscription = Meteor.subscribe('getWorkFlow');
    if(subscription.ready()) {
        let page = props.workflow.flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'][0];
        console.log(page);
        const datas = _.map(props.openIncidents,data=> {
            return {"Created At":moment(data.createdAt).format("dddd, MMMM Do YYYY, h:mm a"), Status:data.status};
        });
        onData(null,{datas:datas,page:page})
    }    


};

export default composeWithTracker(composer)(OpenIncident);
