import {composeWithTracker} from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {FormDb} from '../../api/form/collection/form.collection.js'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {PageDb} from '../../api/page/collection/page.collection.js';
import AddPage from '../components/aptitude/page/addPage.jsx'
const composer = (props, onData) => {
    let pageSub = Meteor.subscribe('getPage');
    let clinetSubcription = Meteor.subscribe('getClient');
    let formSubcription = Meteor.subscribe('getForm');
    if (formSubcription.ready()) {
        let user = window
            .localStorage
            .getItem('user')
        let a = AptitudeAdminDb.findOne({user: user});
        let clients = ClientDb
            .find({
            user: {
                $in: a.companies
            }
        })
            .fetch();
        let forms = FormDb
            .find({user: user})
            .fetch();
        let pages = PageDb
            .find()
            .fetch();
        let page = PageDb.findOne({_id: props.id});
        let edit = props.id
            ? true
            : false;
        let data = {
            clients: clients,
            forms: forms,
            pages: pages,
            page: page,
            edit: edit
        }
        onData(null, {data})
    }

};

export default composeWithTracker(composer)(AddPage);
