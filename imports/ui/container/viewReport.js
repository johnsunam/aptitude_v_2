import {composeWithTracker} from 'react-komposer';
import View from '../components/app/dashboard/viewReport.jsx';
import {FormDataDb} from '../../api/formdata/collection/formdata.collection.js'

const composer = (props, onData) => {
  let subscription = Meteor.subscribe("getFormData")
  if (subscription.ready()) {
    let formdatas = FormDataDb.find({
      user: window
        .localStorage
        .getItem('user'),
      workflow: props.id
    }).fetch();
    onData(null, {formdatas})
  };

}
export default composeWithTracker(composer)(View);
