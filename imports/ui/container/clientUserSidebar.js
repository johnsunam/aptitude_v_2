import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import ClientUserSidebar from '../components/common/clientUserSidebar.jsx'

const composer = (props, onData) => {
  let workflow = Meteor.subscribe('getWorkFlow');
  let subcription = Meteor.subscribe('getClientUser')
  if (subcription.ready()) {
    let client = ClientUserDb.findOne({
      user: Meteor.userId()
    });
    if (window.localStorage.getItem('appType') == "client-admin") {
      let workflows = WorkflowDb
        .find({
        client: {
          $in: [
            window
              .localStorage
              .getItem('company')
          ]
        }
      })
        .fetch();

      onData(null, {workflows})
    } else {

      let workflows = WorkflowDb.find({
        $or: [
          {
            client: {
              $in: [
                window
                  .localStorage
                  .getItem('company')
              ]
            }
          }, {
            roles: {
              $in: client.roles
            }
          }
        ]
      }).fetch();

      let assignerWorkflow = [];

      _.each(workflows, (obj) => {
        if (obj.assignerRoles) {
          _.each(obj.assignerRoles, (single) => {
            let com = _.intersection(single.roles, client.roles);
            com.length > 0
              ? assignerWorkflow.push(obj)
              : '';
          });
        }
      })
      onData(null, {
        workflows: workflows,
        assignerWorkflow: assignerWorkflow
      })
    }

  }

};

export default composeWithTracker(composer)(ClientUserSidebar);