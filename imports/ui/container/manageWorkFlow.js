import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {RoleDb} from '../../api/role/collection/role.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import ManageWorkFlow from '../components/aptitude/workflow/manageWorkFlow.jsx'
const composer = (props, onData) => {
  var workflow = Meteor.subscribe('getWorkFlow');

  var subcription = Meteor.subscribe('getClient')

  if (subcription.ready()) {

    let user = window
      .localStorage
      .getItem('user');
    console.log(props.client);
    props.client
      ? workflows = WorkflowDb
        .find({user: user, client: props.client})
        .fetch()
      : workflows = WorkflowDb
        .find({user: user})
        .fetch();

    let data = {
      workflows: workflows
    }
    onData(null, {data})
  }
};

export default composeWithTracker(composer)(ManageWorkFlow);
