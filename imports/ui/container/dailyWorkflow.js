import {composeWithTracker} from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection';
import DailyWorkflow from '../components/client/homePages/dailyWorkflow.jsx';

const composer = (props, onData) => {
    const clientSub = Meteor.subscribe('getClient');
    const workflowSub = Meteor.subscribe('getWorkFlow');
    const WorkflowDataSub = Meteor.subscribe('getWorkflowDatas');
    if (WorkflowDataSub.ready()) {
        let workflows;
        if (window.localStorage.getItem('appType') === 'aptitude') {
            workflows = WorkflowDb
                .find()
                .fetch();
        } else {

            let client = ClientDb.findOne({
                user: Meteor.userId()
            });

            workflows = WorkflowDb
                .find({
                roles: {
                    $in: client.roles
                }
            })
                .fetch();
            console.log(workflows);
        }

        let workflow_details = _.map(workflows, (workflow) => {
            let count = WorkflowDataDb
                .find({workflow: workflow._id})
                .count();

            return {id: workflow._id, name: workflow.name, description: workflow.description, type: workflow.type, total_inc: count};
        });

        onData(null, {workflowDetails: workflow_details});
    }

}

export default composeWithTracker(composer)(DailyWorkflow);