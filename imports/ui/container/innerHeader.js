import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import InnerHeader from '../components/common/innerHeader.jsx';
const composer = (props, onData) => {
    let workflowdata = Meteor.subscribe("getWorkFlow");
    console.log(props.id);
    if (workflowdata.ready()) {
        let workflow = WorkflowDb.findOne({_id: props.id});
        onData(null, {workflow})
    }

};

export default composeWithTracker(composer)(InnerHeader);