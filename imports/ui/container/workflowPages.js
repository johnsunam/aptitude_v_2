import {composeWithTracker} from 'react-komposer';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection'
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import WorkflowPages from '../../ui/components/client/workflowConfigure/workflowPages.jsx';

const composer = (props, onData) => {
    console.log(props.id);

    let subcription = Meteor.subscribe('getClientUser');
    let workflows = Meteor.subscribe('getWorkFlow');

    if (workflows.ready()) {
        const workflow = WorkflowDb.findOne({_id: props.id});

        pages = workflow['flowchart']['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask'];
        console.log(pages, workflow);

        onData(null, {
            pages: pages,
            workflow: workflow
        });
    }
}

export default composeWithTracker(composer)(WorkflowPages);