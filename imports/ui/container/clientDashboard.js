import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js';
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection';
import ClientAdminPages from '../components/client/dashboard/dashboard.jsx';

const composer = (props, onData) => {
  let email = Meteor.user()
  let subcription = Meteor.subscribe('getPage');
  let client = Meteor.subscribe('getClient');
  let workflow = Meteor.subscribe('getWorkFlow');
  let WorkflowData = Meteor.subscribe('getWorkflowDatas');

  if (WorkflowData.ready()) {

    let workflows = WorkflowDb
      .find({
      client: {
        $in: [
          window
            .localStorage
            .getItem('company')
        ]
      }
    })
      .fetch();
    let datas = _.map(workflows, (obj) => {
      count = WorkflowDataDb
        .find({workflow: obj._id})
        .count();
      obj.count = count;
      return obj;
    });
    onData(null, {datas})
  }

};

export default composeWithTracker(composer)(ClientAdminPages);
