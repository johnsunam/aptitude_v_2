import {composeWithTracker} from 'react-komposer';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection'
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import WorkflowAssignation from '../components/client/dashboard/workflowAssignation.jsx';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
const composer = (props, onData) => {
    console.log(props.id);
    let subcription = Meteor.subscribe('getClientUser');
    let workflows = Meteor.subscribe('getWorkFlow');

    if (workflows.ready()) {
        const users = ClientUserDb
            .find({
            createdBy: Meteor.userId()
        })
            .fetch();

        const workflow = WorkflowDb.findOne({_id: props.id});
        onData(null, {
            users: users,
            workflow: workflow,
            choosedRoles: []
        });
    }
}

export default composeWithTracker(composer)(WorkflowAssignation);