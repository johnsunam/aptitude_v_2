import {composeWithTracker} from 'react-komposer';
import {RoleDb} from '../../api/role/collection/role.collection.js'
import AddWorkFlow from '../components/aptitude/workflow/addWorkFlow.jsx'
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'

const composer = (props, onData) => {
    const subscription = Meteor.subscribe('getWorkFlow');
    if (subscription.ready()) {
        const workflow = WorkflowDb.findOne({_id: props.id});
        const edit = !!props.id;
        onData(null, {
            workflow: workflow,
            edit: edit
        })
    }
};

export default composeWithTracker(composer)(AddWorkFlow);
