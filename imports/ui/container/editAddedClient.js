import { composeWithTracker } from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import AddClient from '../components/aptitude/client/addClient.jsx'
const composer = ( props, onData ) => {
  console.log(props);
    var subcription=Meteor.subscribe('getClient');
    if(subcription.ready()){
      console.log(props.client._id)
        var client=ClientDb.findOne({_id:props.client._id});
        onData( null, {client} )
      }

  };


export default composeWithTracker(composer)(AddClient);
