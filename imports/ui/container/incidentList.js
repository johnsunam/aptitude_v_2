import { composeWithTracker } from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import IncidentList from '../components/app/dashboard/incidentlist.jsx';
const composer = ( props, onData ) => {
  console.log(props);
    let workflowDatas=Meteor.subscribe("getWorkflowDatas");
    if(workflowDatas.ready()){
      var datas =WorkflowDataDb.find({level:props.page.lvl,workflow:props.workflow}).fetch();
       onData(null,{datas})
      }

  };


export default composeWithTracker(composer)(IncidentList);