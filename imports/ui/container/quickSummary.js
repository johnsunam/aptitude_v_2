import {composeWithTracker} from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection';
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import QuickSummary from '../components/client/homePages/quickSummary.jsx';
import {ClientDb} from '../../api/clients/collection/client.collection';
import {clientUserDb} from '../../api/clientUser/collection/clientUser.collection';

const composer = (props, onData) => {
    var subcription = Meteor.subscribe('getWorkflow');
    var clientSubscribe = Meteor.subscribe('getClient');
    var clientUser = Meteor.subscribe('getClientUser');
    var workflowSubscription = Meteor.subscribe('getWorkflowDatas');
    if (workflowSubscription.ready()) {
        let user = Meteor.user();
        let date = moment().subtract(7, 'days');
        let weekstart = new Date(moment(date).startOf('day'));
        let workflows;
        let allWeeklyIncident = 0;
        let allDailyIncident = 0;
        if (_.contains(user.roles, 'aptitude-admin')) {
            workflows = WorkflowDb
                .find()
                .fetch();
        } else if (_.contains(user.roles, 'client-admin')) {
            let client = ClientDb.findOne({user: user._id});
            workflows = WorkflowDb
                .find({
                roles: {
                    $in: client.roles
                }
            })
                .fetch()
        } else {

            let clientUser = clientUserDb.findOne({user: user._id});
            workflows = WorkflowDb
                .find({
                roles: {
                    $in: clientUser.roles
                }
            })
                .fetch()
        }

        let weeklyincidents = _.map(workflows, (obj) => {
            let count = WorkflowDataDb
                .find({
                workflow: obj._id,
                createdAt: {
                    $gte: weekstart,
                    $lt: new Date()
                }
            })
                .count();
            count != 0?allWeeklyIncident++ : '';
            return {name: obj.name, value: count}
        });

        let todayIncident = _.map(workflows, (obj) => {

            let count = WorkflowDataDb
                .find({
                workflow: obj._id,
                createdAt: {
                    $gte: new Date(moment().startOf('day'))
                }
            })
                .count();
            count ? allDailyIncident++ : '';
            return {name: obj.name, value: count}
        });
        onData(null, {
            weeklyincidents: weeklyincidents ,
            todayIncident: todayIncident ,
            worflows: workflows ,
            allWeeklyIncident:allWeeklyIncident ,
            allDailyIncident:allDailyIncident 
        });
    }

};

export default composeWithTracker(composer)(QuickSummary);