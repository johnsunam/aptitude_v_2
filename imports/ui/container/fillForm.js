import {composeWithTracker} from 'react-komposer';
import FillForm from '../components/app/dashboard/fillForm.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'

const composer = (props, onData) => {
    const clientUser = Meteor.subscribe('getClientUser')
    const subcription = Meteor.subscribe('getWorkFlow');
    const clientsubscribe = Meteor.subscribe('getClient');
    const workflowDatas = Meteor.subscribe('getWorkflowDatas');
    if (workflowDatas.ready()) {
        let workflow = WorkflowDb.findOne({_id: props.id});
        let data;
        let user;
        let open_incident;
        
        if (window.localStorage.getItem('appType') == "client-admin") {
            user = ClientDb.findOne({
                user: Meteor.userId()
            })
            // data = {
            //     workflow: workflow,
            //     user: c
            // }
            // onData(null, {data})
        } else {
            user = ClientUserDb.findOne({
                user: Meteor.userId()
            })

            // let data = {
            //     workflow: workflow,
            //     user: c
            // }
            // onData(null, {data})
        }
        console.log(window.localStorage.getItem("user"), workflow._id);
        if(workflow.type == 'track_incident') {
            open_incident = WorkflowDataDb.find({workflow:workflow._id, user: window.localStorage.getItem("user"), status: "open"}).fetch();
        }
        console.log(open_incident);
         data = {
            workflow: workflow,
            user: user,
            open_incident: open_incident
        }
        onData(null,{data});

    }

};

export default composeWithTracker(composer)(FillForm);
