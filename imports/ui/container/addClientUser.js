import { composeWithTracker } from 'react-komposer';
import {RoleDb} from '../../api/role/collection/role.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import AddClientUser from '../../ui/components/client/clientUser/addClientUser.jsx'
const composer = ( props, onData ) => {
    // var Subcription=Meteor.subscribe('getRole');
    // var Subcription=Meteor.subscribe('getAccount');
    let subscription=Meteor.subscribe('getClient');
    if(subscription.ready()){
      let user=window.localStorage.getItem('company');
      let data=ClientDb.findOne({user:user});

       data=data?data:[];

        onData( null, {data} )
      }

  };


export default composeWithTracker(composer)(AddClientUser);
