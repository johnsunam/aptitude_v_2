import {composeWithTracker} from 'react-komposer';
import ShowForm from '../components/app/dashboard/showForm.jsx';
import {ConfigurableValueDb} from '../../api/configurableValues/collection/configurableValues.collection';

const composer = (props, onData) => {
    console.log(props);

    let subcription = Meteor.subscribe('getConfigurableValue');

    if (subcription.ready()) {

        let fieldValue = [];
        
        let datas = ConfigurableValueDb
            .find({page:props.page.page ? props.page.page.detail.page._id: props.page.detail.page._id, worflow: props.workflow._id})
            .fetch();

        _.each(datas, (data) => {
            let values = _.findWhere(data.userData, {user: props.user});
            fieldValue.push({field: data.field_name, values: values.values});

        })

        onData(null, {fieldValue});
    }

};

export default composeWithTracker(composer)(ShowForm);
