import {composeWithTracker} from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import AdminLogin from '../components/accounts/login/adminlogin.jsx';
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection';
import {ClientDb} from '../../api/clients/collection/client.collection';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';

const composer = (props, onData) => {
  const aptitudeAdmin = Meteor.subscribe('getAptitudeAdmin');
  const client = Meteor.subscribe('getClient');
  const clientUser = Meteor.subscribe('getClientUser');
  if (!Meteor.userId()) {
    const val = null;
    onData(null, {val});

  } else {
    if (clientUser.ready()) {
      let user = Meteor.user();

      if (Roles.userIsInRole(Meteor.userId(), ["aptitude-admin"])) {
        window
          .localStorage
          .setItem("appType", "aptitude");
        let aptitude = AptitudeAdminDb.findOne({user: user._id});
        let companies = ClientDb
          .find({
          user: {
            $in: aptitude.companies
          }
        })
          .fetch();
        let data = {
          aptitudeAdmin: aptitude,
          companies: companies
        };
        onData(null, {data});

      } else if (Roles.userIsInRole(Meteor.userId(), ["client-admin"])) {

        window
          .localStorage
          .setItem("appType", "client-admin");
        let clientAdmin = ClientDb.findOne({
          user: Meteor.userId()
        });
        onData(null, {clientAdmin});

      } else {

        window
          .localStorage
          .setItem("appType", "client-user");
        let clientUser = ClientUserDb.findOne({
          user: Meteor.userId()
        });
        onData(null, {clientUser});

      }

    }

  }

};

export default composeWithTracker(composer)(AdminLogin);