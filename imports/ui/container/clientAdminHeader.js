import { composeWithTracker } from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import Header from '../components/common/clientAdminHeader.jsx'
const composer = ( props, onData ) => {
let subcription=Meteor.subscribe('getAptitudeAdmin');
subcription=Meteor.subscribe('getClient');
subcription=Meteor.subscribe('getClientUser')

    if(subcription.ready()){
        let data;
        if(window.localStorage.getItem('appType')=='aptitude'){
             let user= window.localStorage.getItem('user')
            data=AptitudeAdminDb.findOne({user:Meteor.userId()});
        }
        else if(window.localStorage.getItem('appType')=='client-admin'){
            let user=window.localStorage.getItem('user');
            console.log('here')
            data=ClientDb.findOne({user:user});
        }
        else{
            let user=window.localStorage.getItem('user');
            data=ClientUserDb.findOne({user:user});
      
      }
     console.log(data)
        onData(null, {data} )
      }
      

  };


export default composeWithTracker(composer)(Header);
