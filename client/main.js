import '../imports/startup/client/routes.js'

Meteor.startup(()=>{
if(!getCookie('loginStatus')) {
  window.localStorage.setItem('choosed',null)
  window.localStorage.setItem('users',null)
  window.localStorage.setItem('user',null)
  window.localStorage.setItem('type',null)
  window.sessionStorage.setItem('user',null);
  window.localStorage.setItem('userAccess',null);

  Meteor.logout();
}
})

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}
