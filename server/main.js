//default builds the super aptitude admin when app startup
import '../imports/startup/server'
import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import {AptitudeAdminDb} from '../imports/api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {AccountDb} from '../imports/api/account/collection/collection.js'
import {UserAccountDb} from '../imports/api/account/collection/userAccount.js'
import {WorkflowDataDb} from '../imports/api/formdata/collection/workflowData.collection.js'
import {ClientUserDb} from '../imports/api/clientUser/collection/clientUser.collection.js';
import {WorkflowDb} from '../imports/api/workflow/collection/workflow.collection';
import moment from 'moment';
import { NotificationDb } from '../imports/api/notification/collection/notification.collection';
Meteor.startup(() => {

  Meteor
    .setInterval(function () {

      let workflows = WorkflowDb.find({type: 'incident_tracking'}).fetch();

      _.each(workflows,workflow =>{
        let starttime = workflow.start_time;
        let manipulatedTime;
        if(workflow.interval.type == 'minute') {
          manipulatedTime = moment(starttime).add(workflow.interval.interval, 'minutes').format('YYYY-MM-DD h:mm a');
          data = WorkflowDataDb.findOne({workflow: workflow._id, createdAt: {$gte: starttime, $lte: manipulatedTime}});
          if(!data) {
            Meteor.call('addNotification',{workflow: workflow._id, message:"You need to fill up "+workflow.name+" workflow." });
         }
          
        } else if (workflow.interval.type == 'hour') {
          manipulatedTime = moment(starttime).add(workflow.interval.interval, 'hours');
          data = WorkflowDataDb.findOne({workflow: workflow._id, createdAt: {$gte: starttime, $lte: manipulatedTime}});
          if(!data) {
            Meteor.call('addNotification',{workflow: workflow._id, message:"You need to fill up "+workflow.name+" workflow." });
          }
        }

      })
      var date = new moment().format('MM/DD/YYYY HH:mm A');
      var datas = WorkflowDataDb
        .find({
        'action.status': 'assigned',
        'action.ETA': {
          $gte: date
        }
      })
        .fetch();

      _.each(datas, function (data) {
        var msgdate = new moment(data.action.lastmsg).format('MM/DD/YYYY HH:mm');
        var lastmsg = new moment(msgdate)
          .add(data.action.msgInterval, 'hours')
          .format('MM/DD/YYYY HH:mm');
        var current = new moment().format('MM/DD/YYYY HH:mm')
        if (moment(lastmsg).isSame(current)) {
          var receipt = ClientUserDb.findOne({user: data.action.user});
          var mail = {
            to: receipt.email,
            cc: [],
            bcc: [],
            subject: "ETA Notificaton",
            text: "Today is the last day  for the task to get completed"
          }
          Meteor.call('sendEmail', mail, function (err) {
            if (err) 
              console.log(err);
            }
          );
          Meteor.call('updateLastmsg', {
            lastmsg: lastmsg,
            id: data.id
          }, function (err) {
            if (!err) {
              console.log('update');
            }
          })
        }

      });

    }, 60000);

  if (!Meteor.users.findOne()) {

    const userId = Accounts.createUser({email: "admin@yahoo.com", password: "aptitude123"})
    Roles.addUsersToRoles(userId, ['aptitude-admin']);
    let adminId = AptitudeAdminDb.insert({
      user: userId,
      contactName: "john",
      name: "john",
      mobile: '53453453',
      createdBy: null,
      companies: []
    })
  }
});
